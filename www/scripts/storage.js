var Storage;

Storage = (function () {
   function Storage() {

   }

   Storage.prototype.set = function (name, value) {
      return window.localStorage.setItem(name, value);
   };

   Storage.prototype.get = function (name) {
      return window.localStorage.getItem(name);
   };

   Storage.prototype.dump = function () {
      return window.localStorage.dump();
   };

   Storage.prototype.clear = function () {
      return window.localStorage.clear();
   };

   return Storage;
})();
