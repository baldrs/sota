$(document).ready(function () {
   var body = $('body'), ticket = new Ticket();

   // tab update-related code start
   setInterval(function () {
      if (window.location.pathname === '/') {
         $.post('/ajax/', {do:'updateCounts'}, function (response) {
            for (var index = 0, length = response.counters.length; index < length; index++) {
               var current = response.counters[index], page = $('.page' + index), text = page.text();
               if (text.replace(/^\s*|\s*$/, '') != current) {
                  page.text(current);
                  $.post('/ajax/', {do:'updateTab', index:index}, function (response) {
                     $('#' + response.index).html(response.html);
                     $('#' + response.index + ' .table-sortable').tablesorter();
                     $('#' + response.index + ' .table-filterable').tableFilter();
                  }, 'json')
               }
            }
         }, 'json');
      }
   }, 5000);

   body.on('click', '#statuses li', function () {
      var self = $(this), index = $(this).index();

      if (index == ($('#statues').find('li').length - 1)) return;

      $.post('/ajax/', {do:'updateTab', index:self.index()}, function (response) {
         $('#' + response.index).html(response.html);
         $('#' + response.index + ' .table-sortable').tablesorter();
         $('#' + response.index + ' .table-filterable').tableFilter();
         $.post('/ajax/', {do:'updateCounts'}, function (response) {
            for (var index = 0, length = response.counters.length; index < length; index++) {
               var current = response.counters[index], page = $('.page' + index), text = page.text();
               page.text(current);
            }
         }, 'json')
      }, 'json')
   });

   // tab update-related code end

   $('#search').find('input').typeahead({
      source:function (arg) {
         return $.post('ajax/', {
            do:   'suggest',
            field:$('#search').find('button.submit').data('type'),
            value:$('#search').find('input').val()
         }, function (response) {
            arg.process(response);
            return response;
         }, 'json');
      }
   });

   $('.table-sortable').tablesorter();

   body.on('focus', '.date-filter input, .datepicker', function () {
      var self = this;
      $(this).datepicker({ weekStart:1, format:'yyyy-mm-dd', language:'ru' }).on('changeDate', function () {
         $(self).trigger('change');
      });
   });

   body.on('click', '.addToTicket', function () {
      var id = $(this).data('id');
      if (ticket.addProduct(id)) {
         $('#ticket').modal('show');
         $(this).attr('disabled', 'disabled');
      }
   });

   body.on('click', '.modal-footer .add-product', function () {
      var article = $(this).siblings('input.article').val(), ticketID = $(this).data('ticket');

      $.post('/ajax/', {do:'addProductByID', ID:article, ticketID:ticketID}, function (response) {
         if (!response.success) {
            return void(0);
         }

         if (!response.exists) {
            alert('Товара с артикулом ' + article + ' не существует!');
            return void(0);
         }

         return $('#save').html(ticket.atob(response.html));

      }, 'json');
   });

   body.on('click', '#currentTicket', function (e) {
      e.preventDefault();

      $("#ticket").modal('show');
   });

   body.on('click', '#ticket .btn-primary', function () {
      if (!$(this).prop('disabled'))
         ticket.saveTicket($(this).data('additional'));
      $(this).prop('disabled', true);
   });

   body.on('click', '#ticket a.textify', function (e) {
      e.preventDefault();
      var price, amount, text, productID;

      productID = $(this).data('product');
      /*      row = $(this).parentsUntil('tbody').last();

       price = row.find('td.price').text().replace(/[^0-9]/g, '');
       amount = row.find('td.amount').text().replace(/[^0-9]/g, '');*/

      price = $('input.product-price[data-product=' + productID + ']').val();
      amount = $('input.product-amount[data-product=' + productID + ']').val();

      text = $(this).data('replace') + " %a - %p".replace('%a', amount).replace('%p', price);

      if (!$(this).data('clean')) {
         $(this).parentsUntil('td').last().find('textarea.comment-operator').val(text);
      } else {
         $(this).parentsUntil('td').last().find('textarea.comment-operator').val('');
      }
   });

   body.on('click', '#logistics a.textify', function (e) {
      e.preventDefault();
      var text, price, amount, row;

      row = $(this).parentsUntil('tbody').last();

      price = row.find('td.price input').val();
      amount = row.find('td.amount input').val();

      text = $(this).data('replace');
      /*? $(this).data('replace') +
       ' %a - %p'.replace('%a', amount).replace('%p', price) : '';
       */
      $(this).parentsUntil('td').last().find('textarea.comment-operator').val(text);
   });

   body.on('click', '#save button.delete', function () {
      var self = this;

      $.post('/ajax/', {do:'removeProduct', productID:$(this).data('product')}, function (response) {
         if (!response) return;
         $(self).parentsUntil('tbody').last().hide();
         $(self).parentsUntil('table').last().find('tr.supplier[data-product=' + $(self).data('product') +
                                                   ']').hide();
      });
   });

   body.on('change', 'input.purchase', function () {
      $(this).parentsUntil('tbody').toggleClass('success');
   });

   $('.table-filterable').tableFilter();

   body.on('click', '.btn.edit-ticket', function () {

      var ticketID = $(this).data('ticket');

      $.post('ajax/', {do:'initTicket', dataSource:'database', ticketID:ticketID}, function (response) {
         var _ticket = $('#ticket');

         _ticket.html(response);
         _ticket.modal('show');

      });
   });

   body.on('click', '.btn.view-ticket', function () {
      var ticketID = $(this).data('ticket');

      $.post('ajax/', {do:'initTicket', dataSource:'database', ticketID:ticketID, mode:'view'}, function (response) {
         var _ticket = $('#ticket');

         _ticket.html(ticket.atob(response));
         _ticket.modal('show');

      });
   });

   body.on('click', '#search li a', function (e) {
      e.preventDefault();

      var search = $('#search');

      search.find('button.submit').data('type', $(this).data('type'));

      search.find('input[type=text]').attr('placeholder',
         $(this).data('type') == 'sku' ? 'По артикулу' : 'По наименованию');
   });

   body.on('click', '#search button.submit', function (e) {
      e.preventDefault();
      var search = $('#search'), question = search.find('input[name=value]').val(), type = search.find('button.submit').data('type');
      if (question) {
         window.location.assign('/search/' + type + '/' + question.replace(/\s/g, '+').replace(/\//g, '+') + '/');
      }
   });

   body.on('keypress', "#search input[type=text]", function (e) {
      if (e.which == 13) {
         $('#search').find('button.submit').trigger('click');
      }
   });

   body.on('change', '#type', function () {
      var fc = $('.filter-container');
      $.post('ajax/', {do:'getTypeFilters', type:$(this).val()}, function (response) {
         fc.html(ticket.atob(response.html));
      }, 'json');
   });

   body.on('click', '.lookup', function () {
      var fc = $('.filter-container'), selects = fc.find('select'), checkboxes = fc.find('input[type=checkbox]:checked'), result = {};

      selects.each(function (index, elem) {
         var element = $(elem);
         if (element.val() != '-') {
            result[element.attr('name')] = element.val();
         }
      });

      checkboxes.each(function (index, elem) {
         var element = $(elem);
         result[element.attr('name')] = true;
      });

      $.post('ajax/', {do:'filterLookup', data:result}, function (response) {
         $('.filter-output').html(ticket.atob(response.html));
      }, 'json');
   });

   body.on('click', '.btn.update-supplier', function (/*response*/) {

      var now = new Date, data = { where:{
         supplierID:0,
         productID: 0
      }, update:                         {
         price:    0,
         amount:   0,
         soComment:'',
         purchase: 0,
         status:   0,
         update:   new Date
      }}, row;

      now = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ':' +
            now.getMinutes();

      data.update.update = now;

      row = $(this).parentsUntil('tbody').last();

      data.update.price = row.find('input.supplier-price').val();
      data.update.inprice = row.find('input.supplier-inprice').val();

      data.update.amount = row.find('input.supplier-amount').val();

      data.update.soComment = row.find('textarea').val();

      data.update.purchase = row.find('input[type=checkbox]').length ? 1 : 0;

      data.update.status = 2;

      data.where.supplierID = $(this).data('supplier');
      data.where.productID = $(this).data('product');
      data.where.ticketID = $(this).data('ticket');

      var self = this;

      $.post('/ajax/', {do:'updateSupplier', data:data}, function (response) {
         if (!response.success) {
            return void(0);
         }

         $(self).parentsUntil('tbody').fadeOut().remove();

         return true;
      }, 'json');
   });

   body.on('click', '#update', function (e) {
      e.preventDefault();
      window.location.assign('http://' + window.location.host + '/import/');
   });
   body.on('click', '#exportall', function (e) {
      e.preventDefault();
      window.location.assign('http://' + window.location.host + '/export/');
   });

   body.on('click', '.search-tickets', function (response) {
      var what = $(this).siblings('input').val();

      $.post('/ajax/', {do:'findTickets', what:what}, function (response) {
         if (!response) {
            return void(0);
         }

         $('#search-tickets').find('tbody').html(ticket.atob(response.html));
      }, 'json');
   });

   body.on('keypress', '.search-tickets-text', function (e) {
      // e.preventDefault();

      if (e.which == 13) $('.search-tickets').trigger('click');
   });
});
