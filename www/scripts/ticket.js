var Ticket;

Ticket = (function () {
   // constructor
   function Ticket() {
      var self = this, container = $('#save');

      $.post('ajax/', {do:'initTicket'}, function (response) {
         container.html(response);
      });
      //}
   }

   Ticket.prototype.btoa = function (str) {
      return btoa(encodeURIComponent(str));
   };

   Ticket.prototype.atob = function (str) {
      return decodeURIComponent(atob(str));
   };

   Ticket.prototype.addProduct = function (ID) {
      var self = this;
      $.post('ajax/', {do:'addProduct', productID:ID }, function (response) {
         var data = self.atob(response);
         //         console.log(data);
         $('#save').html(data);
      });

      return true;
   };

   Ticket.prototype.saveTicket = function (additional) {
      var data = {
         delivery:   {},
         credentials:{},
         suppliers:  [],
         comment:    '',
         products:   {}
      };

      var save = $('#save');

      data.credentials.name = save.find('input[name=name]').val();
      data.credentials.phone = save.find('input[name=phone]').val();

      if (additional) {
         data.additional = additional;
         data.ID = additional;
         data.status = save.find('input[name=status]:checked').val();
      }

      save.find('.comment-operator').each(function (index, elem) {
         var element = $(elem), supplier = { productID:element.data('product'), supplierID:element.data('supplier'), comment:element.val() };
         supplier.purchase = (function () {
            var checked = element.parentsUntil('tbody').last().find('input[type=checkbox]:checked');
            return checked.length ? 1 : 0;
         })();
         data.suppliers.push(supplier);
      });

      save.find('input.product-amount').each(function (index, element) {
         var price = $(this).parentsUntil('tbody').last().find('input.product-price').val();
         data.products[$(this).data('product')] = { amount:$(this).val(), price:price };
      });

      save.find('td input.additionalInfo').each(function (index, element) {
         var value = $(this).val(), ID = $(this).data('product'), supplier = $(this).data('supplier');

         for (var index in data.suppliers) {
            var elem = data.suppliers[index];
            if (elem.productID == ID && elem.supplierID == supplier) {
               data.suppliers[index].additionalInfo = value;
            }
         }
      });

      data.comment = save.find('textarea[name=comment]').val();

      save.find('input.delivery[type=checkbox]:checked').each(function (index, elem) {
         var element, deliveryMethodID;
         element = $(elem);
         deliveryMethodID = parseInt(element.val());
         data.delivery[deliveryMethodID] = (function () {
            var template = { deliverTime:(new Date).toGMTString(), address:'', warehouse:'', receiver:'', delivererID:0, city:''};
            if (deliveryMethodID == 1) {
               template.deliverTime = save.find('.date').eq(0).val();
               template.deliveryMethodID = 1;
            } else if (deliveryMethodID == 2) {
               template.deliverTime = save.find('.date').eq(1).val();
               template.address = save.find('input[name=address]').val();
               template.deliveryMethodID = 2;
            } else if (deliveryMethodID == 3) {
               template.receiver = save.find('input[name=receiver]').val();
               template.delivererID = save.find('select[name=delivererID]').val();
               template.deliveryMethodID = 3;
               template.city = save.find('input[name=city]').val();
               template.warehouse = save.find('input[name=warehouse]').val();
            }
            return template;
         })();
      });

      console.log(data);

      $.post('ajax/', { do:'saveTicket', data:this.btoa(JSON.stringify(data))}, function (response) {
         if (!response) {
            return void(0);
         }

         window.location.assign('http://' + window.location.host + '/');

         return true;
      });
   };

   return Ticket;
})();
