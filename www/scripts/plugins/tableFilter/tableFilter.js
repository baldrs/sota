/**
 *
 * @source: https://github.com/Baldrs/jquery-tablefilter/blob/master/tableFilter.js
 *
 * @author Baldrs <manwe64@gmail.com>
 * @license
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2012 Arsenij Łozicki aka Baldrs
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

(function ($) {
   $.fn.tableFilter = function () {
      var w = {}, body = $('body');

      this.each(function () {
         var head = $(this).find('thead'), html, width;

         head.find('th').each(function (index) {
            if (typeof w[index] == 'undefined') {
               w[index] = $(this).width();
            }

            width = w[index];

            if ($(this).data('filter-type') == 'dropdown') {
               var data = [];
               head.parent().find('tbody tr td').each(function () {
                  if ($(this).index() == index) {
                     data.push($(this).text());
                  }
               });
               data = //_.uniq(data).sort();
               (function (data) {
                  var result = [], seen = [];
                  for (var i in data) {
                     if (!~seen.indexOf(data[i])) {
                        seen.push(data[i]);
                        result.push(data[i]);
                     }
                  }
                  return result;
               })(data).sort();
               data.unshift('-');
               var options = [];
               for (var i = 0, l = data.length; i < l; i++) {
                  options.push("<option value='" + data[i] + "'>" + data[i] + "</option>");
               }
               html += "<td class='select-filter'><select style='width: " + width + "px'>" + options.join() +
                       "</select></td>";
            } else if ($(this).data('filter-type') == 'date-range') {
               var date = new Date();
               html +=
               "<td class='date-filter'><div class='control-row' style='width:" + (width + 15) + "px'> "
                  + "<input type='text' class='from datepicker' style='margin-right: 5px; width: " +
               Math.ceil((width - 20) / 2) + "px' value='"
                  + date.getFullYear() + '-' + (date.getMonth() + 1) + "-01'>"
                  + "<input type='text' class='to datepicker'   style='width: " + Math.ceil((width - 20) / 2) +
               "px' value='"
                  + date.getFullYear() + '-' + (date.getMonth() + 1) + '-' +
               ((date.getDate() < 10) ? '0' + date.getDate() : date.getDate()) + "'>"
                  + "</div></td>";
            } else if ($(this).data('filter') !== 'none') {
               html += "<td class='text-filter'><input type='text' style='width: " + width + "px'></td>";
            } else {
               html += "<td class='none-filter'></td>";
            }
         });

         html = "<tr class='filter'>%row</tr>".replace(/%row/, html);
         head.siblings('tbody').prepend(html);
      });

      body.on('keyup', this.selector + ' .text-filter input', function (e) {
         e.preventDefault();

         var index = $(this).parent().index(), tb = $(this).parentsUntil('table').last(), search = $(this).val();

         tb.find('tr[class!="filter"] td').each(function () {
            var found, text = $(this).text(), ind = $(this).index();

            found = (function (text, search) {
               if (!~search.search(' ')) {
                  return !!~text.search(search);
               }

               search = search.split(' ').filter(function (elem) { return !!elem;});
               var result = 0;

               for (var i in search) {
                  var test = search[i];

                  if (~text.search(test)) {
                     result++;
                  }
               }

               return !!result;
            })(text, search);

            if (ind == index && found /*~text.search(search)*/) {
               $(this).parent().show();
            } else if (ind == index) {
               $(this).parent().hide();
            } else {
               //   $(this).parentsUntil('table').last().find('td.text-filter input').eq(index).val('');
            }
         });

         if (!search) {
            tb.find('tr[class!="filter"]').show();
         }

      });

      body.on('change', this.selector + ' .select-filter select', function (e) {
         e.preventDefault();

         var index = $(this).parent().index(), tb = $(this).parentsUntil('table').last(), search = $(this).val();
         tb.find('tr[class!="filter"] td').each(function () {
            var text = $(this).text(), ind = $(this).index();

            if (ind == index && text == search) {
               $(this).parent().show();
            } else if (ind == index) {
               $(this).parent().hide();
            }
         });

         if (search == '-') {
            tb.find('tr[class!="filter"]').show();
         }
      });

      body.on('change', this.selector + ' .date-filter input', function () {
         var td = $(this).parent(), index = $(this).parentsUntil('tr').last().index(),
            from = new Date(td.find('.from').val()).getTime() / 1000,
            to = new Date(td.find('.to').val()).getTime() / 1000, tb = td.parentsUntil('table').last();

         var inRange = function (what, a, b) {
            return a <= what && what <= b;
         };

         tb.find('tr[class!="filter"] td').each(function () {
            var current = new Date($(this).text().split(' ')[0]).getTime() / 1000, ind = $(this).index();

            if (ind == index && inRange(current, from, to)) {
               $(this).parent().show();
            } else if (ind == index) {
               $(this).parent().hide();
            }
         });

      });

      return this;
   };
})(jQuery);

