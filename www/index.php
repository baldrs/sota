<?php

namespace WWW {
   use System\Core\Engine;

   error_reporting(E_ALL & E_STRICT & ~E_DEPRECATED & ~E_NOTICE);
   ini_set('display_errors', 1);

   $time = microtime(true);
   session_start();
   header('Content-Type: text/html; charset=utf-8');
   spl_autoload_register(function ($path) {
      if(stripos($path, 'smarty') !== false) return;

      $path = __DIR__ . '/../' . str_replace(array('\\'), array('/'), $path) . '.php';
      if(!file_exists($path))
         throw new \ErrorException("$path does not exist");

      require $path;
   });

   register_tick_function('flush');

   if($argc) { //we're in command-line mode
//      Engine::$mode = 'cli';
      error_reporting(E_ALL);

      try {
         $controller = new \System\Application\Controllers\Shell( /*array('shell', $argv[1]), null*/);
      } catch(\ErrorException $e) {
         var_dump($e);
      }

      $controller->{$argv[1]}($argv);
      exit;
   }

   $allowed_hosts = array('sota.kolesiko.ua', 'sota.kl', 'dev-sota.kolesiko.com.ua');
   if(!isset($_SERVER['HTTP_HOST']) || !in_array($_SERVER['HTTP_HOST'], $allowed_hosts)) {
      header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
      exit;
   }

   Engine::route();

   if(Engine::$config->local->debug == "true") {
      ?>
   <pre>Debug information
      <?php
      echo (microtime(true) - $time) . "\n";  ?>
   </pre> <?php
   }

}

