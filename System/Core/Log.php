<?php

namespace System\Core {
   abstract class Log {
      private static $logs = array('System' => null, 'Database' => null,);

      public static function init() {
         foreach(array_keys(self::$logs) as $name)
            self::$logs[$name] = fopen(__DIR__ . '/../Temp/Logs/' . $name . '-' . date('d-m-Y') . '.log', 'a+');
      }

      public static function write($message, $logName = 'System') {
         $ptr = self::$logs[$logName];

         $trace = debug_backtrace();

         $trace = $trace[1];

         $message = date('H:i:s: ') . $trace['class'] . '::' . $trace['function'] . '(): line: ' . $trace['line'] . ': ' . $message . "\n";

         fputs($ptr, $message);
      }

      public static function uninit() {
         foreach(self::$logs as $log)
            fclose($log);
      }
   }

   Log::init();

   register_shutdown_function(array('System\Core\Log', 'uninit'));
}
