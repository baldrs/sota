<?php

namespace System\Core {
   use \System\Core\Engine;

   define('SMARTY_DIR', __DIR__ . '/../Lib/Smarty/');
   class Design {
      /**
       * @var \Smarty
       */
      protected static $smarty;

      public static function init() {
         require SMARTY_DIR . '/Smarty.class.php';
         self::$smarty = new \Smarty();
         self::$smarty
            ->setTemplateDir(Engine::$config->smarty->folders->views)
            ->setCompileDir(Engine::$config->smarty->folders->compiled)
            ->setCacheDir(Engine::$config->smarty->folders->cache)
            ->setCompileCheck(Engine::$config->smarty->compileCheck);
      }

      public static function  statusToText($status) {
         $statusText = array(1 => 'В обработке', 'Обработан', 'Заявка', 'Заказ', 'Отказ');

         return $statusText[$status];
      }

      public static function assign($var, $data) {
         return self::$smarty->assign($var, $data);
      }

      public static function display($template = null, $cache_id = null, $compile_id = null, $parent = null) {
         self::$smarty->display($template, $cache_id, $compile_id, $parent);
      }

      public static function fetch($template = null, $cache_id = null, $compile_id = null, $parent = null,
                                   $display = false, $merge_tpl_vars = true, $no_output_filter = false) {
         return self::$smarty->fetch($template, $cache_id, $compile_id, $parent, $display, $merge_tpl_vars, $no_output_filter);
      }
   }

   Design::init();
}
