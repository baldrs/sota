<?php
namespace System\Core {
   abstract class Controller {
      protected $allowed = array();
      protected $secure = array();

      protected $secured = false;
      protected $config = null;

      protected $user;

      protected $input;

      /**
       * @param $url
       * @param $queryString
       */
      public function __construct($url, $queryString) {
         $path = __DIR__ . '/../Application/Controllers/Controllers.yml';
         if(file_exists($path)) {
            $this->config = \System\Lib\Yaml\Yaml::parse(file_get_contents($path));
            $c            = explode('\\', get_class($this));
            $this->config = $this->config[end($c)];
         }

         if($this->config) {
            $this->secure  = $this->config['secure'];
            $this->allowed = $this->config['allowed'];
            $this->secured = $this->config['secured'];
         }

         if(!is_array($queryString))
            $queryString = array();
         $this->input = array_merge(array_slice($url, 2), $queryString);

         if(!$this->access($url))
            \System\Core\Engine::show403();
      }


      abstract public function index();

      /**
       * @param $url
       *
       * @return bool
       */
      protected final function access($url) {
         // if secured is true, disallow access to modules listed in unsecured list
         if(in_array($url[1], $this->allowed) && !$this->secured) return true;

         $user       = new \System\Core\User();
         $user       = $user->getUser();
         $this->user = $user;

         if(($this->secured || in_array($url[1], $this->secure)) && (!$user || !$user->authorized)) {
            Design::display('../../Core/Views/login.tpl');
            exit;
         }

         Design::assign('user', $user);

         if(in_array($url[1], $this->secure) && $user && $this->enough($user))
            return true;

         return false;
      }

      /**
       * @param \stdClass $user
       *
       * @return bool
       */
      protected function enough(\stdClass $user) {
         return $user->enabled && in_array($user->group, $this->config['groups'])
                && $user->rights >= $this->config['minRights'];

      }
   }
}