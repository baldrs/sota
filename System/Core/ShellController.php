<?php

namespace System\Core {
   use System\Lib\Yaml\Yaml;
   use System\Core\Engine;

   class ShellController {
      protected $config = null;

      final protected function getConfig($name = 'NeueConfig.yml') {
         $class = get_class($this);

         if(is_null($this->config))
            $this->setConfig(Yaml::parse(file_get_contents($this->getPathByClass($class) . '/' . $name)));

         return $this->config;
      }

      final protected function getPathByClass($class) {
         $class = explode('\\', $class);
         array_pop($class);

         $root_dir = str_replace('System/Core', '', __DIR__);

         return $root_dir . implode('/', $class);
      }

      public function setConfig($config) {
         $this->config = $config;
      }
   }
}
