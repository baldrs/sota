<?php

namespace System\Core {
   class Ajax {
      protected function output($string) {
//         echo base64_encode(rawurlencode(
         echo $string;
//         ));
      }

      public static function encode($string) {
         return base64_encode(rawurlencode($string));
      }

      protected function outputB64JSON($value) {
         $this->output(json_encode($value));
      }
   }
}
