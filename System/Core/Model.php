<?php

namespace System\Core {
   class Model {
      protected $config;

      public function __construct() {
         $path = '../System/Application/Models/Models.yml';
         if(file_exists($path)) {
            $this->config = \System\Lib\Yaml\Yaml::parse(file_get_contents($path));
            $temp         = explode('\\', get_class($this));
            $this->config = $this->config[$temp[sizeof($temp) - 1]];
         }
      }

      /**
       * Создает из строки массив, фильтруя его для поиска
       *
       * Минимальная длина строки на выходе - 2 символа
       * @param $what
       *
       * @return array
       */
      protected function what2array($what) {
         return array_filter(array_map('trim', explode(' ', $what)),
            function ($elem) {
               return mb_strlen($elem) >= 2;
            });
      }

      /**
       * Поиск по базе данных, используя заданные поля в конфиге.
       *
       * Таблица и поля указываются в конфиге
       * @param $what
       *
       * @return array
       */
      public function search($what) {
         $what = $this->what2array($what);

         $fields = $this->config['fields'];

         // склеиваем и экранируем
         $sFields = implode(', ', array_map(function ($elem) { return is_string($elem) ? "`$elem`" : ''; }, $fields));

         return Engine::$dbi
            ->prepare("select $sFields from `{$this->config['table']}` " . Engine::$dbi->whereSearch($fields, $what) . ' limit ?')
            ->bindValue(1, 20, \PDO::PARAM_INT)
            ->execute()->result();
      }

      /**
       * Поиск по полю в базе данных
       *
       * Функция возвращает поля, в строках которых совпало поле с искомой фразой
       * @param $field поле, по которому происходит поиск
       * @param $what  поисковая фраза
       *
       * @return array|null
       */
      public function searchField($field, $what) {
         $sFields = implode(', ', $this->config['fields']);

         return Engine::$dbi
            ->prepare("select $sFields from `" . $this->config['table'] . '` ' . Engine::$dbi->whereSearch(array($field), $this->what2array($what)) . ' limit ?')
            ->bindValue(1, 20, \PDO::PARAM_INT)
            ->execute()->result();
      }

      /**
       * Поиск всего по значению поля
       *
       * Соответствует SELECT * FROM tbl_name WHERE field_name = condition
       *
       * @param $field
       * @param $what
       *
       * @return array|null
       */
      public function searchAllByField($field, $what) {
         return Engine::$dbi
            ->prepare('select * from `' . $this->config['table'] . '` ' . Engine::$dbi->whereSearch(array($field), $this->what2array($what)) . ' limit ?')
            ->bindValue(1, 20, \PDO::PARAM_INT)
            ->execute()->result();
      }

      /**
       * Получить строку по уникальнму индентификатору
       *
       * @param $ID
       *
       * @return mixed|null
       */
      public function get($ID) {
         return Engine::$dbi->prepare('select * from `' . $this->config['table'] . '` where ID = ?')->execute(array($ID))->row();
      }

      /**
       * @param $ID
       *
       * @return array|null
       */
      public function getAll($ID) {
         return Engine::$dbi->prepare('select * from `' . $this->config['table'] . '` where ID = ?')->execute(array($this->config['table'], $ID))->result();
      }
   }
}