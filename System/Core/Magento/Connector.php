<?php
/**
 * Created by JetBrains PhpStorm.
 * User: baldrs
 * Date: 05.02.13
 * Time: 12:20
 * To change this template use File | Settings | File Templates.
 */

namespace System\Core\Magento {

   class Connector {
      public function __construct() {
         $params       = new \stdClass();
         $params->pdo  = "mysql:host=localhost;dbname=kolesikok";
         $params->user = 'u_kolesikoM';
         $params->pass = 'Dm1GmYSG';
         $this->db     = new \System\Core\DBI($params);
      }

      private function getEntityBySku($sku) {
         return $this->db->prepare("select entity_id as entity from catalog_product_entity where sku = ? ")->execute(array($sku))->row()->entity;
      }

      public function getUrl($sku) {
//         $entity = $this->getEntityBySku($sku);
         return $this->db
            ->prepare(
            "select concat('http://kolesiko.ua/',request_path) as url from core_url_rewrite
               where product_id = (select entity_id as entity from catalog_product_entity where sku = ? limit 1)
               and id_path = (select concat('product/',entity_id) as entity from catalog_product_entity where sku = ? limit 1)"
         )
            ->execute(array($sku, $sku))->row()->url;
      }

      public function getImageUrl($sku) {
         // $entity = $this->getEntityBySku($sku);
         return $this->db
            ->prepare('
               select concat("http://kolesiko.ua/media/catalog/product", value) as url from catalog_product_entity_media_gallery
               where entity_id = (select entity_id as entity from catalog_product_entity where sku = ? limit 1)')
            ->execute(array($sku))->row()->url;
      }

   }
}