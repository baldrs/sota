<?php

namespace System\Core {
   class DBI {
      /**
       * @var \PDO
       */
      private $pdo;

      /**
       * @var \PDOStatement
       */
      private $statement;
      public $success;

      //private $q;

      /**
       * @param null|\stdClass $params
       */
      public function __construct($params = null) {
         if(is_null($params))
            $this->pdo = new \PDO(Engine::$config->local->pdo, Engine::$config->local->user, Engine::$config->local->pass);
         else
            $this->pdo = new \PDO($params->pdo, $params->user, $params->pass);
         $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
         $this->pdo->query("set names utf8");
      }

      public function query($q) {
         $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
         try {
            $this->statement = $this->pdo->query($q);
         } catch(\PDOException $e) {
            Log::write($e->getMessage());
            Engine::out($e->getMessage());
         }

         if($this->pdo->errorCode() && $this->pdo->errorCode() != '000000') {
            foreach($this->pdo->errorInfo() as $errorLine)
               Log::write($errorLine, 'Database');
            Log::write($q, 'Database');
         }

         return $this;
      }

      public function prepare($statement, $options = array()) {
         $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
         $this->statement = $this->pdo->prepare($statement, $options);

         if($this->statement->errorCode() && $this->statement->errorCode() != '000000') {
            foreach($this->statement->errorInfo() as $errorLine)
               Log::write($errorLine, 'Database');
            Log::write($this->statement->queryString);
         }

         return $this;
      }

      public function execute($params = null) {
         $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
         $this->success = $this->statement->execute($params);

         if($this->statement->errorCode() && $this->statement->errorCode() != '000000') {
            foreach($this->statement->errorInfo() as $errorLine)
               Log::write($errorLine, 'Database');
            Log::write($this->statement->queryString);
         }

         return $this;
      }

      public function row($style = \PDO::FETCH_OBJ) {
         return $this->statement ? $this->statement->fetch($style) : null;
      }

      public function result($style = \PDO::FETCH_CLASS) {
         return $this->statement ? $this->statement->fetchAll($style) : null;
      }

      public function column() {
         return $this->statement ? $this->statement->fetchAll(\PDO::FETCH_COLUMN) : null;
      }

      public function insert($table, $data, $where = null) {
         $sql      = "insert into $table set ";
         $fields   = array_keys($data);
         $template = array();

         for($i = 0, $l = sizeof($fields); $i < $l; $i++)
            $template[] = '`' . substr($fields[$i], 1) . "` = $fields[$i]";

         if(is_array($where)) {
            $where = $this->array2where($where);

            $data  = array_merge($where[1]);
            $where = $where[0];
         }

         $sql .= implode(', ', $template) . ($where ? ' ' . $where : '');

         $this->prepare($sql)->execute($data);

         return $this;
      }

      public function update($table, $data, $where = null) {
         $sql      = "update $table set ";
         $fields   = array_keys($data);
         $template = array();

         for($i = 0, $l = sizeof($fields); $i < $l; $i++)
            $template[] = '`' . substr($fields[$i], 1) . "` = $fields[$i]";

         if(is_array($where)) {
            $where = $this->array2where($where);

            $data  = array_merge($data, $where[1]);
            $where = $where[0];
         }

         $sql .= implode(', ', $template) . ($where ? ' ' . $where : '');

         $this->prepare($sql)->execute($data);

         return $this;
      }

      /**
       * @return \PDOStatement
       */
      public function getStatement() {
         return $this->statement;
      }

      private function array2where($array, $join = ' and ') {
         $values = array_values($array);
         $array  = array_keys($array);

         $result = array();
         $vr     = array();

         for($i = 0, $l = sizeof($array); $i < $l; $i++) {
            $result[]                  = '`' . $array[$i] . '`' . ' = :' . $array[$i] . $i;
            $vr[':' . $array[$i] . $i] = $values[$i];
         }

         return array('where ' . implode($join, $result), $vr);
      }

      public static function parametrize($array) {
         $tmp = array();
         foreach($array as $key => $value)
            $tmp[':' . $key] = $value;

         return $tmp;
      }

      public function whereSearch($fields, $what) {
         $result = array();
         foreach($what as $value) {
            $row = array();
            foreach($fields as $field) {
               $field = "`$field`";
               $row[] = "$field like " . $this->pdo->quote("%$value%");
            }
            $result[] = '(' . implode(' or ', $row) . ')';
         }
         $result = implode(' and ', $result);

         $result = "WHERE $result ";

         return $result;
      }

      public function bindValue($parameter, $variable, $data_type = \PDO::PARAM_STR) {
         $this->statement->bindValue($parameter, $variable, $data_type);

         return $this;
      }

      public function quote($val) { return $this->pdo->quote($val); }

      public function insertID() {
         return $this->pdo->lastInsertId();
      }

      /**
       * Replaces any parameter placeholders in a query with the value of that
       * parameter. Useful for debugging. Assumes anonymous parameters from
       * $params are are in the same order as specified in $query
       *
       * @param string $query  The sql query with parameter placeholders
       * @param array  $params The array of substitution parameters
       *
       * @return string The interpolated query
       */
      public static function interpolateQuery($query, $params) {
         $keys = array();

         # build a regular expression for each parameter
         foreach($params as $key => $value) {
            if(is_string($key)) {
               $keys[] = '/:' . $key . '/';
            } else {
               $keys[] = '/[?]/';
            }
            $params[$key] = "'$value'";
         }

         $query = preg_replace($keys, $params, $query, 1, $count);

         #trigger_error('replaced '.$count.' keys');

         return $query;
      }

   }
}
