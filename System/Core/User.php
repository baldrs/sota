<?php

namespace System\Core {
   use System\Core\Engine;
   use System\Core\Design;

   class User {
      private $salt = '_SOTA400';
      private $login;
      private $passwd;

      public function __construct() {
         $this->_user = $this->authorize();
      }

      private function authorize() {
         if(isset($_POST['__authorize'])) {
            $this->login  = $_POST['login'];
            $this->passwd = $_POST['passwd'];

            $user = Engine::$dbi
               ->prepare(
               'select `login`, U.name, U.ID, U.dept, rights, G.name as `group`, U.enabled
                  from sota_users U left join sota_userGroups G on G.ID = U.groupID
                  where `login` = :login and `passwd` = sha1(:passwd) and U.enabled = 1
                  ')->execute(array(':login' => $this->login, ':passwd' => $this->passwd . $this->salt))->row();


            if(!$user) {
               Engine::$dbi
                  ->update('sota_users',
                  array(
                       ':session' => null,
                       ':hex'     => null,
                  ),
                  array('login' => $this->login)
               );
               Design::assign('error', true);

               return null;
            }

            $hex  = sha1(openssl_random_pseudo_bytes(32));
            $hash = sha1($hex . $_SERVER['REMOTE_ADDR'] . $this->salt . $user->login);


            Engine::$dbi
               ->update('sota_users', array(
                                           ':session' => $hash,
                                           ':hex'     => $hex,
                                      ), array('login' => $this->login));
            setcookie('__utms', $hash, time() + 36000, '/');

            $user->authorized = true;

            return $user;
         }

         if(isset($_COOKIE['__utms'])) {
            $user = Engine::$dbi->prepare(
               'select U.ID, `login`, U.name, U.dept, rights, G.name as `group`, hex, session, U.enabled
               from sota_users U left join sota_userGroups G on G.ID = U.groupID
               where U.session = :session and U.enabled = 1
               '
            )->execute(array(':session' => $_COOKIE['__utms']))->row();

            if(!$user) {
               Design::assign('error', true);

               return null;
            }

            $hash = sha1($user->hex . $_SERVER['REMOTE_ADDR'] . $this->salt . $user->login);

            if($hash != $user->session) { // session is a fake or old
               Engine::$dbi
                  ->update('sota_users', array(
                                              'session' => null,
                                              'hex'     => null,
                                         ), array('login' => $user->login));
               setcookie('__utms', null, -1, '/');

               Design::assign('error', true);

               return null;
            }

            $user->authorized = true;

            return $user;
         }
         Design::assign('error', true);

         return null;
      }

      public function logout() {
         Engine::$dbi
            ->update('sota_users', array(
                                        ':session' => null,
                                        ':hex'     => null,
                                   ), array('login' => $this->_user->login));
         setcookie('__utms', null, -1, '/');
      }

      public function isAuthorized() {
         return $this->_user && $this->_user->enabled && $this->_user->authorized;
      }

      public function getUser() {
         return $this->_user;
      }
   }

}
