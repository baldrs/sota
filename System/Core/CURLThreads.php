<?php

namespace System\Core {
   class CURLThreads {
      private $mh;
      private $hs = array();
      private $c = 0;

      public function __construct() {
         $this->mh = curl_multi_init();
      }

      /**
       * Adds task to tasks list
       *
       * Task is an url of a worker string or an array of such urls
       *
       * @param array|string $task
       */
      public function add($task) {
         if(is_string($task)) {
            $handle = curl_init($task);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
            $this->hs[$this->c] = $handle;
            curl_multi_add_handle($this->mh, $this->hs[$this->c]);
            $this->c += 1;
         } elseif(is_array($task)) {
            foreach($task as $t) {
               $handle = curl_init($t);
               curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
               $this->hs[] = $handle;
               curl_multi_add_handle($this->mh, $this->hs[$this->c]);
               $this->c += 1;
            }
         }
      }

      /**
       * Executes the list of threads
       *
       * Each thread is executed and result is stored in an multidimensional array
       *
       * Possible keys of returned array are time, response and error
       *
       * @return array
       */
      public function exec() {
         $active = null;

         $result = array();

         do {
            $mrc = curl_multi_exec($this->mh, $active);
         } while($mrc == CURLM_CALL_MULTI_PERFORM);

         while($active && $mrc == CURLM_OK) {
            if(curl_multi_select($this->mh) != -1) {
               do {
                  $mrc = curl_multi_exec($this->mh, $active);
               } while($mrc == CURLM_CALL_MULTI_PERFORM);
            }
         }

         foreach($this->hs as $key => $handle) {
            $err = curl_error($handle);
            if($err === '') {
               $result[$key] = array(
                  'time'     => curl_getinfo($handle, CURLINFO_TOTAL_TIME),
                  'response' => curl_multi_getcontent($handle),
               );

            } else {
               $result[$key] = array(
                  'error' => $err,
               );
            }
            curl_multi_remove_handle($this->mh, $handle);
            curl_close($this->hs[$key]);
         }

         return $result;
      }

      public function __destruct() {
         curl_multi_close($this->mh);
      }
   }
}
