<?php

namespace System\Core {
   use \System\Core\Engine;

   abstract class Router {
      public static function route() {
         $url = $_SERVER['REQUEST_URI'];
         if($url && $url != '/') {
            list($url, $queryString) = explode("?", $url);
            $_queryString = array();
            if(isset($queryString) && $queryString) {
               parse_str($queryString, $_queryString);
               $queryString = $_queryString;
            }
            $url = array_values(array_filter(explode('/', $url), function ($e) {
               return $e === "0" ? true : !!$e;
            }));
            self::execute($url, $queryString);
         } else {
            $url         = array(
               Engine::$config->default->controller,
               Engine::$config->default->action,
            );
            $queryString = null;

            if(!self::run($url, $queryString))
               Engine::show404();
         }
      }

      private static function controllerExists($name) {
         $name = ucfirst($name);

         return file_exists("../System/Controllers/$name.php") || file_exists("../System/Controllers/$name/$name.php");
      }

      private static function execute($url, $queryString) {
         if(isset($url[0]) && self::controllerExists($url[0]) && $url[0] != 'index') {
            return self::run($url, $queryString);
         } elseif(isset($url[0]) && !self::controllerExists($url[0])) {
            $_url = $url;
            $url  = array(
               Engine::$config->default->controller,
               $url[0],
            );

            $url = array_merge($url, array_slice($_url, 1));

            return self::run($url, $queryString);
         } elseif(empty($url) && $queryString) {
            $url = array(
               Engine::$config->default->controller,
               Engine::$config->default->action,
            );

            return self::run($url, $queryString);
         } else {
            Engine::show404();
         }

         return false;
      }

      private static function run($url, $queryString) {
         $name = ucfirst($url[0]);
         $path = array(
            Engine::$config->systemPath . "Application/Controllers/$name/$name.php", // prefer controller dirs instead of files
            Engine::$config->systemPath . "Application/Controllers/$name.php",
         );

         for($i = 0, $l = count($path); $i < $l; $i++) {
            $file = $path[$i];
            if(file_exists($file)) {
               $file = str_replace(array('../', '/', '.php'), array('', '\\', ''), $file);
               if(!$url[1])
                  $url[1] = Engine::$config->default->action;
               $controller = new $file($url, $queryString);
               $controller->$url[1]();

               return true;
            }
         }

         return false;
      }

   }
}
