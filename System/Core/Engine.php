<?php
namespace System\Core {
   use System\Lib\Yaml\Yaml;

   abstract /* abstract means static here */
   class Engine {
      const OUT_PRE = 'pre';
      /**
       * @var DBI
       */
      public static $dbi;
      public static $config;
      public static $mode = 'web';

      public static function init() {
         global $argc, $argv;
//         self::$config = json_decode(file_get_contents('../Engine/config.json'));
         self::$config = Engine::array2object(Yaml::parse(file_get_contents(__DIR__ . '/Config.yml')));
         if(!$argc)
            self::$config->local = self::$config->{$_SERVER['HTTP_HOST']};
         else
            self::$config->local = self::$config->cli;
//         self::$db            = new DBInterface();
         self::$dbi = new DBI();
      }

      public static function out($text, $mode = null) {
         if(self::$mode == 'cli' || $mode = 'text')
            echo $text, "\n";
         elseif($mode = 'pre')
            echo "<pre>$text</pre>"; else
            echo $text, '<br/>';
      }

      public static function route() {
         Router::route();
      }

      public static function isXHR() {
         return
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
      }

      public static function flatten(array $array) {
         $return = array();
         array_walk_recursive($array, function ($a, $k) use (&$return) {
            if(is_numeric($k)) $return[] = $a; else $return[$k] = $a;
         });

         return $return;
      }

      public static function isAssoc($array) {
         return !ctype_digit(implode('', array_keys($array)));
      }

      public static function show404() {
         header('HTTP/1.1 404 not found'); ?>
      <h1>404 Not found</h1>
      <div>The page you have requested is not found on this server</div>
      <div>Please go <a href="javascript: window.history.back()">back</a>, or try again later.</div>
      <?php
         exit;
      }

      public static function show403() {
         header('HTTP/1.1 403 Forbidden');
         echo "<style type=\"text/css\">
               .content {
                  padding: 10px;
                  margin: 20px;
                  border: 1px solid red;
                  background-color: #ff9999;
                  color: #c00000;
               }
            </style>
            <div class=\"content\">
               <h1>403 Forbidden</h1>
               <p>You don't have permission to access {$_SERVER['REQUEST_URI']} on this server</p>
            </div>";

         exit;
      }

      public static function internalRedirect($internalUrl) {
         header("Location: http://{$_SERVER['HTTP_HOST']}/$internalUrl");
         exit;
      }

      public static function readConfig($cls) {
         return Yaml::parse(file_get_contents(getcwd() . '/../' . str_replace('\\', '//', get_class($cls)) . '.yml'));
      }

      public static function object2array($object, $prefix = '') {
         $result = array();

         foreach($object as $key => $value) {
            if($prefix) $key = $prefix . $key;
            if(is_object($value)) {
               $result[$key] = self::object2array($value, $prefix);
            } else {
               $result[$key] = $value;
            }
         }

         return $result;
      }

      public static function array2object($array) {
         $result = new \stdClass();

         foreach($array as $key => $value) {
            if(is_array($value))
               $result->{$key} = Engine::array2object($value);
            else
               $result->{$key} = $value;
         }

         return $result;
      }

      public static function dump($var) {
         var_dump($var);
         exit;
      }

      public static function dumpR($var) {
         if(is_array($var)) {
            foreach($var as $val)
               self::dumpR($val);
         } else {
            self::out($var);
         }
      }

      public static function fork($cmd) {
         $pidfile    = __DIR__ . '/../Temp/Proc/' . crc32($cmd) . '.pid';
         $outputfile = __DIR__ . '/../Temp/Logs/Processes/' . crc32($cmd) . '-' . date('Y-m-d') . '.log';
         $cmd        = sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile);
         exec($cmd);

         return $pidfile;
      }

      public static function isRunning($pid) {
         try {
            $result = shell_exec(sprintf("ps %d", $pid));
            if(count(preg_split("/\n/", $result)) > 2) {
               return true;
            }
         } catch(\Exception $e) {
         }

         return false;
      }

      public static function getMode() {
         return self::$mode;
      }

      public static function setMode($mode) {
         self::$mode = $mode;
      }

   }

   Engine::init();

}

