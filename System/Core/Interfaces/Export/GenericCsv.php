<?php

namespace System\Core\Interfaces\Export {
   interface GenericCsv {
      public function open($open, $mode = 'w');

      public function write($line);

      public function read();

      public function close();

      public function getDelimiter();

      public function getEnclosure();
   }
}
