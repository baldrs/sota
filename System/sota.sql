-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 27 2012 г., 12:17
-- Версия сервера: 5.5.28
-- Версия PHP: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sota`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sota_categories`
--

DROP TABLE IF EXISTS `sota_categories`;
CREATE TABLE IF NOT EXISTS `sota_categories` (
   `ID` int(11) NOT NULL,
   `name` varchar(64) NOT NULL,
   `parent` int(11) NOT NULL,
   KEY `parent` (`parent`),
   KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_deliverers`
--

DROP TABLE IF EXISTS `sota_deliverers`;
CREATE TABLE IF NOT EXISTS `sota_deliverers` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `deliveryMethodID` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `deliveryMethodID` (`deliveryMethodID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `sota_deliverers`
--

INSERT INTO
   `sota_deliverers` (`ID`, `deliveryMethodID`, `name`)
   VALUES (1, 3, 'Новая Почта'),
      (2, 3, 'In Time'),
      (3, 3, 'Другой');

-- --------------------------------------------------------

--
-- Структура таблицы `sota_deliveryMethods`
--

DROP TABLE IF EXISTS `sota_deliveryMethods`;
CREATE TABLE IF NOT EXISTS `sota_deliveryMethods` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `sota_deliveryMethods`
--

INSERT INTO
   `sota_deliveryMethods` (`ID`, `name`)
   VALUES (1, 'Самовывоз'),
      (2, 'Киев'),
      (3, 'Украина');

-- --------------------------------------------------------

--
-- Структура таблицы `sota_products`
--

DROP TABLE IF EXISTS `sota_products`;
CREATE TABLE IF NOT EXISTS `sota_products` (
   `ID` int(11) NOT NULL,
   `categoryID` int(11) NOT NULL,
   `supplierID` int(11) NOT NULL,
   `vendorID` int(11) NOT NULL,
   `sku` varchar(10) NOT NULL,
   `vendor` varchar(255) NOT NULL,
   `supplier` varchar(255) NOT NULL,
   `supplierCity` varchar(255) NOT NULL,
   `name` varchar(255) NOT NULL,
   `path` varchar(255) NOT NULL,
   `numericPath` varchar(255) NOT NULL,
   `availability` varchar(255) NOT NULL,
   `inprice` int(11) NOT NULL,
   `price` int(11) NOT NULL,
   `amount` int(11) NOT NULL,
   `update` datetime NOT NULL,
   `typeID` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `categoryID` (`categoryID`,`supplierID`,`vendorID`),
   KEY `typeID` (`typeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_productTypes`
--

DROP TABLE IF EXISTS `sota_productTypes`;
CREATE TABLE IF NOT EXISTS `sota_productTypes` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `importID` int(11) NOT NULL,
   `name` varchar(255) DEFAULT NULL,
   `magento` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `sota_productTypes`
--

INSERT INTO
   `sota_productTypes` (`ID`, `importID`, `name`, `magento`)
   VALUES (1, 7296, 'Легковые шины', 'legkovie_shini'),
      (2, 7310, 'Легковые диски', 'legkovie_diski');

-- --------------------------------------------------------

--
-- Структура таблицы `sota_properties`
--

DROP TABLE IF EXISTS `sota_properties`;
CREATE TABLE IF NOT EXISTS `sota_properties` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `typeID` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   `magento` varchar(255) NOT NULL,
   `gentype` int(11) NOT NULL,
   `gendefault` varchar(255) NOT NULL,
   `sort` int(11) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `typeID` (`typeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `sota_properties`
--

INSERT INTO
   `sota_properties` (`ID`, `typeID`, `name`, `magento`, `gentype`, `gendefault`, `sort`) VALUES (1, 1, 'Высота', 'k_atyre_car_height', 0, '70', 4),
   (2, 1, 'Ширина', 'k_atyre_car_width', 0, '175', 3),
   (3, 1, 'Диаметр', 'k_atyre_car_diameter', 0, 'R13', 2),
   (4, 1, 'ИнНг', 'k_atyre_car_loadindex', 0, '', 5),
   (5, 1, 'ИнСк', 'k_atyre_car_speedindex', 0, '', 6),
   (6, 1, 'xl', 'xl', 1, '', 8),
   (7, 1, 'Сезон', 'k_atyre_car_season', 0, 'Зимние шины', 0),
   (8, 1, 'Шип', 'spiked', 0, '', 7),
   (9, 1, 'RunFlat', 'runflat', 1, '', 9),
   (10, 2, 'Диаметр', 'k_awheel_car_diameter', 0, '', 2),
   (11, 2, 'PCD', 'k_awheel_car_pcd', 0, '', 3),
   (12, 2, 'ET', 'k_awheel_car_et', 2, '', 4),
   (13, 2, 'W', 'k_awheel_car_w', 2, '', 5),
   (14, 2, 'DIA', 'k_awheel_car_dia', 0, '', 6),
   (15, 2, 'Тип', 'k_awheel_car_type', 0, '', 0),
   (16, 1, 'Бренд', 'manufacturer', 0, '', 1),
   (17, 2, 'Бренд', 'manufacturer', 0, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sota_propertiesValues`
--

DROP TABLE IF EXISTS `sota_propertiesValues`;
CREATE TABLE IF NOT EXISTS `sota_propertiesValues` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `productID` int(11) NOT NULL,
   `value` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `propertyID` (`propertyID`,`productID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_suppliers`
--

DROP TABLE IF EXISTS `sota_suppliers`;
CREATE TABLE IF NOT EXISTS `sota_suppliers` (
   `productID` int(11) NOT NULL,
   `supplierID` int(11) NOT NULL,
   `categoryID` int(11) NOT NULL,
   `vendorID` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   `vendor` varchar(255) NOT NULL,
   `supplier` varchar(255) NOT NULL,
   `supplierCity` varchar(255) NOT NULL,
   `availability` varchar(255) NOT NULL,
   `additionalInfo` varchar(255) NOT NULL,
   `currency` varchar(255) NOT NULL,
   `rawPrice` int(11) NOT NULL,
   `inprice` int(11) NOT NULL,
   `price` int(11) NOT NULL,
   `amount` int(11) NOT NULL,
   `update` datetime NOT NULL,
   `deliverers` varchar(255) NOT NULL,
   `cashless` varchar(255) NOT NULL,
   KEY `productID` (`productID`,`supplierID`,`categoryID`,`vendorID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_ticketComments`
--

DROP TABLE IF EXISTS `sota_ticketComments`;
CREATE TABLE IF NOT EXISTS `sota_ticketComments` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `userID` int(11) NOT NULL,
   `ticketID` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   `comment` text NOT NULL,
   `created` datetime NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `userID` (`userID`),
   KEY `ticketID` (`ticketID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_ticketCredentials`
--

DROP TABLE IF EXISTS `sota_ticketCredentials`;
CREATE TABLE IF NOT EXISTS `sota_ticketCredentials` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `ticketID` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   `phone` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `ticketID` (`ticketID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_ticketDelivery`
--

DROP TABLE IF EXISTS `sota_ticketDelivery`;
CREATE TABLE IF NOT EXISTS `sota_ticketDelivery` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `deliveryMethodID` int(11) NOT NULL,
   `ticketID` int(11) NOT NULL,
   `delivererID` int(11) NOT NULL,
   `address` varchar(255) NOT NULL,
   `warehouse` varchar(255) NOT NULL,
   `city` varchar(255) NOT NULL,
   `receiver` varchar(255) NOT NULL,
   `deliverTime` datetime NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `deliveryMethodID` (`deliveryMethodID`,`ticketID`,`delivererID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_ticketProducts`
--

DROP TABLE IF EXISTS `sota_ticketProducts`;
CREATE TABLE IF NOT EXISTS `sota_ticketProducts` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `productID` int(11) NOT NULL,
   `supplierID` int(11) NOT NULL,
   `ticketID` int(11) NOT NULL,
   `price` int(11) NOT NULL,
   `amount` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   `sku` varchar(10) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `productID` (`productID`,`supplierID`,`ticketID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_tickets`
--

DROP TABLE IF EXISTS `sota_tickets`;
CREATE TABLE IF NOT EXISTS `sota_tickets` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `userID` int(11) NOT NULL,
   `creator` varchar(255) NOT NULL,
   `comment` text NOT NULL,
   `created` datetime NOT NULL,
   `modified` datetime NOT NULL,
   `status` tinyint(4) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `userID` (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_ticketStatuses`
--

DROP TABLE IF EXISTS `sota_ticketStatuses`;
CREATE TABLE IF NOT EXISTS `sota_ticketStatuses` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `sota_ticketStatuses`
--

INSERT INTO
   `sota_ticketStatuses` (`ID`, `name`)
   VALUES (1, 'Наличие'),
      (2, 'Отдел сбыта'),
      (3, 'Callcentre'),
      (4, 'Заявка на заказ'),
      (5, 'Заказ в 1С'),
      (6, 'Отказ');

-- --------------------------------------------------------

--
-- Структура таблицы `sota_ticketSuppliers`
--

DROP TABLE IF EXISTS `sota_ticketSuppliers`;
CREATE TABLE IF NOT EXISTS `sota_ticketSuppliers` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `productID` int(11) NOT NULL,
   `supplierID` int(11) NOT NULL,
   `categoryID` int(11) NOT NULL,
   `vendorID` int(11) NOT NULL,
   `name` varchar(255) NOT NULL,
   `vendor` varchar(255) NOT NULL,
   `supplier` varchar(255) NOT NULL,
   `supplierCity` varchar(255) NOT NULL,
   `availability` varchar(255) NOT NULL,
   `additionalInfo` varchar(255) NOT NULL,
   `currency` varchar(255) NOT NULL,
   `rawPrice` int(11) NOT NULL,
   `inprice` int(11) NOT NULL,
   `price` int(11) NOT NULL,
   `amount` int(11) NOT NULL,
   `update` datetime NOT NULL,
   `ticketID` int(11) NOT NULL,
   `comment` varchar(255) NOT NULL,
   `soComment` varchar(255) NOT NULL,
   `status` tinyint(4) NOT NULL,
   `purchase` tinyint(4) NOT NULL DEFAULT '0',
   `deliverers` varchar(255) NOT NULL,
   `cashless` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `productID` (`productID`,`supplierID`,`categoryID`,`vendorID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sota_userGroups`
--

DROP TABLE IF EXISTS `sota_userGroups`;
CREATE TABLE IF NOT EXISTS `sota_userGroups` (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(255) NOT NULL,
   `enabled` int(11) NOT NULL,
   PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `sota_userGroups`
--

INSERT INTO
   `sota_userGroups` (`ID`, `name`, `enabled`)
   VALUES (1, 'administrators', 1),
      (2, 'operators', 1),
      (3, 'superoperators', 1),
      (4, 'logistics', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
