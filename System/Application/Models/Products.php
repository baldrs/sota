<?php
namespace System\Application\Models {
   use \System\Core\Engine;

   class Products extends \System\Core\Model {

      public function searchForProduct($what, $by) {
         $what = $this->what2array($what);

         if($by == 'sku')
            $what = $what[0];

         $products = null;
         if($by == 'sku')
            $products = Engine::$dbi
               ->prepare('select * from sota_products where sku = :sku order by name asc')
               ->execute(array(':sku' => $what))->result();
         else
            $products = Engine::$dbi
               ->query('select * from sota_products ' . Engine::$dbi->whereSearch(array($by), $what) . ' order by name asc')->result();

         $suppliers = new \System\Application\Models\Suppliers();
         foreach($products as &$product) {
            $product->suppliers = $suppliers->getSuppliers($product->ID);
         }

         $_SESSION['lastsearchused'] = $by;

         return $products;
      }

      public function recount() {
         $result = Engine::$dbi->query("select ID from sota_products")->getStatement();

         Engine::$dbi->update('sota_products', array(
                                                    ':price'        => 0,
                                                    ':inprice'      => 0,
                                                    ':amount'       => 0,
                                                    ':supplier'     => '',
                                                    ':supplierCity' => '',
                                                    ':availability' => 'Нет',
                                                    ':supplierID'   => 0,
                                                    ':update'       => '1970-01-01 00:00',
                                               ));

         $query =
            "INSERT INTO sota_products(ID, supplierID, price, inprice, amount, supplier, supplierCity, availability, `update`) VALUES ";

         while($id = $result->fetch(\PDO::FETCH_OBJ)->ID) {
            $min =
               Engine::$dbi->prepare(
                  "(select sql_calc_found_rows supplierID, price, inprice, amount, supplier, supplierCity, availability, `update`
                from sota_suppliers where productID = ? and amount >= 8 order by productID, inprice asc limit 1)
                union all
                (select supplierID, price, inprice, amount, supplier, supplierCity, availability, `update`
                   from sota_suppliers where productID = ? and amount > 0 and FOUND_ROWS() = 0 order by productID, inprice asc limit 1)")
                  ->execute(array($id, $id))->row();

            if(is_object($min)) {
               $query .= '(' .
                         implode(',', array_map(function ($elem) { return "'$elem'"; }, array_merge(array($id), (array) $min))) .
                         '),';
            }
         }

         Engine::$dbi->query(
            substr($query, 0, strlen($query) - 1)
            . " ON DUPLICATE KEY UPDATE supplierID = values(supplierID), price = values(price), inprice = values(inprice),
            amount = values(amount), supplier = values(supplier), supplierCity = values(supplierCity),
            availability = values(availability), `update` = values(`update`);"
         );
         unset($query);
      }

   }
}