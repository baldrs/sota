<?php

/**
 * Part of SOTA project
 */

namespace System\Application\Models {
   use System\Core\Engine;

   class Filters extends \System\Core\Model {

      public function getTypes() {
         return Engine::$dbi->query('select * from sota_productTypes')->result();
      }

      public function searchForProducts() {
         $available = false;
         if($_POST['data']['available']) {
            $available = true;
            unset($_POST['data']['available']);
         }
         $list      = array();
         $minmax    = array();
         $_products = array();
         foreach($_POST['data'] as $key => $value) {
            $list[] = $key;
            if(strpos($key, '_') !== false) {
               $ID            = explode('_', $key);
               $ID            = $ID[0];
               $minmax[$ID][] = array_merge(array_values(array_reverse(explode('_', $key))), array(intval($value)));
            } else {
               $_products[] =
                  Engine::$dbi
                     ->prepare("select distinct productID from sota_propertiesValues where propertyID = ? and value = ?")
                     ->execute(array($key, $value))->result();
            }
         }

         foreach($minmax as $raw) {
            if(count($raw) == 2) {
               $min        = $raw[0][2];
               $max        = $raw[1][2];
               $propertyID = $raw[1][1];

               $_products[] =
                  Engine::$dbi
                     ->prepare("select distinct productID from sota_propertiesValues where propertyID = ? and value <= ? and value >= ?")
                     ->execute(array($propertyID, $max, $min))->result();
            }
         }

         $products = array();

         foreach($_products as $key => $_product) {
            foreach($_product as $product)
               $products[$key][] = $product->productID;
         }
         unset($_products);

         $result = $products[0];

         foreach($products as $product)
            $result = array_intersect($result, $product);

         $model     = new \System\Application\Models\Products();
         $suppliers = new \System\Application\Models\Suppliers();
         $products  = array();
         if($result) {
            foreach(array_values($result) as $key => $productID) {
               $products[$key]            = $model->get($productID);
               $products[$key]->suppliers = $suppliers->getSuppliers($productID);
               if($available && !$products[$key]->amount)
                  unset($products[$key]);
            }
         }

         uasort($products, function ($a, $b) {
            return strcmp($a->name, $b->name);
         });

         return $products;
      }

      public function getTypeFilters($typeID) {
         $properties =
            Engine::$dbi
               ->prepare('select * from sota_properties where typeID = ? order by sort asc')
               ->execute(array($typeID))->result();

         foreach($properties as &$property)
            $property->values =
               Engine::$dbi
                  ->prepare('select distinct value from sota_propertiesValues where propertyID = ? order by value asc')
                  ->execute(array($property->ID))->result();

         return $properties;
      }
   }
}
