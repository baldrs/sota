<?php

namespace System\Application\Models {
   use System\Core\Engine;

   class Suppliers extends \System\Core\Model {

      public function getSuppliers($productID) {
         $result    = array();
         $suppliers = Engine::$dbi
            ->prepare("select * from sota_suppliers where productID = ? order by price ASC, amount DESC")
            ->execute(array($productID))->result();

         foreach($suppliers as $supplier) {
            $result[$supplier->supplierID] = $supplier;
         }

         return $result;
      }

      public function getTabs() {
         $suppliers = Engine::$dbi
            ->query('select distinct supplier, supplierID from sota_ticketSuppliers where status = 1 and comment != ""')->result();

         foreach($suppliers as &$supplier) {
            $supplier->items =
               Engine::$dbi->prepare('select * from sota_ticketSuppliers where status = 1 and supplierID = ? and comment != ""')->execute(array($supplier->supplierID))->result();
         }

         usort($suppliers, function ($a, $b) {
            return count($a->items) == count($b->items) ? 0 : (count($a->items) < count($b->items) ? 1 : -1);
         });

         return $suppliers;
      }
   }
}
