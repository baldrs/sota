<?php

namespace System\Application\Models {
   use System\Core\Engine;

   class Tickets extends \System\Core\Model {
      // here we go, ticket statuses at the time
      const STATUS_PROCESSED = 0x0001;
      const STATUS_READY     = 0x0002;
      const STATUS_ORDERED   = 0x0003;
      const STATUS_DISCARDED = 0x0004;
      const STATUS_COMPLETE  = 0x0005;
      const STATUS_ARCHIVE   = 0x0006;

      const STATUS_CLOSED = 0x0007;

      public function getTicketsByStatus($status) {
         $tickets = Engine::$dbi->prepare('select * from sota_tickets where status = :status order by created desc')->execute(array(':status' => $status))->result();

         for($i = 0, $l = sizeof($tickets); $i < $l; $i++) {
            $current              = $tickets[$i];
            $ticketID             = $current->ID;
            $current->products    = $this->getProducts($ticketID);
            $current->credentials = $this->getCredentials($ticketID);
            $current->delivery    = $this->getDelivery($ticketID);
            $tickets[$i]          = $current;
         }

         return $tickets;
      }

      public function getTicket($ticketID) {
         $ticket = Engine::$dbi->prepare('select * from sota_tickets where ID = ?')->execute(array($ticketID))->row();

         if($ticket) {
            $ticket->products    = $this->getProducts($ticketID);
            $ticket->credentials = $this->getCredentials($ticketID);
            $ticket->delivery    = $this->getDelivery($ticketID);
            $ticket->statuses    = Engine::$dbi->query('select * from sota_ticketStatuses')->result();
            $ticket->comments    = Engine::$dbi->prepare('select * from sota_ticketComments where ticketID = ? order by created asc')->execute(array($ticketID))->result();
         }

         return $ticket;
      }

      public function getTickets() {
         $tabs   = Engine::$dbi->query('select * from sota_ticketStatuses')->result();
         $result = array();
         for($i = 0, $l = sizeof($tabs); $i < $l; $i++) {
            $result[$i]          = $tabs[$i];
            $result[$i]->tickets = $this->getTicketsByStatus($i + 1);
            $result[$i]->count   = sizeof($result[$i]->tickets);
         }

         return $result;
      }

      public function getDeliverers() {
         return Engine::$dbi->query('select * from sota_deliverers')->result();
      }

      public function getSuppliers($ID, $productID) {
         return Engine::$dbi
            ->prepare('select * from sota_ticketSuppliers where ticketID = ? and productID = ? order by price asc, amount desc')
            ->execute(array($ID, $productID))->result();
      }

      public function getProducts($ID) {
         $products = Engine::$dbi->prepare('select * from sota_ticketProducts where ticketID = ?')->execute(array($ID))->result();
         foreach($products as &$product) {
            $product->suppliers = $this->getSuppliers($ID, $product->productID);
         }

         return $products;
      }

      public function getCredentials($ID) {
         return Engine::$dbi->prepare('select * from sota_ticketCredentials where ticketID = ?')->execute(array($ID))->row();
      }

      public function saveTicket() {
         $data = json_decode(rawurldecode(base64_decode($_POST['data'])));

         $user = new \System\Core\User();
         $user = $user->getUser();


         if($data->additional) {
            return $this->_updateTicket($data, $user);
         }

         $ticket = (object) $_SESSION['ticket'];

         $ticket->credentials = $data->credentials;
         $ticket->delivery    = $data->delivery;

         foreach($data->suppliers as $supplier) {
            $ticket->products[$supplier->productID]->suppliers[$supplier->supplierID]->comment        = $supplier->comment;
            $ticket->products[$supplier->productID]->suppliers[$supplier->supplierID]->additionalInfo = $supplier->additionalInfo;
         }
         $ticket->comment = $data->comment;

         Engine::$dbi->insert('sota_tickets', array(
                                                   ':created'  => date('Y-m-d H:i'),
                                                   ':modified' => date('Y-m-d H:i'),
                                                   ':userID'   => $user->ID,
                                                   ':creator'  => $user->name,
                                                   ':status'   => self::STATUS_PROCESSED,
                                              ));

         $ticket->ID = Engine::$dbi->insertID();
         if($ticket->comment)
            Engine::$dbi->insert('sota_ticketComments',
               array(
                    ':ticketID' => $ticket->ID,
                    ':userID'   => $user->ID,
                    ':comment'  => $ticket->comment,
                    ':name'     => $user->name,
                    ':dept'     => $user->dept,
                    ':created'  => date('Y-m-d H:i'),
               ));


         foreach($ticket->delivery as $row) {
            $row->ticketID    = $ticket->ID;
            $row->deliverTime = date('Y-m-d H:i', strtotime($row->deliverTime));
            Engine::$dbi->insert('sota_ticketDelivery', Engine::object2array($row, ':'));
         }
         $ticket->credentials->ticketID = $ticket->ID;
         Engine::$dbi->insert('sota_ticketCredentials', Engine::object2array($ticket->credentials, ':'));
         $products = $_SESSION['ticket']['products'];
         foreach($products as $product) {
            $_product           = Engine::object2array($product);
            $_suppliers         = $product->suppliers;
            $_product['amount'] = $data->products->{$product->ID}->amount;
            $_product['price']  = $data->products->{$product->ID}->price;
            $insert             = array(
               ':productID' => $_product['ID'],
               ':ticketID'  => $ticket->ID,
               ':price'     => $_product['price'],
               ':amount'    => $_product['amount'],
               ':name'      => $_product['name'],
               ':sku'       => $_product['sku'],
            );


            Engine::$dbi->insert('sota_ticketProducts', $insert);

            foreach($_suppliers as $supplier) {
               if($supplier->productID != $_product['ID'])
                  continue;

               $supplier->status   = $supplier->comment ? self::STATUS_PROCESSED : 0;
               $supplier->ticketID = $ticket->ID;

               Engine::$dbi->insert('sota_ticketSuppliers', Engine::object2array($supplier, ':'));
            }
         }

         if(Engine::$dbi->success) {
            unset($_SESSION['ticket']);
         } else {
            $_SESSION['ticket']['credentials'] = $ticket->credentials;
            $_SESSION['ticket']['delivery']    = $ticket->delivery;
            $_SESSION['ticket']['comment']     = $ticket->comment;
         }

         return Engine::$dbi->success;
      }

      public function updateSupplier($data) {

         Engine::$dbi->update('sota_ticketSuppliers', \System\Core\DBI::parametrize($data['update']), $data['where']);

         unset($data['update']['soComment'], $data['update']['status'], $data['update']['purchase'], $data['where']['ticketID']);

         Engine::$dbi->update('sota_suppliers', \System\Core\DBI::parametrize($data['update']), $data['where']);

         $this->_checkIfTicketsReady();

         return Engine::$dbi->success;

      }

      private function getDelivery($ticketID) {
         $del    = Engine::$dbi
            ->prepare('select D.*, DM.name from sota_ticketDelivery D left join sota_deliveryMethods DM on D.deliveryMethodID = DM.ID where ticketID = ?')
            ->execute(array($ticketID))->result();
         $result = array(1 => '', 2 => '', 3 => '');

         foreach($del as $v) {
            $result[$v->deliveryMethodID] = $v;
         }

         return $result;
      }

      private function _updateTicket($data, $user) {
         $ticket   = $this->getTicket($data->ID);
         $ticketID = $data->ID;
         $user     = new \System\Core\User();
         $user     = $user->getUser();


         Engine::$dbi->update('sota_tickets', array(
                                                   ':modified' => date('Y-m-d H:i'),
                                                   ':status'   => $data->status,
                                              ), array('ID' => $ticketID));
         if($data->comment)
            Engine::$dbi->insert('sota_ticketComments',
               array(
                    ':ticketID' => $ticketID,
                    ':userID'   => $user->ID,
                    ':comment'  => $data->comment,
                    ':name'     => $user->name,
                    ':dept'     => $user->dept,
                    ':created'  => date('Y-m-d H:i'),
               ));

         Engine::$dbi->update('sota_ticketCredentials', Engine::object2array($data->credentials, ':'), array('ticketID' => $ticketID));

         if(!Engine::$dbi->getStatement()->rowCount() && !$ticket->credentials) {
            $data->credentials->ticketID = $ticket->ID;
            Engine::$dbi->insert('sota_ticketCredentials', Engine::object2array($data->credentials, ':'));
         }

         Engine::$dbi->prepare('delete from sota_ticketDelivery where ticketID = ? ')->execute(array($ticketID));

         foreach($data->delivery as $row) {
            $row->ticketID    = $ticket->ID;
            $row->deliverTime = date('Y-m-d H:i', strtotime($row->deliverTime));
            Engine::$dbi->insert('sota_ticketDelivery', Engine::object2array($row, ':'));
         }

         foreach($ticket->products as $key => $product) {
            foreach($product->suppliers as $index => $supplier) {
               foreach($data->suppliers as $sp) {
                  if($supplier->productID == $sp->productID && $sp->supplierID == $supplier->supplierID) {
                     $ticket->products[$key]->suppliers[$index]->comment        = $sp->comment;
                     $ticket->products[$key]->suppliers[$index]->purchase       = $sp->purchase;
                     $ticket->products[$key]->suppliers[$index]->additionalInfo = $sp->additionalInfo;
                  }
               }
            }
         }
         error_reporting(E_ALL & E_STRICT);
         foreach($ticket->products as $product) {
            $_product           = (array) $product;
            $_suppliers         = $product->suppliers;
            $_product['amount'] =
               $data->products->{$product->productID}->amount;
            $_product['price']  = $data->products->{$product->productID}->price;
            $insert             = array(
               ':productID' => $_product['productID'],
               ':ticketID'  => $ticketID,
               ':price'     => $_product['price'],
               ':amount'    => $_product['amount'],
               ':comment'   => $_product['comment'],
            );

            Engine::$dbi->update('sota_ticketProducts', $insert, array('ID' => $_product['ID'], 'ticketID' => $ticketID));

            foreach($_suppliers as $supplier) {
               $supplier->status =
                  trim($supplier->comment) && $ticket->status == self::STATUS_PROCESSED ? self::STATUS_PROCESSED : 0;

               Engine::$dbi->update('sota_ticketSuppliers', Engine::object2array($supplier, ':'), array('ID' => $supplier->ID));
            }
         }

         return Engine::$dbi->success;
      }

      private function  _checkIfTicketsReady() {
         $tickets =
            Engine::$dbi->query('select ID from sota_tickets where status = 1')->result();

         foreach($tickets as $ticket) {
            $suppliers =
               Engine::$dbi
                  ->prepare('select count(*) as count from sota_ticketSuppliers where status = 1 and ticketID = ? and comment != ""')
                  ->execute(array($ticket->ID))->row()->count;

            if(!$suppliers)
               Engine::$dbi->update('sota_tickets', array(':status' => self::STATUS_READY), array('ID' => $ticket->ID));
         }

      }

      public function findTickets($string) {
         $keywords = call_user_func(function ($string) {
            $string = explode(' ', $string);
            $string = array_filter($string, function ($elem) {
               return mb_strlen($elem) > 1;
            });

            $string = array_map('trim', $string);

            return $string;
         }, $string);

         $IDList = array();

         $fields = array(
            'sota_tickets'           => array('comment', 'creator'),
            'sota_ticketCredentials' => array('name', 'phone'),
            'sota_ticketProducts'    => array('name'),
         );

         foreach($fields as $table => $columns) {
            $what = $table == 'sota_tickets' ? 'ID as ticketID' : 'ticketID';
            if(!count($keywords)) continue;
            $temp =
               Engine::$dbi->query("select $what from $table " . Engine::$dbi->whereSearch($columns, $keywords))->result();
            if(!$temp) continue;
            foreach($temp as $IDObject) $IDList[] = $IDObject->ticketID;
         }

         return $IDList;
      }

      public function addProduct() {
         $ID            = $_POST['productID'];
         $productsModel = new \System\Application\Models\Products();
         $suppliers     = new \System\Application\Models\Suppliers();

         $products                 = $_SESSION['ticket']['products'];
         $products[$ID]            = $productsModel->get($ID);
         $products[$ID]->productID = $ID;
         $products[$ID]->suppliers = $suppliers->getSuppliers($ID);


         $_SESSION['ticket']['products'] = $products;

         $ticket              = new \stdClass();
         $ticket->products    = $products;
         $ticket->credentials = new \stdClass();

         return $ticket;
      }

      public function addProductByID() {
         $ID       = $_POST['ID'];
         $ticketID = $_POST['ticketID'];

         $exists = Engine::$dbi->prepare('select ID from sota_products where ID = :ID')->execute(array(':ID' => $ID))->row()->ID;
         if(!$exists) {
            return false;
         }

         $products  = new \System\Application\Models\Products();
         $suppliers = new \System\Application\Models\Suppliers();

         $product            = $products->get($ID);
         $product->suppliers = $suppliers->getSuppliers($ID);

         Engine::$dbi->insert('sota_ticketProducts', array(
                                                          ':ticketID'  => $ticketID,
                                                          ':productID' => $product->ID,
                                                          ':price'     => $product->price,
                                                          ':amount'    => $product->amount,
                                                          ':name'      => $product->name,
                                                          ':sku'       => $product->sku,
                                                     ));

         foreach($product->suppliers as $supplier) {
            $supplier = Engine::object2array($supplier, ':');

            Engine::$dbi->insert('sota_ticketSuppliers', $supplier);
         }

         return $ticketID;
      }

      public function getCounters() {
         $statuses = Engine::$dbi->query('select ID from sota_ticketStatuses')->result();

         $result = array();
         foreach($statuses as $status) {
            $result[] = Engine::$dbi->query('select count(*) as count from sota_tickets where status = ' . $status->ID)->row()->count;
         }

         return $result;
      }

      public function getPage() {

      }


   }
}