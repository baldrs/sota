<?php

namespace System\Application\Models {
   use System\Core\Engine;

   class Users extends \System\Core\Model {
      public function getUsers() {
         return Engine::$dbi->query('select U.*, UG.name as `group` from sota_users U left join sota_userGroups UG on UG.ID = U.groupID')->result();
      }

      public function getGroups() {
         return Engine::$dbi->query('select * from sota_userGroups')->result();
      }

      public function saveUser() {
         $data = array(
            ':login'   => $_POST['login'],
            ':name'    => $_POST['name'],
            ':dept'    => $_POST['dept'],
            ':passwd'  => hash('sha1', $_POST['passwd'] . '_SOTA400'),
            ':groupID' => $_POST['groupID'],
            ':rights'  => 0xFF,
         );

         Engine::$dbi->insert('sota_users', $data);

         return Engine::$dbi->success;
      }
   }
}
