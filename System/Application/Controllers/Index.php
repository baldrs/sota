<?php

namespace System\Application\Controllers {
   use System\Core\Design;
   use System\Core\Engine;
   use System\Lib\Yaml\Yaml;
   use System\Core\Log;

   class Index extends \System\Core\Controller {

      /**
       * Main controller page
       *
       */
      public function index() {
         $tickets = new \System\Application\Models\Tickets();
         $pages   = $tickets->getTickets();
         Design::assign('pages', $pages);
         Design::display('Index/index.tpl');
      }

      /**
       * Main page for tests
       */
      public function test() {
         $b = new \System\Application\Controllers\Import\NeueImport();
         $b->parallelValueLoad();
         sleep(30);
         $b->recountAll();
         $a = new \System\Application\Controllers\Export\NeueExport();
         $a->basicExport();
         $a->propertiesExport('k_atyre_auto');
         $a->propertiesExport('k_awheel_auto');
         $a->propertiesExport('k_atyre_gruz');
         $a->populateOldTables();
      }

      public function exportThread() {
         $export = new \System\Application\Controllers\Export\Export();

         $export->exportType($this->input[0]);
      }

      public function export() {
         $thread = new \System\Core\CURLThreads();
         $count  = Engine::$dbi->query('select count(*) as count from sota_productTypes')->row()->count + 1;
         for($i = 0; $i < $count; $i++)
            $thread->add('http://' . $_SERVER['HTTP_HOST'] . '/exportThread/' . $i . '/');
//         $result =
         $thread->exec();
//         foreach($result as $r) echo $r['response'];
         Engine::internalRedirect('/admin/?done=export');
      }

      public function get1C() {
         $export = new \System\Application\Controllers\Export\Export();
         $export->exportToOnec();
      }

      public function ajax() {
         $handle = new \System\Application\Controllers\IndexAjax($this->config);

         if(in_array($_POST['do'], $this->config['ajax']))
            $handle->{$_POST['do']}();

         exit;
      }

      public function filters() {
         $filters = new \System\Application\Models\Filters();
         $types   = $filters->getTypes();

         Design::assign('types', $types);

         Design::display('Index/Filters/filters.tpl');
      }

      public function search() {
         $type  = $this->input[0];
         $value = str_replace('+', ' ', $this->input[1]); //rawurldecode($this->input[1]);

         $model  = new \System\Application\Models\Products();
         $result = $model->searchForProduct($value, $type);

         $_SESSION['lastsearchused'] = $type;

         Design::assign($type . 'Search', $value);
         Design::assign('products', $result);

         Design::display('Index/search.tpl');
      }

      public function logistics() {
         if(in_array($this->user->group, array('operators', 'superoperators')))
            \System\Core\Engine::show403();

         $model = new \System\Application\Models\Suppliers();

         Design::assign('suppliers', $model->getTabs());

         Design::display('Index/logistics.tpl');
      }

      public function logout() {
         $model = new \System\Core\User();
         $model->logout();

         header('Location: http://' . $_SERVER['HTTP_HOST']);
      }

      public function importSubThread() {
         $import = new \System\Application\Controllers\Import\Import();

         list($file, $type) = $this->input;
         Log::write('Starting import thread');
         Log::write('File: ' . $file);
         Log::write('Type: ' . $type);
         $import->importFile($file, $type);
      }

      public function importMainThread() {
         $type = $this->input[0];

         $import = new \System\Application\Controllers\Import\Import();

         $import->simultaneous($type);
      }

      /**
       * @return array
       */
      public function sync() {
         $type = null;
         if(!$this->user) {
            $ip  = $_SERVER['REMOTE_ADDR'] == '31.134.122.40' ? '31.134.122.40' : '';
            $now = date('YmdH3') . 'kolesiko_sota';
            Log::write('Shell sync - received ' . $this->input[0]);
            $keys = array(
               'cron'   => hash('sha256', "{$ip}yx$now"),
               'update' => hash('sha256', "update{$ip}+{$now}"),
               'xml'    => hash('sha256', "prices{$ip}-{$now}"),
            );

            $type = array_search($this->input[0], $keys);

            if(!$type)
               return;
            Log::write('Seems to be valid ' . $type);
         }

         if(in_array($type, array('cron', 'update'))) {
            Log::write('Starting product import');
            /*            $thread = new \System\Core\CURLThreads();
                        $types  = $this->config['import'];
                        Log::write('Adding threads');
                        for($i = 0, $l = sizeof($this->config['import']); $i < $l; $i++)
                           $thread->add('http://' . $_SERVER['HTTP_HOST'] . '/importMainThread/' . $types[$i] . '/');

                        Log::write('Cleaning tables');
                        \System\Application\Controllers\Import\Import::clearTables();
                        Log::write('Executing threads');
                        $threads = $thread->exec();

                        foreach($threads as $t) Engine::out($t['response']);

                        Log::write('Recounting');
                        $model = new \System\Application\Models\Products();
                        $model->recount();*/
            $import = new \System\Application\Controllers\Import\NeueImport();
            $import->parallelValueLoad();
            sleep(30);
            $import->recountAll();
            $export = new \System\Application\Controllers\Export\NeueExport();
            $export->populateOldTables();
         }
         if($this->user) {
            Log::write('Redirecting');
            Engine::internalRedirect('admin/?done=update');
         } elseif($type == 'cron') {
            Engine::out('Вроде все прошло нормально.', 'text');
            Engine::out('Запускаю импорт в magento', 'text');
            Engine::out('ВНИМАНИЕ! Импорт в magento проходит в фоне. Этот процесс вы можете закрыть. Не рекомендуется пергружать сервер ближайших полчаса!', 'text');
            //shell_exec('cd && php sota.kolesiko.ua/index.php migrate > System/Temp/Logs/migration-'.date('Y-m-d-H-i').'.log 2&>1 &');

            Engine::fork('php index.php migrate');
         } elseif($type == 'xml') {
            Engine::out('Генерю xml', 'text');
            $xml = array('yandex', 'nadavi', 'technoportal', 'vcene', 'hotline', 'freemarket');

            foreach($xml as $file)
               Engine::fork('php index.php xml ' . $file);
            Engine::out('Сгеренились. Подробнее в логах', 'text');
         } elseif($type == 'update') {
            Engine::out('Вроде все прошло нормально.', 'text');
         }
      }

      public function recount() {
         $model = new \System\Application\Models\Products();
         $model->recount();
      }

      public function admin() {

         //Design::assign('');
         $model = new \System\Application\Models\Users();

         if($this->input[0] == 'addUser') {
            if($model->saveUser())
               \System\Core\Engine::internalRedirect('admin/');
         } elseif($this->input[0] == 'uploadCsv') {
            require '../System/Lib/Upload/upload.php';
            if(isset($_GET['qqfile'])) {
               $file = new \qqUploadedFileXhr();
            } elseif(isset($_FILES['qqfile'])) {
               $file = new \qqUploadedFileForm();
            } else {
               $file = false;
            }
            if(!$file) {
               echo json_encode(array('error' => 'no file'));
            } else {
               $n = pathinfo($file->getName());
               if($n['extension'] == 'csv') {
                  if(file_exists('../System/Temp/' . $file->getName()))
                     unlink('../System/Temp/' . $file->getName());
                  if($file->save('../System/Temp/' . $file->getName()))
                     echo json_encode(array('success' => true));
               }
            }
            exit;
         }

         if($this->input['done'])
            Design::assign('done', $this->input['done']);

         Design::assign('users', $model->getUsers());

         Design::assign('groups', $model->getGroups());

         Design::display('Index/Admin/admin.tpl');
      }
   }
}
