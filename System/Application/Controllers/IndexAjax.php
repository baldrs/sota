<?php
namespace System\Application\Controllers {
   use System\Core\Engine;
   use System\Core\Design;

   class IndexAjax extends \System\Core\Ajax {
      /**
       * @param $config
       */
      public function __construct($config) {
         $this->config = $config;
         $user         = new \System\Core\User();
         $this->user   = $user->getUser();

         if(!isset($_SESSION['ticket'])) {
            $_SESSION['ticket'] = array('products' => array(), 'credentials' => array(), 'delivery' => array());
         }

         if(!Engine::isXHR()) Engine::show403();
      }

      public function filterLookup() {
         $model = new \System\Application\Models\Filters();

         Design::assign('products', $model->searchForProducts());

         echo json_encode(array('html' => $this->encode(Design::fetch('Index/Filters/filterTable.tpl'))));

      }

      public function getTypeFilters() {
         $typeID = $_POST['type'];

         $model = new \System\Application\Models\Filters();

         Design::assign('properties', $model->getTypeFilters($typeID));

         echo json_encode(array('html' => base64_encode(rawurlencode(Design::fetch('Index/Filters/filter.tpl')))));
      }

      /**
       *
       */
      public function suggest() {
         $model  = new \System\Application\Models\Products();
         $result = $model->searchField($_POST['field'], $_POST['value']);
         $ret    = array();
         foreach($result as $row) {
            if($_POST['field'] == 'sku')
               $ret[] = "$row->sku $row->name";
            else
               $ret[] = $row->name;
         }

         echo json_encode($ret);
      }

      public function initTicket() {
         $model  = new \System\Application\Models\Tickets();
         $ticket = null;
         if($_POST['dataSource'] == 'database') {
            $ticket = $model->getTicket($_POST['ticketID']);
            Design::assign('edit', true);
         } else {
            $ticket = (object) $_SESSION['ticket'];
         }

         Design::assign('ticket', $ticket);
         Design::assign('deliverers', $model->getDeliverers());

         if($_POST['dataSource'] == 'database') {
            Design::assign('title', "Тикет №{$ticket->ID}");
            Design::assign('date', $ticket->created);
            Design::assign('name', $ticket->creator);
            if($_POST['mode'] == 'view')
               Design::assign('view', true);
            $this->output(Design::fetch('Index/modal.tpl'));
         } else {
            $this->output(Design::fetch('Index/Tickets/edit.tpl'));
         }
      }

      public function addProduct() {
         $tickets = new \System\Application\Models\Tickets();
         $ticket  = $tickets->addProduct();

         Design::assign('ticket', $ticket);
         Design::assign('deliverers', $tickets->getDeliverers());
         echo base64_encode(rawurlencode(Design::fetch('Index/Tickets/edit.tpl')));

      }

      public function addProductByID() {
         $tickets = new \System\Application\Models\Tickets();

         $ticketID = $tickets->addProductByID();
         $ticket   = $tickets->getTicket($ticketID);
         Design::assign('ticket', $ticket);
         Design::assign('edit', true);

         if($ticketID) {
            echo json_encode(array(
                                  'success' => Engine::$dbi->success,
                                  'exists'  => true,
                                  'html'    => $this->encode(Design::fetch('Index/Tickets/edit.tpl')),
                             ));

         } else echo json_encode(array('success' => true, 'exists' => false));
      }

      public function findTickets() {
         $what = $_POST['what'];

         $model = new \System\Application\Models\Tickets();

         $tickets = array();

         $IDList = $model->findTickets($what);
         for($i = 0, $l = count($IDList) > 20 ? 20 : count($IDList); $i < $l; $i++) {
            $tickets[$i]             = $model->getTicket($IDList[$i]);
            $tickets[$i]->statusText = Design::statusToText($tickets[$i]->status);
         }

         Design::assign('tickets', $tickets);
         Design::assign('phrase', $what);

         $html = Design::fetch('Index/Tickets/ticketTable.tpl');

         echo json_encode(array('html' => $this->encode($html)));
      }

      public function removeProduct() {
         $ID = $_POST['productID'];
         if(isset($_SESSION['ticket']['products'][$ID])) {
            unset($_SESSION['ticket']['products'][$ID]);

            echo "c3VjY2Vzcw==";
         }
      }

      public function updateSupplier() {
         $data = $_POST['data'];

         $model = new \System\Application\Models\Tickets();

         $model->updateSupplier($data);

         echo json_encode(array('success' => Engine::$dbi->success));
      }

      public function saveTicket() {
         $model = new \System\Application\Models\Tickets();

         echo json_encode(array('success' => $model->saveTicket()));
      }

      public function updateCounts() {
         $ticket = new \System\Application\Models\Tickets();

         echo json_encode(array('counters' => $ticket->getCounters()));
      }

      public function updateTab() {
         $index = $_POST['index'] + 1;

         $model = new \System\Application\Models\Tickets();

         $page = new \stdClass();

         $page->tickets = $model->getTicketsByStatus($index);

         Design::assign('page', $page);
         Design::assign('name', $index);
         Design::assign('first', true);

         $this->answer(array(
                            'html'  => Design::fetch('Index/Tickets/ticketPage.tpl'),
                            'index' => $index - 1,
                       ));

      }

      private function answer($data) {
         echo json_encode($data);
      }
   }
}