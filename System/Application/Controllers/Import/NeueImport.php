<?php

namespace System\Application\Controllers\Import {
   use System\Core\Engine;

   class NeueImport extends \System\Core\ShellController {
      public function __construct() {
         $this->reader = new \System\Application\Controllers\Export\BaseCsv();
      }

      private function clearValueTables() {
         $tables = array('categories', 'products', 'suppliers');
         foreach($tables as $table) {
            Engine::$dbi->query("truncate table sota_neue_{$table}_values");
         }

         Engine::$dbi->query('truncate table sota_neue_products_attributes_values');
      }

      private function detectType($id_string) {
         $config = $this->getConfig();

         $id_string = str_replace(array('"', "'"), '', $id_string);

         $types = $config['types'];

         $value = explode('.', $id_string);

         foreach(array_keys($types) as $type) {
            list($typeID, $position) = explode('_', $type);
            if($value[$position] == $typeID)
               return trim($types[$type]);
         }

         return '';

      }

      public function parallelValueLoad() {
         $this->clearValueTables();
         $path = __DIR__ . '/../../../Temp';
         $dir  = scandir($path);
         for($i = 0, $l = sizeof($dir); $i < $l; $i++) {
            $current = $dir[$i];
            if(is_dir($path . '/' . $current)) continue;

            preg_match('/[a-z]+_\d+\.csv/', $current, $matches);
            if(!empty($matches)) {
               Engine::fork("php index.php loadFile \"$path/$current\"");
            }
         }
      }

      public function fillWithZeroes($string, $prefix) {
         if(strpos($string, '.') !== false) {
            $result = array();
            foreach(explode('.', $string) as $number) {
               $result[] = $this->fillWithZeroes($number, $prefix);
            }

            return implode('.', $result);
         }

         return $prefix . str_pad($string, 7, '0', STR_PAD_LEFT);
      }

      public function putValuesDataIntoTable($file, $table) {
         $time = microtime(true);

         error_reporting(E_ALL & ~E_NOTICE);

         Engine::out('Opening file ' . $file);
         $this->reader->open($file, true);

         $operators = new Operators();

         list(, $prefix) = explode('_', pathinfo($file, PATHINFO_BASENAME));
         list($prefix,) = explode('.', $prefix);

         Operators::setZfp($prefix);
         Engine::out('Zero fill prefix is ' . $prefix);

         $lists = array(
            'categories' => array(1, 2, 12),
            'products'   => array(55, 56, 57, 58, 60, 62, 63,),
            'suppliers'  => array(30, 31, 32, 33, 34)
         );

         $where_is_baseID = array(
            'categories' => 0,
            'products'   => 59,
            'suppliers'  => 32,
         );

         $not_suppliers = $table != 'suppliers';

         if($not_suppliers) {
            $big_query_head = "insert into sota_neue_{$table}_values(`propertyID`, `baseID`, `value`) values ";
         } else {
            $big_query_head = "insert into sota_neue_{$table}_values(`propertyID`, `baseID`, `supplierID`, `value`) values ";
         }

         $advanced_properties = $advanced_properties_head = null;

         if($table == 'products') {
            $advanced_properties_head = "insert into sota_neue_products_attributes_values(`baseID`, `propertyID`, `value`) values ";
            $advanced_properties      = '';
         }

         $big_query = '';

         $baseID_position = $where_is_baseID[$table];

         $head = $this->reader->read();

         Engine::out('started read cycle');

         while($row = $this->reader->read()) {
            for($i = 1, $l = sizeof($row) + 1; $i < $l; $i++) {
               $c = $row[$i - 1];

               if(in_array($i, $lists[$table]))
                  $c = $this->fillWithZeroes($c, $prefix);

               if($table == 'products') {
                  $baseID = $row[$baseID_position];
                  switch($i) {
                     case 57:
                        $type = Engine::$dbi->quote($this->detectType($c));
                        $big_query .= "(70, {$this->fillWithZeroes($row[$baseID_position], $prefix)}, {$type}), ";
                        break;
                     case 4:
                        Operators::setType('auto');
                        Operators::setZfp($prefix);
                        $operators->setData(array($baseID, 9 => $row[56], 15 => '', 4 => $row[68], 8 => $row[12]));
                        $operators->PATTERN(str_replace(array('"', "'"), '', $c));
                        $advanced_properties .= Operators::$query;
                        Operators::$query = '';

                        if(in_array($this->detectType($this->fillWithZeroes($row[56], $prefix)), array('k_atyre_auto', 'k_awheel_auto', 'k_atyre_gruz'))) {
                           $c = "{$row[19]} {$row[20]} $c";
                        }

                        break;
                     case 43:
                     case 44:
                        $c = date('Y-m-d H:i', strtotime(str_replace('/', '-', $c)));
                        break;
                  }
               } elseif($table == 'suppliers') {
                  switch($i) {
                     case 15:
                        $c = trim($c);
                        if(strpos($c, '+') !== false) {
                           $c = 20;
                        } elseif(stripos($c, 'зак') !== false) {
                           $c = 4;
                        } elseif(strpos($c, '.') !== false) {
                           $c = floatval(preg_replace('/[^0-9.]+/iu', '', $c));
                        } else {
                           $c = preg_replace('/[^0-9]+/iu', '', $c);
                        }
                        break;
                     case 28:
                     case 29:
                        $c = date('Y-m-d H:i', strtotime(str_replace('/', '-', $c)));
                        break;

                  }

               }

               $c = Engine::$dbi->quote($c);

               $big_query .=
                  ($not_suppliers ?
                     "($i, {$this->fillWithZeroes($row[$baseID_position], $prefix)}, $c), " :
                     "($i, {$this->fillWithZeroes($row[$baseID_position], $prefix)}, {$this->fillWithZeroes($row[29], $prefix)} , $c), "
                  );

               if(strlen($big_query) > 0x80000) {
                  //Engine::$dbi->query(
                  $query = $big_query_head . $big_query;
                  $query = substr($query, 0, strlen($query) - 2);
                  Engine::$dbi->query($query);
                  //);
                  $big_query = $query = '';
               }

            }
         }

         if($big_query) {
            $q1 = $big_query_head . substr($big_query, 0, strlen($big_query) - 2);
            Engine::$dbi->query($q1);
         }

         if($advanced_properties) {
            $q3 = $advanced_properties_head . substr($advanced_properties, 0, strlen($advanced_properties) - 2);
            Engine::$dbi->query($q3);
         }

         echo "Execution time: " . (microtime(true) - $time) . "\n";
      }

      /**
       * @param        $ruleID
       * @param string $selection_rules
       */
      public function recount($ruleID, $selection_rules = 'propertyID = 70 and value = "k_atyre_auto"') {
         // TODO move to model
         error_reporting(E_ALL);

         $suppliers_properties = "(1, 2, 3, 4, 6, 7, 8, 9, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 27, 29, 30, 32, 34, 35, 36, 37, 38)";
         $products_properties  = "(1, 2, 70, 3, 29, 30, 6, 7, 10, 11, 12, 31, 32, 33, 34, 35, 36, 37, 38, 41, 44, 62, 55, 63, 65, 66, 67, 68)";

         $id_list = '(' . implode(',', Engine::$dbi
            ->query('select distinct baseID from `sota_neue_products_values` where ' . $selection_rules)
            ->getStatement()->fetchAll(\PDO::FETCH_COLUMN)) . ')';

         $products = Engine::$dbi
            ->query(
            'select baseID, propertyID, value, ID
            from sota_neue_products_values
            where baseID in ' . $id_list . ' and propertyID in ' . $products_properties)
            ->getStatement();

         $suppliers = Engine::$dbi->query(
            'SELECT baseID, propertyID, value, supplierID
            FROM sota_neue_suppliers_values
            WHERE baseID in ' . $id_list . ' and propertyID in ' . $suppliers_properties
         )->getStatement();

         $data = array();
         while($current = $products->fetch(\PDO::FETCH_NUM)) {
            if(!isset($data[$current[0]])) {
               $data[$current[0]] = array($current[1] => array($current[2], $current[3]), 'suppliers' => array());
               continue;
            }

            $data[$current[0]][$current[1]] = array($current[2], $current[3]);
         }

         unset($products);

         while($current = $suppliers->fetch(\PDO::FETCH_NUM)) {
            if(!isset($data[$current[0]]['suppliers'][$current[3]])) {
               $data[$current[0]]['suppliers'][$current[3]] = array($current[1] => $current[2]);
               continue;
            }

            $data[$current[0]]['suppliers'][$current[3]][$current[1]] = $current[2];

         }

         unset($suppliers);

         $keys          = array_keys($data);
         $recount_query = '';
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $c = $keys[$i];
            if(count($data[$c]['suppliers']) > 1) {
               $recount_query .= $this->{$ruleID}($data[$c], $c);
            }

            unset($data[$c]);
         }

         if($recount_query) {
            $recount_query_head = "insert into sota_neue_products_values(`ID`, `propertyID`, `baseID`, `value`) values \n";
            $recount_query_end  = "\n ON DUPLICATE KEY UPDATE `value` = values(`value`)";
            $full_recount_query = $recount_query_head . substr($recount_query, 0, strlen($recount_query) - 2) . $recount_query_end;

            file_put_contents(__DIR__ . '/../../../Temp/Export/recount_' . time() . '.sql', $full_recount_query);

            Engine::$dbi->query($full_recount_query);
         }

      }

      /**
       * @param $supplier
       * @param $product
       *
       * @return mixed
       */
      private function mapSupplierToProduct($supplier, $product) {
         $equality = array(
            1  => 1, 2 => 2, 3 => 69, 4 => 3, 6 => 29, 7 => 30, 8 => 6, 9 => 7, 12 => 10, 13 => 11, 14 => 12, 15 => 31, 16 => 32,
            18 => 33, 19 => 34, 20 => 35, 21 => 36, 22 => 37, 23 => 38, 27 => 41, 29 => 44, 30 => 62, 32 => 55, 34 => 63,
            35 => 65, 36 => 66, 37 => 67, 38 => 68,
         );

         if(isset($product['suppliers']))
            unset($product['suppliers']);

         $keys = array_keys($supplier);
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $key = $keys[$i];
//            if(isset($product[$equality[$key]]))
//            if(!isset($product[$equality[$key]])) continue;

            $product[$equality[$key]][0] = $supplier[$key];
         }

         return $product;
      }

      /**
       * @param $recounted_product
       * @param $productID
       *
       * @return string
       */
      private function productToQueryItem($recounted_product, $productID) {
         if(empty($recounted_product))
            return '';

         $result = '';

         $keys = array_keys($recounted_product);
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $current = $recounted_product[$keys[$i]];

            $keys[$i]   = intval($keys[$i]);
            $productID  = intval($productID);
            $current[1] = intval($current[1]);

            $current[0] = Engine::$dbi->quote($current[0]);

            $result .= "({$current[1]}, {$keys[$i]}, {$productID}, {$current[0]}), ";

         }

         return $result;
      }

      private function lowestPrice($products) {
         $keys   = array_keys($products);
         $min    = $products[$keys[0]];
         $minkey = $keys[0];

         foreach($products as $id => $value) $minkey = $value[9] < $min[9] ? $id : $minkey;

         return $products[$minkey];
      }

      private function ruleOne($product, $productID) {
         $suppliers  = $product['suppliers'];
         $keys       = array_keys($suppliers);
         $eights     = array();
         $low_amount = array();

         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $key = $keys[$i];

            if(!isset($suppliers[$key][15]))
               continue;

            if($suppliers[$key][15] >= 8) {
               $eights[$key] = $suppliers[$key];
            } elseif($suppliers[$key][15] > 0) {
               $low_amount[$key] = $suppliers[$key];
            }
         }
         unset($suppliers);

         $keys = array_keys($eights);

         $result = '';

         if(sizeof($eights) == 1) {
            $result = $this->mapSupplierToProduct($eights[$keys[0]], $product);
         } elseif(sizeof($eights) > 1) {
            $result = $this->mapSupplierToProduct($this->lowestPrice($eights), $product);
         } elseif(sizeof($low_amount)) {
            $result = $this->mapSupplierToProduct($this->lowestPrice($low_amount), $product);
         }

         $result = $this->productToQueryItem($result, $productID);

         return $result;
      }

      private function ruleTwo($product, $productID) {
         $suppliers  = $product['suppliers'];
         $keys       = array_keys($suppliers);
         $has_amount = array();

         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $key = $keys[$i];

            if(!isset($suppliers[$key][15]))
               continue;

            if($suppliers[$key][15] > 0) {
               $has_amount[$key] = $suppliers[$key];
            }
         }
         unset($suppliers);

         $result = '';

         if(sizeof($has_amount) > 1) {
            $result = $this->mapSupplierToProduct($this->lowestPrice($has_amount), $product);
         } elseif(sizeof($has_amount) == 1) {
            $keys   = array_keys($has_amount);
            $result = $has_amount[$keys[0]];
         }

         $result = $this->productToQueryItem($result, $productID);

         return $result;
      }

      public function recountAll() {
         $rules = array(
            array('ruleOne', 'propertyID = 70 and value = "k_atyre_auto"'),
            array('ruleTwo', 'propertyID = 70 and value = "Default"'),
         );

         foreach($rules as $rule) {
            $this->recount($rule[0], $rule[1]);
         }
      }
   }
}
