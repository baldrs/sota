/**
  Никто не читает sql файлы

  ЭТОТ SQL ФАЙЛ СГЕНЕРЕН ВРУЧНУЮ И СОБДЕРЖИТ МНОГО <<<НЕНАВИСТИ>>>

  neue = новое, но у меня это кодовая фраза для слова "ТУПОЙ"

 */
drop table if exists
   sota_neue_products_attributes_groups;

create table
   sota_neue_products_attributes_groups (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(255) NOT NULL, -- Название
   `description` varchar(255) NOT NULL, -- Описание группы атрибутов
   `magento_name` varchar(255) NOT NULL, -- название для маженто
   `export` int(1) NOT NULL, -- выгружать или нет в магенто
   `filter_name` varchar(255) NOT NULL, -- название для фильтра
   `use_in_filter` int(1) NOT NULL, -- использовать или нет в фильтре соты
   `sort_order` int(11) NOT NULL,
   PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

drop table if exists sota_neue_products;

create table sota_neue_products (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `typeID` int(11) NOT NULL,   -- тип
   `groupID` int(11) NOT NULL, -- АЙДИШНИК АТТРИБУТОВ
   `name` varchar(255) NOT NULL,  -- Название
   `description` varchar(255),   -- описание атрибута
   `magento_name` varchar(255) NOT NULL,   -- название для маженто
   `to_magento` tinyint(1) NOT NULL DEFAULT 0,   -- выгружать или нет в магенто
   `filter_name` varchar(255) NOT NULL,   -- название для фильтра
   `typeof_filter` tinyint not null,   -- тип для фильтра
   `use_in_filter` tinyint not null default 0,   -- использовать или нет в фильтре
   `sort_order` tinyint not null,   -- sort_order
   PRIMARY KEY (`ID`),
   KEY `secondary` (`typeID`, `groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

drop table if exists sota_neue_suppliers;

create table sota_neue_suppliers (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `typeID` int(11) NOT NULL,   -- тип
   `groupID` int(11) NOT NULL, -- АЙДИШНИК АТТРИБУТОВ
   `name` varchar(255) NOT NULL,  -- Название
   `description` varchar(255),   -- описание атрибута
   `magento_name` varchar(255) NOT NULL,   -- название для маженто
   `to_magento` tinyint(1) NOT NULL DEFAULT 0,   -- выгружать или нет в магенто
   `filter_name` varchar(255) NOT NULL,   -- название для фильтра
   `typeof_filter` tinyint not null,   -- тип для фильтра
   `use_in_filter` tinyint not null default 0,   -- использовать или нет в фильтре
   `sort_order` tinyint not null,   -- sort_order
   PRIMARY KEY (`ID`),
   KEY `secondary` (`typeID`, `groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

drop table if exists sota_neue_categories;

create table sota_neue_categories (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `typeID` int(11) NOT NULL,   -- тип
   `groupID` int(11) NOT NULL, -- АЙДИШНИК АТТРИБУТОВ
   `name` varchar(255) NOT NULL,  -- Название
   `description` varchar(255),   -- описание атрибута
   `magento_name` varchar(255) NOT NULL,   -- название для маженто
   `to_magento` tinyint(1) NOT NULL DEFAULT 0,   -- выгружать или нет в магенто
   `filter_name` varchar(255) NOT NULL,   -- название для фильтра
   `typeof_filter` tinyint not null,   -- тип для фильтра
   `use_in_filter` tinyint not null default 0,   -- использовать или нет в фильтре
   `sort_order` tinyint not null,   -- sort_order
   PRIMARY KEY (`ID`),
   KEY `secondary` (`typeID`, `groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

drop table if exists sota_neue_products_values;

create table sota_neue_products_values (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary` (`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;

drop table if exists sota_neue_suppliers_values;

create table sota_neue_suppliers_values (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary` (`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;

drop table if exists sota_neue_categories_values;

create table sota_neue_categories_values (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary` (`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;

drop table if exists sota_neue_products_values_long;

create table sota_neue_products_values_long (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` text NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary` (`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;

drop table if exists
   sota_neue_suppliers_values_long;

create table
   sota_neue_suppliers_values_long (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` text NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary` (`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;

drop table if exists
   sota_neue_categories_values_long;

create table
   sota_neue_categories_values_long (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` text NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary` (`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;


drop table if exists sota_neue_products_attributes;

create table sota_neue_products_attributes (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(255) NOT NULL, -- Название
   `description` varchar(255), -- описание атрибута
   `typeID` int(11) NOT NULL, -- тип
   `magento_name` varchar(255) NOT NULL, -- название для маженто
   `to_magento` tinyint(1) NOT NULL DEFAULT 0,   -- выгружать или нет в магенто
   `filter_name` varchar(255) NOT NULL, -- название для фильтра
   `typeof_filter` tinyint not null, -- тип для фильтра
   `use_in_filter` tinyint not null default 0, -- использовать или нет в фильтре
   `sort_order` tinyint not null, -- sort_order
   PRIMARY KEY (`ID`),
   KEY(`typeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

drop table if exists
   sota_neue_products_attributes_values;

create table
   sota_neue_products_attributes_values (
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `propertyID` int(11) NOT NULL,
   `baseID` int(11) NOT NULL,
   `value` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `secondary`(`propertyID`, `baseID`)
) ENGINE=MyISAM default CHARSET=utf8;