<?php

namespace System\Application\Controllers\Import;

use System\Core\Engine;

class Pattern {
   public $operators;

   protected $pattern;
   protected $fields;
   protected $table;
   protected $duplicate;
   protected $ready = false;
   /**
    * @var array
    */
   protected $operatorList = array(
      'i' => 'INT', 's' => 'STRING', 'r' => 'REGEXP', '_' => 'IGNORE', 'D' => 'DATE', 'e' => 'EXTRACT',
      'S' => 'SPEC', 'P' => 'PATTERN', 'T' => 'IDOFTYPE', 'Z' => 'ZEROFILL',
   );

   protected $request;
   protected $requestHead;

   #public $request;
   #public $requestHead;

   /**
    * @param $table
    */
   public function __construct($table) {
      $this->operators = new Operators();
      $this->table     = "`$table`";
   }

   /**
    * Sets pattern according to input file
    *
    * The goal is to match every column in row and process it using desired method of processing
    * Available processing methods are registered in $this->operators and are required to be members of
    * Import\Operators class
    *
    * @param      $fields
    * @param      $pattern
    * @param null $duplicate
    *
    * @internal param $data
    */
   public function setPattern($fields, $pattern, $duplicate = null) {
      $this->pattern = $pattern;

      $this->count  = count($fields);
      $this->fields = '(' . implode(',', array_map(function ($arg) {
         return "`$arg`";
      }, $fields)) . ')';

      if(!is_null($duplicate)) $this->duplicate = $this->createDuplicateKeys($duplicate);

      $this->ready = true;

   }

   /**
    * @param $data
    *
    * @throws Exceptions\PatternException
    * @return string
    */
   public function requestFromData($data) {
      if(!$this->fields && !$this->pattern)
         throw new \System\Application\Controllers\Import\Exceptions\PatternException(
            "You have to set pattens and fields in order to use " . __FUNCTION__);

      return "INSERT INTO {$this->table}{$this->fields} VALUES " . $this->map($data) . ' ' . ($this->duplicate ? : '');

   }

   /**
    * @param null $table
    *
    * @return Pattern
    */
   public function requestStart($table = null) {
      if(!$table && $this->table)
         $this->requestHead = "INSERT INTO {$this->table}{$this->fields} VALUES ";
      else {
         $this->requestHead = "INSERT INTO `{$table}`{$this->fields} VALUES ";
      }

      return $this;
   }

   /**
    * @param      $data
    * @param null $fn
    *
    * @throws Exceptions\PatternException
    */
   public function appendRequest($data, $fn = null) {
      $s = '';
      if($data !== false)
         $s = $this->map($data);

      /** @var $fn callback */
      if(is_array($s) && is_callable($fn)) {
         $s = $fn($s);
         if(!is_string($s))
            throw new \System\Application\Controllers\Import\Exceptions\PatternException(
               "Callback supplied to " . __METHOD__ . " should return string value");

      } elseif(is_array($s)) {
         $s = '(' . implode(',', array_slice($s, 0, $this->count)) . ')';
      } /*else {
         throw new \ErrorException("AN FATAL ERROR OCCURRED WITH THE DATA SUPPLIED");
      }*/

      if($s && !empty($s) && strlen($s) && $s != '()')
         $this->request .= $s . ",\n";
      unset($s);
   }

   /**
    * @param $data
    *
    * @throws Exceptions\PatternException
    * @return string
    */
   public function map($data) {
      if(!$data) return '';
      // if(count($data) != substr_count($this->pattern, '%'))
      //  throw new \Exception('wrong number of fields or incompatible data');
      // cast data to pattern

      $i = -1;
      $s = array();
      $c = 0;
      $this->operators->setData($data);
      while(($i = strpos($this->pattern, '%', $i + 1)) !== false) {
         $char = $this->pattern[$i + 1];
         $add  = null;
         if(!in_array($char, array_keys($this->operatorList)))
            throw new \System\Application\Controllers\Import\Exceptions\PatternException("Unknown operator $char");

         if($this->pattern[$i + 2] == ':') { // pattern handler argument ends at next space
            $end = (strpos($this->pattern, ' ', $i + 3) !== false ?
               strpos($this->pattern, ' ', $i + 3) : strlen($this->pattern)) - ($i + 3);

            if($end > strlen($this->pattern) || !$end)
               $add = substr($this->pattern, $i + 2);
            else
               $add = substr($this->pattern, $i + 3, $end);

            if(strpos($add, ':') != false)
               $add = explode(':', $add);
         }

         if($char != '_') {
            try {
               $s[] = $this->operators->{$this->operatorList[$char]}($data[$c], $add);
            } catch(\System\Application\Controllers\Import\Exceptions\PatternException $e) {
               return '';
            }
         }
         $c++;
      }

      $s = Engine::flatten($s);
      if(count($s) < $this->count) {
         $s = array_merge($s, array_fill(0, $this->count - count($s), '""'));
      } elseif(count($s) > $this->count) {
         // $s = array_slice($s, 0, $this->count);
         return $s;
      }

      if($s && count($s))
         return '(' . implode(',', $s) . ')';
      else
         return '';
   }

   /**
    * @param $data
    *
    * @return string
    */
   private function createDuplicateKeys($data) {
      $result  = " ON DUPLICATE KEY UPDATE ";
      $aResult = array();
      for($i = 0, $l = count($data); $i < $l; $i++)
         $aResult[] = "`$data[$i]` = values(`$data[$i]`)";

      $result .= implode(', ', $aResult);
      unset($aResult);

      return $result;
   }

   protected $finished = false;
   protected $count;
   protected $file;

   public function readLine() { return fgetcsv($this->file, 0, ';'); }

   /**
    * @return Pattern
    */
   public function finishRequest() {
      if(!$this->finished) {
         $this->request  = substr($this->request, 0, strlen($this->request) - 2);
         $this->finished = true;
      }

      return $this;
   }

   /**
    * @return mixed
    */
   public function executeRequest() {
      $result = Engine::$dbi->query($this->requestHead . $this->request . ($this->duplicate ? : ''));
      unset($this->requestHead, $this->request, $this->duplicate);

      return $result;
   }

   /**
    * @param null $table
    *
    * @return Pattern
    */
   public function purge($table = null) {
      if($table)
         Engine::$dbi->query('truncate table ' . $table);
      else
         Engine::$dbi->query('truncate table ' . $this->table);

      return $this;
   }

   public function open($file) {
      if($this->file)
         fclose($file);

      $this->file = fopen($file, 'r');
   }

   public function execute() {
      if(!$this->ready)
         throw new  \Exception('You must set pattern first to use ' . __METHOD__);

      while(($row = $this->readLine()) !== false)
         $this->appendRequest($row);
   }

   public function close() { fclose($this->file); }

}

