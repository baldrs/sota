<?php
namespace System\Application\Controllers\Import;

use \System\Core\Engine;

class Operators {
   /**
    * @var
    */
   protected $regexp;
   private $data;

   private static $zfp;
   private static $type;

   public static $query;
   /**
    * @var null|\stdClass
    */
   private static $patterns = null;
   private static $head;

   //private static $properties = array();

   /**
    * @param $regex
    */
   public function setRegexp($regex) { $this->regexp = $regex; }

   /**
    * @param      $value
    * @param null $argument
    *
    * @throws Exceptions\OperatorException
    * @internal param $data
    * @return array
    */
   public function REGEXP($value, $argument = null) {
      if(preg_match($this->regexp, $value, $aMatches)) {
         $aMatches = array_map(function ($argument) {
            if(is_numeric($argument)) return $argument;
            else return Engine::$dbi->quote($argument);
            //'"' . Engine::$db->escape($argument) . '"';
         }, array_values(array_filter($aMatches, function ($argument) {
            return is_numeric($argument[0]) && !!$argument;
         })));

         return $aMatches; //implode(',', $aMatches);
      }

      throw new \System\Application\Controllers\Import\Exceptions\OperatorException("Incompatible input string found " . $value);
      //return '';
   }

   /**
    * @param                   $value
    * @param null|array|string $argument order is length prefix source
    *
    * @return string
    */
   public function ZEROFILL($value, $argument = null) {
      $length = $argument[0];
      $source = isset($argument[1]) ? $this->data[$argument[1]] : $value;

      $prefix = Operators::getZfp();

      return Engine::$dbi->quote($prefix . str_pad($source, $length, '0', STR_PAD_LEFT));
   }

   /**
    * @param null $value
    * @param      $argument
    *
    * @return string
    */
   public function IGNORE($value = null, $argument = null) { return ''; }

   /**
    * @param $value
    * @param $argument
    *
    * @return int
    */
   public function EXTRACT($value, $argument = null) {
      if(!trim($value)) {
         return $this->data[7] == 'Есть' ? 1 : 0;
      }
      if(trim($value) == '+') return 20;

      $value = preg_replace("/[^0-9]/", '', $value);

      if(is_numeric($value)) return intval($value);

      return 0;

   }

   /**
    * @param $value
    * @param $argument
    *
    * @throws \Exception
    * @return int
    */
   public function INT($value, $argument = null) {
      if(!is_numeric($value))
         throw new \Exception("$value is NaN");

      return intval($value);
   }

   /**
    * @param $value
    * @param $argument
    *
    * @return string
    */
   public function STRING($value, $argument = null) { return Engine::$dbi->quote($value); }

   /**
    * @param $value
    * @param $argument
    *
    * @return string
    */
   public function DATE($value, $argument = null) {
      return $this->STRING(date('Y-m-d', strtotime(str_replace('/', '-', $value))));
   }

   /**
    * @param      $value
    * @param null $argument
    *
    * @return string
    */
   public function SPEC($value, $argument = null) {
      $target = $this->data[$argument];

      $target = array_map('trim', explode("/", $target));

      if(in_array($target[2], array('Шины', 'Диски')))
         return $this->STRING("$target[5] $target[6] $value");

      return $this->STRING($value);
   }

   public function setData($data) {
      $this->data = $data;
   }

   public function IDOFTYPE($value, $argument = null) {
////      $type = array_map('trim', explode(".", $this->data[$argument]));
////      $type = self::$patterns['types'][$type[3]] ? : (self::$patterns['types'][$type[1]] ? : 0); // 0 is unknown type, thus no filter is going to find them
//      $type = array_search($value, self::$patterns['types_text']) ? : 0;
//
//      //    self::setType($type);
//      return $type;
      $types = self::$patterns['types'];

      $value = $this->data[$argument];
      $value = explode('.', $value);

      foreach(array_keys($types) as $type) {
         list($typeID, $position) = explode('_', $type);
         if($value[$position] == $typeID)
            return $types[$type];
      }

      return 0;
   }

   public static function start() {
      self::$head = "INSERT INTO sota_propertiesValues(productID, propertyID, value) VALUES ";
   }

   public static function finish( /*$purged = false*/) {
      self::$query = substr(self::$query, 0, strlen(self::$query) - 2);
//      if(!$purged || $purged == 'false')
//         Engine::$dbi->query('truncate table sota_propertiesValues');
      if(self::$query)
         Engine::$dbi->query(self::$head . self::$query);
   }

   public function PATTERN($value, $argument = null) {
      $_value = $value;

      $type = Operators::getType();

      $productID = (int) str_replace('\'', '', $this->ZEROFILL($this->data[0], array(7)));

      $pattern = $this->getPattern($type);

      if($pattern['regexp']) {
         preg_match_all($pattern['regexp'], $value, $matches, PREG_SET_ORDER);

         if(count($matches)) {
            $matches = $matches[0];
            unset($matches[0]);
            $matches = array_values($matches);
//            foreach ($pattern->properties as $index => $propertyID) {
            for($index = 0, $l = sizeof($pattern['properties']); $index < $l; $index++) {
               $propertyID = $pattern['properties'][$index];
               if(isset($pattern['special'])) {
                  if($pattern['special'][$propertyID]) {
                     if(strpos($matches[$index], $pattern['special'][$propertyID]['match']) !== false) {
                        if($pattern['special'][$propertyID]['do'] == 'replicate') {
                           $tmp = explode($pattern['special'][$propertyID]['match'], $matches[$index]);
                           foreach($tmp as $value)
                              self::push(array($productID, $propertyID, $value));
                        }
                     } else self::push(array($productID, $propertyID, $matches[$index]));
                  } else self::push(array($productID, $propertyID, $matches[$index]));
               } else self::push(array($productID, $propertyID, $matches[$index]));
            }
            $value = substr($value, strlen($matches[0]));
         }
      }

      if($pattern['variable']) {
         foreach($pattern['variable'] as $key => $example) {
            if(!$example)
               continue;

            if(strpos($example, ' present?') !== false) {
               $raw      = explode(' present?', $example);
               $variants = explode('|', $raw[0]);
               for($i = 0, $l = sizeof($variants); $i < $l; $i++) {
                  $e = $variants[$i];
                  if(stripos($value, $e) !== false) {
                     self::push(array($productID, $key, 'true'));
                     break;
                  }
               }
            } elseif(strpos($example, ' multiple!') !== false) {
               $raw    = explode(' multiple!', $example);
               $values = explode('|', $raw[0]);

               for($i = 0, $l = sizeof($values); $i < $l; $i++) {
                  $e = $values[$i];
                  if(stripos($value, $e) !== false) {
                     self::push(array($productID, $key, $e));
                  }
               }

            } elseif(strpos($example, '|') !== false) {
               $example = explode('|', $example);
               for($i = 0, $l = sizeof($example); $i < $l; $i++) {
                  $e = $example[$i];
                  if(stripos($value, $e) !== false) {
                     self::push(array($productID, $key, $e));
                     break;
                  }
               }
            } else if(strpos($value, $example) !== false) {
               self::push(array($productID, $key, 'true'));
            } else {
               continue;
            }
         }
      }

      if($pattern['column']) {
         foreach($pattern['column'] as $key => $col) {
            $current = $this->data[$key];
            $rules   = explode('|', $col);

            list($function, $arguments, $index, $propertyID) = $rules;

            $arguments = strpos($arguments, ':') !== false ? array_push(explode(':', $arguments), $current) :
               array($arguments, $current);

            $result = call_user_func_array($function, $arguments);

            self::push(array($productID, $propertyID, trim($result[$index])));
            unset($result, $arguments, $current, $rules);
         }
      }

      return $this->SPEC($_value, 8);
   }

   private function getPattern($type) {

      $types = array('', 'shini', 'diski', 'gshini', '');
      if(!is_null(self::$patterns)) {
         if($type == 'auto') {
            return self::$patterns['auto'][$types[$this->IDOFTYPE($this->data[15], 9)]];
         }

         return isset(self::$patterns[$type]) ? self::$patterns[$type] : array();

      }

      $path = __DIR__ . '/Import.yml';
      if(file_exists($path)) {
         self::$patterns = \System\Lib\Yaml\Yaml::parse(file_get_contents($path));
         self::$patterns = self::$patterns['operators'];
         if($type == 'auto') {
            return self::$patterns['auto'][$types[$this->IDOFTYPE($this->data[15], 9)]];
         }
      }

      return isset(self::$patterns[$type]) ? self::$patterns[$type] : array();
   }

   private static function push($array) {
      $array[2] = Engine::$dbi->quote($array[2]);
      self::$query .= '(' . implode(',', $array) . '), ';
   }

   public static function getZfp() {
      return self::$zfp;
   }

   public static function setZfp($zfp) {
      self::$zfp = $zfp;
   }

   public static function getType() {
      return self::$type;
   }

   public static function setType($type) {
      self::$type = $type;
   }
}

