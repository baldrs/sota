<?php

namespace System\Application\Controllers\Import;

use System\Core\Engine;

class Import {
   protected $filePtr;

   protected $db;

   /**
    * @var array
    */
   protected $config;

   public function __construct($file = null) {
      \setlocale(LC_ALL, 'ru_RU.UTF-8');

      $this->config = \System\Lib\Yaml\Yaml::parse(file_get_contents(__DIR__ . '/Import.yml'));
      $this->config = $this->config['import'];

      if($file)
         $this->openCSV($file);
   }

   public function readLine() { return fgetcsv($this->filePtr, 0, ';'); }

   public function importCategories() {
      $pattern = new Pattern('sota_categories');
      $pattern->setPattern(array('ID', 'parent', 'name'), '%i %i %s');

      $pattern->requestStart();

      while(($row = $this->readLine()) !== false)
         $pattern->appendRequest($row);

      $pattern->purge()->finishRequest()->executeRequest();
      unset($pattern);
   }

   public function importProducts() {
      $products = new Pattern('sota_products');
      $products->setPattern(
         array(
              'ID', 'categoryID', 'supplierID', 'vendorID', 'vendor', 'supplier', 'supplierCity', 'name',
              'path', 'numericPath', 'availability', 'inprice', 'price', 'amount', 'update', 'typeID', 'sku'
         ),
         '%Z:7:9 %i %i %i %s %s %s %P:9 %s %s %s %i %i %i %D %T:9 %Z:7:9:0'
      );
      $products->requestStart();

      Operators::start();

      while(($row = $this->readLine()) !== false)
         $products->appendRequest($row);

      Operators::finish();

      $products->purge()->finishRequest()->executeRequest();
      unset($products);
   }

   public function importSuppliers() {
      $pattern = new Pattern('sota_suppliers');
      $pattern->setPattern(
         array(
              'productID', 'supplierID', 'categoryID', 'vendorID', 'name', 'vendor', 'supplier',
              'supplierCity', 'availability', 'additionalInfo', 'currency', 'rawPrice', 'inprice',
              'price', 'amount', 'update', 'cashless', 'deliverers',
         ),
         '%Z:7:9 %i %i %i %s %s %s %s %s %s %s %i %i %i %e %D %s %s'
      );

      while(($row = $this->readLine()) !== false)
         $pattern->appendRequest($row);

      $pattern->purge()->requestStart()->finishRequest()->executeRequest();
      unset($pattern);
   }

   public function importFile($file, $filetype) {
      $settings = $this->config[$filetype];

      $pattern = new Pattern($settings['table']);

      $pattern->setPattern($settings['fields'], $settings['pattern']);

      $info = explode('_', pathinfo($file, PATHINFO_FILENAME));

      Operators::setType($info[1]);
      Operators::setZfp($info[2]);

      $pattern->open(__DIR__ . '/../../Temp/' . $file);

      $pattern->requestStart();

      if($settings['hasproperties'])
         Operators::start();

      $pattern->execute();
      $pattern->finishRequest()->executeRequest();

      $pattern->close();

      if($settings['hasproperties'])
         Operators::finish();
   }

   public function countFiles($match) {
      $path = __DIR__ . '/../../Temp';
      $dir  = scandir($path);

      $result = array();

      $count = sizeof($dir);

      for($counter = 0; $counter < $count; $counter++) {
         $current = $dir[$counter];
         if(is_dir($path . '/' . $current)) continue;

         if(strpos($current, $match) !== false) $result[] = $current;
      }

      return $result;
   }

   public function simultaneous($type) {
      $files = $this->countFiles($type);

      if(!sizeof($files))
         return false;

      if(sizeof($files) == 1) {
         $this->importFile($files[0], $type);

         return true;
      }

      $threads = new \System\Core\CURLThreads();

      for($i = 0, $l = sizeof($files); $i < $l; $i++) {
         $current = $files[$i];
         \System\Core\Log::write('http://' . $_SERVER['HTTP_HOST'] . '/importSubThread/' . $current . '/' . $type . '/');
         $threads->add('http://' . $_SERVER['HTTP_HOST'] . '/importSubThread/' . $current . '/' . $type . '/');
      }

      $r = $threads->exec();
      foreach($r as $t) Engine::out($t['response']);

      return true;
   }

   public function openCSV($file) {
      if($this->filePtr)
         fclose($this->filePtr);

      if(file_exists($file))
         $this->filePtr = fopen($file, 'r');
      else
         trigger_error("File $file does not exists.", E_USER_ERROR);
   }

   public static function clearTables() {
      $config       = \System\Lib\Yaml\Yaml::parse(file_get_contents(__DIR__ . '/Import.yml'));
      $cleaningList = $config['cleaningList'];
      foreach($cleaningList as $scheduled)
         Engine::$dbi->query("truncate table `$scheduled`");
   }
}

