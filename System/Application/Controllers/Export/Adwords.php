<?php

namespace System\Application\Controllers\Export {
   use System\Core\Engine;

   class Adwords extends BaseCsv {
      public function __construct() {
         $config = \System\Lib\Yaml\Yaml::parse(file_get_contents(__DIR__ . '/Adwords.yml'));
         \System\Lib\Yaml\Yaml::dump($config);
         $this->config = $config['keywords']['rules'];
      }

      public function glue($a1, $a2) {
         foreach($a2 as $k => $e)
            $a1[$k] = $e;

         return $a1;
      }

      //const TOKEN_MODIFIER = '#\{[a-z0-9_]+\|?[a-z0-9_-]+?:?[a-z]+?}#i';
      const TOKEN_MODIFIER = "#\{[a-z0-9_]+\|?[a-z0-9_-]*((:[a-z0-9_-]+|(?R))*)*}#ix";

      private $head = "\xEF\xBB\xBFCampaign,Ad Group,Keyword,Criterion Type,Destination URL,Campaign Status,AdGroup Status,Status";
      private $head_adgroup = "\xEF\xBB\xBFCampaign,Ad Group,Headline,Description Line 1,Description Line 2,Display URL,Destination URL,Campaign Status,AdGroup Status,Status";

      private function prepareData($source = 'summer') {
         /*         ini_set('error_reporting', E_ALL);
                  ini_set('display_errors', 1);
                  echo "<pre>";*/
         $seasons = array('summer' => 'Летние шины', 'winter' => 'Зимние шины');
//         $products = Engine::$dbi->query('select ID, categoryID, sku from products where typeID = 1')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $id_list = implode(',',
            Engine::$dbi
               ->prepare(
               'select PV.productID from sota_propertiesValues PV
                left join sota_products P on P.ID = PV.productID
                where PV.propertyID = 7 and PV.value = ? and P.amount > 0')
               ->execute(array($seasons[$source]))->getStatement()->fetchAll(\PDO::FETCH_COLUMN));

         $_categories = Engine::$dbi
            ->query(
            'select C1.name, C2.name as vendor, P.sku
            from sota_categories C1
            left join sota_categories C2 on C1.parent = C2.ID
            left join sota_products P on C1.ID = P.categoryID
            where P.ID in (' . $id_list . ')')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $categories = array();
         for($i = 0, $l = sizeof($_categories); $i < $l; $i++)
            $categories[$_categories[$i][2]] = array('model' => $_categories[$i][0], 'vendor' => $_categories[$i][1]);

         unset($_categories);

         $_properties = Engine::$dbi
            ->query('select PV.value, P.sku, PP.magento from sota_propertiesValues PV
            left join sota_properties PP on PP.ID = PV.propertyID
            left join sota_products P on P.ID = PV.productID
         where PV.productID in (' . $id_list . ')')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $properties = array();
         for($i = 0, $l = sizeof($_properties); $i < $l; $i++) {

            if(!isset($properties[$_properties[$i][1]])) {
               $properties[$_properties[$i][1]] = array($_properties[$i][2] => $_properties[$i][0]);
               continue;
            }

            $properties[$_properties[$i][1]][$_properties[$i][2]] = $_properties[$i][0];
         }

         unset($_properties);

         //foreach($properties as $sku => $array) {
         $keys = array_keys($properties);
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $sku              = $keys[$i]; //$array = $properties[$sku];
            $properties[$sku] = $this->glue($properties[$sku], $categories[$sku]);
         }

         unset($categories);
         /*       $sku_list = implode(',',Engine::$dbi->query('select sku from sota_products where ID in ('.$id_list.')')->getStatement()->fetchAll(\PDO::FETCH_COLUMN));

                  $connector = new \System\Core\Magento\Connector();*/

         /*         $_url_list = $connector->db->query(
                     'select CUR.request_path, CPE.sku from core_url_rewrite CUR
                     left join catalog_product_entity CPE on CPE.entity_id = CUR.product_id
                     where sku in (' . $sku_list . ') and id_path = concat("product/", product_id) group by CUR.product_id')->getStatement()->fetchAll(\PDO::FETCH_NUM);

                  $url_list = array();
                  for($i = 0, $l = sizeof($_url_list); $i < $l; $i++)
                     $url_list[$_url_list[$i][1]] = 'http://kolesiko.ua/'.$_url_list[$i][0];

                  unset($_url_list);*/
         $_url_list = Engine::$dbi->query('select sku, path from sota_products where ID in (' . $id_list . ')')->getStatement()->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_GROUP);

         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $raw = $_url_list[$keys[$i]][0];
            $raw = explode(' / ', $raw);
            $raw = array_filter($raw);
            array_shift($raw);
            array_unshift($raw, 'http://kolesiko.ua');

            $raw[sizeof($raw) - 1] = strtolower($raw[sizeof($raw) - 1]) . '.html';
            $raw[sizeof($raw) - 2] = strtolower($raw[sizeof($raw) - 2]);

            $properties[$keys[$i]]['url'] = str_replace(' ', '-', implode('/', $raw));
         }

         unset($_url_list); //print_r($properties);

         return $properties;
      }

      private function _modifier($matched, $modifier, $arguments, $data) {
         $parts = array('w' => 'ширина-профиля/k_atyre_car_width', 'h' => 'высота-профиля/k_atyre_car_height', 'd' => 'диаметр/k_atyre_car_diameter');
         switch($modifier) {
            case 'no-spaces':
               $matched = str_replace(' ', '_', $matched);
               break;
            case 'tolower':
               $matched = strtolower($matched);
               break;
            case 'ucfirst':
               $matched = ucfirst($matched);
               break;
            case 'no-radius':
               $matched = str_replace(array('R', 'r'), '', $matched);
               break;
            case 'size-url':
               if(!isset($arguments)) {
                  $_d      = $data;
                  $repl    = "/где/диаметр/k_atyre_car_diameter/высота-профиля/k_atyre_car_height/ширина-профиля/k_atyre_car_width.html";
                  $repl    = str_replace(array_keys($_d), array_values($_d), $repl);
                  $matched = str_replace('.html', $repl, $matched);
               } elseif($arguments[0]) {
                  $args = str_split($arguments[0]);
                  $res  = array();
                  foreach($args as $a)
                     $res[] = str_replace(array_keys($data), array_values($data), $parts[$a]);
                  $res[sizeof($res) - 1] = $res[sizeof($res) - 1] . '.html';
                  if(isset($arguments[1]) && $arguments[1] == 'strip-vm') {
                     $matched = explode('/', $matched);

                     array_pop($matched);
                     array_pop($matched);

                     $matched = implode('/', $matched) . '/где/' . implode('/', $res);

                  } else
                     $matched = str_replace('.html', '/где/' . implode('/', $res), $matched);
               }

               break;
         }

         return $matched;
      }

      public function generate($season = 'summer') {
         $data = $this->prepareData($season);

         $template = explode("\n", file_get_contents(__DIR__ . '/Adwords.tpl'));

         $adgroups = explode("\n", file_get_contents(__DIR__ . '/Adwords_adgroups.tpl'));

         $result          = $this->head . "\n";
         $adgroups_result = $this->head_adgroup . "\n";
         $keys            = array_keys($data);
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $c                  = $keys[$i];
            $data[$c]['season'] = $season;
            for($j = 0, $m = sizeof($template); $j < $m; $j++) {
               $line = $template[$j];
               preg_match_all(self::TOKEN_MODIFIER, $line, $matches);
               $matches = $matches[0];
               for($k = 0, $n = sizeof($matches); $k < $n; $k++) {
                  $original = $matches[$k];
                  $match    = trim(str_replace(array('{', '}'), '', $matches[$k]));

                  if(strpos($match, '|') !== false) { //
                     list($match, $modifier) = explode('|', $match);
                     $args = array();
                     if(strpos($modifier, ':') !== false) {
                        $modifier = explode(':', $modifier);
                        $args     = $modifier;
                        $modifier = array_shift($args);
                     }
                     $matched = $data[$c][$match];

                     $matched = $this->_modifier($matched, $modifier, $args, $data[$c]);
                  } else
                     $matched = $data[$c][trim(str_replace(array('{', '}'), '', $matches[$k]))];

//                  echo $original, $matched, $line, "\n";
                  $line = str_replace($original, $matched, $line);
               }
               $result .= $line . "\n";
            }

            for($f = 0, $o = sizeof($adgroups); $f < $o; $f++) {
               $line = $adgroups[$f];
               preg_match_all(self::TOKEN_MODIFIER, $line, $matches);
               $matches = $matches[0];
               for($g = 0, $p = sizeof($matches); $g < $p; $g++) {
                  $original = $matches[$g];
                  $match    = trim(str_replace(array('{', '}'), '', $matches[$g]));

                  if(strpos($match, '|') !== false) { //
                     list($match, $modifier) = explode('|', $match);
                     $args = array();
                     if(strpos($modifier, ':') !== false) {
                        $modifier = explode(':', $modifier);
                        $args     = $modifier;
                        $modifier = array_shift($args);
                     }
                     $matched = $data[$c][$match];

                     $matched = $this->_modifier($matched, $modifier, $args, $data[$c]);
                  } else
                     $matched = $data[$c][trim(str_replace(array('{', '}'), '', $matches[$g]))];

//                  echo $original, $matched, $line, "\n";
                  $line = str_replace($original, $matched, $line);
               }
               $adgroups_result .= $line . "\n";
            }
//            if($i == sizeof($template)+1) break;
         }
         //echo $result;
         $result          = implode("\n", array_unique(explode("\n", $result)));
         $adgroups_result = implode("\n", array_unique(explode("\n", $adgroups_result)));

         file_put_contents("../System/Temp/Export/adwords_$season.csv", $result);
         file_put_contents("../System/Temp/Export/agroups_$season.csv", $adgroups_result);

      }

      public function mapto($dataitem, $season, $current) {
         $patterns = $this->config[$season][$current];
         $result   = '';
         if(is_array($patterns[0])) {
            return $result;
         }

         for($i = 0, $l = sizeof($patterns); $i < $l; $i++) {
            $current = 0;
         }

         return $result;
      }
   }
}