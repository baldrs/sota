<?php

namespace System\Application\Controllers\Export {
   class CsvIO implements \System\Core\Interfaces\Export\GenericCsv {

      private $file = null;
      private $delimiter;
      private $enclosure;

      /**
       * @param string $delimiter
       * @param string $enclosure
       */
      public function __construct($delimiter = ';', $enclosure = '"') {
         $this->setDelimiter($delimiter);
         $this->setEnclosure($enclosure);
      }

      /**
       * @param string $file
       * @param string $mode
       */
      public function open($file, $mode = 'w') {
         if($this->file)
            fclose($this->file);

         $this->file = fopen($file, $mode);
      }

      /**
       * @param $line
       */
      public function write($line) {
         $d = $this->getDelimiter();
         $e = $this->getEnclosure();

         $line = array_map(function ($element) use ($e) { return $e . $element . $e; }, $line);

         $line = implode($d, $line) . "\n";

         fputs($this->file, $line, strlen($line));
      }

      /**
       *
       */
      public function read() {
         $line = fgets($this->file, 8192);
         $line = explode($this->getDelimiter(), $line);

         $enclosure = $this->getEnclosure();

         $line = array_map(function ($e) use ($enclosure) {
            $e = trim($e);
            if($e[0] == $enclosure) {
               return substr($e, 1, strlen($e) - 2);
            }

            return $e;
         }, $line);

         return $line;
      }

      public function close() {
         fclose($this->file);
      }

      private function setDelimiter($delimiter) {
         $this->delimiter = $delimiter;
      }

      private function setEnclosure($enclosure) {
         $this->enclosure = $enclosure;
      }

      public function getDelimiter() {
         return $this->delimiter;
      }

      public function getEnclosure() {
         return $this->enclosure;
      }
   }
}
