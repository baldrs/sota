<?php

namespace System\Application\Controllers\Export {
   use System\Core\Engine;

   abstract /* I meant static */
   class Helpers {
      public static function  availToInt($arg) {
         return trim($arg) == 'Нет' || trim($arg) == 'Под заказ' ? 0 : 1;
      }

      public static function zeroFill($string, $length) {
         $string = preg_replace('/[^0-9]/', '', trim($string));

         if(strlen($string) < $length) {
            $string = str_pad($string, $length, '0', STR_PAD_LEFT);
         }

         return $string;
      }

      public static function dasherize($string) {
         return preg_replace('/[^a-zа-я0-9]/ui', '-', trim(str_replace('  ', ' ', $string)));
      }

      public static function nofirst($string, $separator) {
         return str_replace(" $separator ", $separator, trim(substr($string, strpos($string, $separator) + 1)));
      }

      public static function intval($val) {
         return intval($val);
      }

      public static function minSale($amount, $what) {
         if(!$amount)
            return $what == 'use' ? 1 : 1;

         $qty = intval($amount) % 2;

         if(!$qty) {
            return ($what == 'use') ? 0 : ($amount >= 8 ? 2 : 4);
         } else {
            return $what == 'use' ? 1 : 1;
         }
      }

      public static function test() {
         echo self::zeroFill("20", 7) . '<br/>';
         echo self::availToInt("Нет") . '<br/>';
         echo self::dasherize("a ds 59 (*&&($^# 13 - = [[[[] ]]]]df a////") . '<br/>';
         echo self::nofirst("NADA / ERES / HRONIKA", '/');

      }

      public static function __callStatic($fn, $args) {
         trigger_error('Undefined static method ' . $fn, E_USER_ERROR);
      }
   }
}
