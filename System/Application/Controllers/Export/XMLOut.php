<?php
/**
 * Created by JetBrains PhpStorm.
 * User: baldrs
 * Date: 05.02.13
 * Time: 12:19
 * To change this template use File | Settings | File Templates.
 */

namespace System\Application\Controllers\Export {
   class XMLOut {

      public function __construct($options = array()) {

         $this->options = array_merge($this->options, $options);

         if($this->options['file'])
            $this->setFile($this->options['file']);
         $this->connector = new \System\Core\Magento\Connector();
      }

      /**
       * @var \System\Core\Magento\Connector
       */
      private $connector;

      private $file;

      private $options = array(
         'patterns' => array(
            'nadavi'  => array(
               5    => '{vendor} {name}',
               7301 => '{vendor} {name} ({2}/{1} {3} {4}{5})', // width/height diameter {load index}{speed index}
               7304 => '{vendor} {name} ({13}x{10}/{11} ET{12} DIA{14})', // w r pcd et dia
            ),
            'hotline' => array(
               5    => '{vendor} {name}',
               7301 => '{vendor} {name} ({2}/{1} {3} {4}{5})', // width/height diameter {load index}{speed index}
               7304 => '{vendor} {name} ({10} W{13} PCD{11} ET{12} DIA{14})', //r w pcd et dia
            ),
            5         => '{vendor} {name}',
            7301      => '{vendor} {name} ({2}/{1} {3} {4}{5})', // width/height diameter {load index}{speed index}
            7304      => '{vendor} {name} ({10} W{13} PCD{11} ET{12} DIA{14})', //r w pcd et dia
         ),
      );

      public function strip($string) {
         return preg_replace('#[^a-zа-яёїґ0-9,. ()_\'\\/""-]#iu', '', $string);
      }

      public static function writeArrayRecursive($array, $d = 0) {
         //if($d == 2) return;
         $o      = '';
         $indent = str_repeat(' ', ($d + 1) * 3);
         foreach($array as $k => $v) {
            if(strpos($k, '-') !== false) {
               $k = explode('-', $k);
               $k = $k[0];
            }

            $o .= "$indent<$k>";
            if(is_array($v)) {
               $o .= "\n";
               $o .= self::writeArrayRecursive($v, $d + 1);
            } elseif(ctype_digit($v) || is_float($v)) {
               $o .= $v;
            } else
               $o .= '<![CDATA[' . htmlentities($v, ENT_XML1 | ENT_COMPAT, 'UTF-8') . ']]>';

            if(!is_array($v))
               $o .= "</$k>\n";
            else
               $o .= "$indent</$k>\n";
         }

         return $o;
      }

      /**
       * @param array   $array
       * @param integer $patternID
       * @param string  $type
       *
       * @return mixed
       */
      public function map($array, $patternID, $type = 'default') {
         if($type != 'default')
            $pattern = $this->options['patterns'][$type][$patternID];
         else
            $pattern = $this->options['patterns'][$patternID];

         $array = array_flip(array_map(function ($e) { return '{' . $e . '}'; }, array_flip($array)));

         if($type == 'nadavi')
            if(isset($array['{10}']))
               $array['{10}'] = preg_replace('#[^0-9.]#', '', $array['{10}']);

         return str_replace(array_keys($array), array_values($array), $pattern);
      }

      public function glue($a1, $a2) {
         foreach($a2 as $k => $e)
            $a1[$k] = $e;

         return $a1;
      }

      public function yandex() {
         $this->_yandex(array(1, 2, 3), '../kolesiko.ua/yandex.xml');
      }

      public function nadavi() {
         $this->_yandex(array(1, 2, 3), '../kolesiko.ua/nadavi.xml', array('categories' => 'catalog', 'item' => 'item', 'picture' => 'image'));
      }

      public function technoportal() {
         $this->_yandex(array(1, 2, 3), '../kolesiko.ua/technoportal.xml');
      }

      public function vcene() {
         $this->_yandex(array(1, 2, 3), '../kolesiko.ua/vcene.xml');
      }

      public function freemarket() {
         $this->_yandex(array(1, 2, 3), '../kolesiko.ua/freemarket.xml');
      }

      public function hotline($types = array(1, 2, 3), $file = '../kolesiko.ua/hotline.xml') {
         $this->_prepareFile($file);

         $ptid = array(1 => 7301, 2 => 7304, 3 => 5);

         $this->_xmlHead();
         $this->put('<price>');
         $this->put(
            self::writeArrayRecursive(
               array(
                    'date'     => date('Y-m-d H:i'),
                    'firmName' => 'Kolesiko.ua',
                    'firmId'   => '3449',
                    'rate'     => '8.13'
               )));
         $categories = array(
            'categories' => array(
               'category-0' => array('id' => 1, 'name' => "Шины и диски"),
               'category-1' => array('id' => 2, 'parentId' => 1, 'name' => "Автошины"),
               'category-2' => array('id' => 3, 'parentId' => 1, 'name' => 'Колесные диски'),
               'category-3' => array('id' => 4, 'name' => "Сантехника"),
            )
         );

         $this->put(self::writeArrayRecursive($categories));

         $this->put('<items>');

         $types = implode(',', $types);

         $urls = $attrs = $cats = $imgs = null;
         extract($this->gather($types));

         $type = $this->_detectType($file);

         $products = \System\Core\Engine::$dbi->query("select distinct sku, categoryID, ID, typeID, price from sota_products where typeID in ($types) and price > 0")->result();
         for($i = 0, $l = sizeof($products); $i < $l; $i++) {
            $row = $products[$i];

            $model = $this->strip($this->map($this->glue($attrs[$row->ID], $cats[$row->categoryID]), $ptid[$row->typeID], $type));
            $this->put(
               sprintf(
                  '<item><id>%s</id><categoryId>%d</categoryId><code>%s</code><vendor><![CDATA[%s]]></vendor>
                  <name><![CDATA[%s]]></name><description><![CDATA[]]></description><url><![CDATA[http://kolesiko.ua/%s]]></url>
                  <image><![CDATA[%s]]></image><priceRUAH>%d</priceRUAH><stock><![CDATA[Есть]]></stock>
                  <guarantee><![CDATA[Есть]]></guarantee></item>',
                  $row->sku, $row->typeID + 1, $row->sku, $cats[$row->categoryID]['vendor'], $model,
                  $urls[$row->sku] . $this->_tag(array(
                                                      'src'    => $this->getFile(),
                                                      'name'   => $model,
                                                      'vendor' => $cats[$row->categoryID]['vendor'],
                                                      'cName'  => $row->typeID,
                                                 )), $imgs[$row->sku] ? 'http://kolesiko.ua/media/catalog/product' . $imgs[$row->sku] : '', $row->price
               )
            );
         }

         $this->put('</items>');

         $this->put('</price>');
      }

      public function put($what) {
         file_put_contents($this->file, $what, FILE_APPEND | FILE_BINARY);
      }

      public function getFile() {
         return $this->file;
      }

      public function setFile($file) {
         $this->file = $file;
      }

      private function _detectType($file) {
         preg_match('#(\w+)\.xml#iu', $file, $matches);
         $type = $matches[1];

         if(!isset($this->options['patterns'][$type])) $type = 'default';

//         var_dump($matches, $type, $this->options['patterns'][$type]); exit;
         return $type;
      }

      private function _yandex($types = array(1, 2, 3), $file = '../kolesiko.ua/yandex.xml', $tags = array('item'    => 'offer', 'categories' => 'categories',
                                                                                                           'picture' => 'picture')) {
         $ptid = array(1 => 7301, 2 => 7304, 3 => 5);
         $this->_prepareFile($file);
         $is_yandex = strpos($this->getFile(), 'yandex') !== false;

         $this->_xmlHead();
         if($is_yandex)
            $this->put("<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");

         $this->put(sprintf('<yml_catalog date="%s">', date('Y-m-d H:i')));
         $this->put('<shop>');

         $this->put(sprintf(
            '<name>Kolesiko.ua</name>
            <company>Kolesiko.ua</company>
            <url>http://kolesiko.ua</url>
            <currencies>
               <currency id="UAH" rate="1"/>
               <currency id="USD" rate="8.12"/>
            </currencies>
            <%s>
               <category id="1">Авто</category>
               <category id="4">Сантехника</category>
               <category id="2" parentId="1">Шины‹</category>
               <category id="3" parentId="1">Диски</category>
            </%s>', $tags['categories'], $tags['categories']));

         $this->put('<' . $tags['item'] . 's>');

         $types = implode(',', $types);

         $urls = $attrs = $cats = $imgs = null;
         extract($this->gather($types));

         $type = $this->_detectType($file);

         $name = 'name';
         $attr = '';

         if($is_yandex) {
            $name = 'model';
            $attr = 'type="vendor.model"';
         }

         $products = \System\Core\Engine::$dbi->query("select distinct sku, categoryID, ID, typeID, price from sota_products where typeID in ($types) and price > 0")->result();
         for($i = 0, $l = sizeof($products); $i < $l; $i++) {
            $row = $products[$i];

            if(!$urls[$row->sku])
               continue;

            $model = $this->strip($this->map($this->glue($attrs[$row->ID], $cats[$row->categoryID]), $ptid[$row->typeID], $type));
            $this->put(sprintf(<<<HERE
<%s id="%s" %s available="true">
<url>http://kolesiko.ua/%s</url>
<price>%d</price>
<currencyId>UAH</currencyId>
<categoryId>%d</categoryId>
<%s><![CDATA[%s]]></%s>
<vendor><![CDATA[%s]]></vendor>
<%s><![CDATA[%s]]></%s>
<description></description>
<manufacturer_warranty>true</manufacturer_warranty>
</%s>
HERE
               , $tags['item'], $row->ID, $attr, htmlspecialchars($urls[$row->sku] . $this->_tag(array(
                                                                                                      'src'    => $this->getFile(),
                                                                                                      'name'   => $model,
                                                                                                      'vendor' => $cats[$row->categoryID]['vendor'],
                                                                                                      'cName'  => $row->typeID,
                                                                                                 )), ENT_DISALLOWED | ENT_XML1 | ENT_QUOTES, 'UTF-8'),
               $row->price, $row->typeID + 1, $tags['picture'], $imgs[$row->sku] ? 'http://kolesiko.ua/media/catalog/product' . $imgs[$row->sku] :
                  '', $tags['picture'], $cats[$row->categoryID]['vendor'], $name, $model, $name, $tags['item']));
         }

         $this->put('</' . $tags['item'] . 's>');

         $this->put('</shop></yml_catalog>');

      }

      private function _tag($args) {
         $template = "utm_source=%s&utm_medium=price&utm_term=%s&utm_content=%s&utm_campaign=%s";

         preg_match('#(\w+)\.xml#iu', $args['src'], $matches);

         $source = $matches[1] == 'yandex' ? 'yandex_market' : $matches[1];

         $campaigns = array(1 => 'shini', 2 => 'diski', 3 => 'santekhnika');
         $campaign  = $campaigns[$args['cName']];

         return '?' . strtr(sprintf(
            $template, $source, $args['name'], $args['vendor'], $campaign
         ), $this->translit);
      }

      private function _xmlHead() {
         $this->put('<?xml version="1.0" encoding="UTF-8" ?>' . "\n");
      }

      private function _prepareFile($file) {
         $this->setFile($file);

         if(file_exists($this->getFile()))
            unlink($this->getFile());
      }

      /**
       * @param string $types
       *
       * @return array
       */
      private function gather($types) {

         $products =
            implode(',', \System\Core\Engine::$dbi->query("select sku from sota_products where typeID in ($types) and price > 0")->getStatement()->fetchAll(\PDO::FETCH_COLUMN));

         $ids = implode(',', $this->connector->db->query(
            'select entity_id from catalog_product_entity where sku in (' . $products . ')')->getStatement()->fetchAll(\PDO::FETCH_COLUMN));
         unset($products);

         //$ids_path = implode(',', array_map(function($e) { return "'product/$e'"; }, explode(',',$ids)));

         $urls = $this->connector->db->query(
            'select CUR.request_path, CPE.sku from core_url_rewrite CUR
            left join catalog_product_entity CPE on CPE.entity_id = CUR.product_id
            where product_id in (' . $ids . ') and id_path = concat("product/", product_id) group by CUR.product_id')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $_e = array();
         for($i = 0, $l = sizeof($urls); $i < $l; $i++) $_e[$urls[$i][1]] = $urls[$i][0];
         $urls = $_e;
         unset($_e);

         $_e   = array();
         $imgs = $this->connector->db->query('
            select CPEMG.value as url, CPE.sku from catalog_product_entity_media_gallery CPEMG
            left join catalog_product_entity CPE on CPE.entity_id = CPEMG.entity_id where CPE.entity_id in (' . $ids . ')')->getStatement()->fetchAll(\PDO::FETCH_NUM);
         for($i = 0, $l = sizeof($imgs); $i < $l; $i++) $_e[$imgs[$i][1]] = $imgs[$i][0];
         $imgs = $_e;
         unset($_e);
         unset($ids);

         $ids = implode(',', \System\Core\Engine::$dbi->query("select ID from sota_products where typeID in ($types) and price > 0")->getStatement()->fetchAll(\PDO::FETCH_COLUMN));

         $attrs = \System\Core\Engine::$dbi->query(
            'select value, propertyID, productID from sota_propertiesValues where productID in (' . $ids . ')')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $_e = array();
         for($i = 0, $l = sizeof($attrs); $i < $l; $i++) {
            $c = $attrs[$i];
            if(!$_e[$c[2]]) {
               $_e[$c[2]] = array($c[1] => $c[0]);
               continue;
            }

            $_e[$c[2]][$c[1]] = $c[0];
         }
         $attrs = $_e;
         unset($_e);

         $ids =
            implode(',', \System\Core\Engine::$dbi->query("select categoryID from sota_products where typeID in ($types) and price > 0")->getStatement()->fetchAll(\PDO::FETCH_COLUMN));

         $cats = \System\Core\Engine::$dbi->query('select C1.ID, C1.name, C2.name as vendor from sota_categories C1
         left join sota_categories C2 on C1.parent = C2.ID where C1.ID in (' . $ids . ')')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $_e = array();
         for($i = 0, $l = sizeof($cats); $i < $l; $i++) {
            $c         = $cats[$i];
            $_e[$c[0]] = array('name' => $c[1], 'vendor' => $c[2]);
         }
         $cats = $_e;
         unset($_e);

         return compact('cats', 'imgs', 'urls', 'attrs');
      }

      private $translit = array(
         'А' => 'A', 'а' => 'a', 'Б' => 'B', 'б' => 'b', 'В' => 'V', 'в' => 'v', 'Г' => 'G', 'г' => 'g',
         'Д' => 'D', 'д' => 'd', 'Е' => 'E', 'е' => 'e', 'Ё' => 'Yo', 'ё' => 'yo',
         'Ж' => 'Zh', 'ж' => 'zh', 'З' => 'Z', 'з' => 'z', 'И' => 'I', 'и' => 'i', 'Й' => 'J', 'й' => 'j',
         'К' => 'K', 'к' => 'k', 'Л' => 'L', 'л' => 'l', 'М' => 'M', 'м' => 'm',
         'Н' => 'N', 'н' => 'n', 'О' => 'O', 'о' => 'o', 'П' => 'P', 'п' => 'p', 'Р' => 'R', 'р' => 'r',
         'С' => 'S', 'с' => 's', 'Т' => 'T', 'т' => 't', 'У' => 'U', 'у' => 'u',
         'Ф' => 'F', 'ф' => 'f', 'Х' => 'X', 'х' => 'x', 'Ц' => 'cz', 'ц' => 'cz', 'Ч' => 'ch', 'ч' => 'ch',
         'Ш' => 'Sh', 'ш' => 'sh', 'Щ' => 'Shh', 'щ' => 'shh', 'Ъ' => '', 'ъ' => '',
         'Ы' => 'Y', 'ы' => 'y', 'Ь' => '', 'ь' => '', 'Э' => 'E', 'э' => 'e', 'Ю' => 'Yu', 'ю' => 'yu', 'Я' => 'Ya',
         'я' => 'ya',
         'a' => 'a', 'b' => 'b', 'c' => 'c', 'd' => 'd', 'e' => 'e', 'f' => 'f', 'g' => 'g', 'h' => 'h', 'i' => 'i',
         'j' => 'j', 'k' => 'k', 'l' => 'l', 'm' => 'm', 'n' => 'n',
         'o' => 'o', 'p' => 'p', 'q' => 'q', 'r' => 'r', 's' => 's', 't' => 't', 'u' => 'u', 'v' => 'v', 'w' => 'w',
         'x' => 'x', 'y' => 'y', 'z' => 'z',
         'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F', 'G' => 'G', 'H' => 'H', 'I' => 'I',
         'J' => 'J', 'K' => 'K', 'L' => 'L', 'M' => 'M', 'N' => 'N',
         'O' => 'O', 'P' => 'P', 'Q' => 'Q', 'R' => 'R', 'S' => 'S', 'T' => 'T', 'U' => 'U', 'V' => 'V', 'W' => 'W',
         'X' => 'X', 'Y' => 'Y', 'Z' => 'Z', ' ' => '_', '/' => '-', '\\' => '-', '(' => '-', ')' => '-'
      );
   }
}