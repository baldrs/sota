<?php

namespace System\Application\Controllers\Export {
   class BaseCsv implements \System\Core\Interfaces\Export\GenericCsv {
      protected $file;
      protected $enclosure;
      protected $delimiter;

      public function write($line) {
         fputcsv($this->file, $line, $this->delimiter, $this->enclosure);
      }

      public function open($file, $open_read = false) {
         if(!$open_read)
            $this->file = fopen($file, 'w');
         else
            $this->file = fopen($file, 'r');
      }

      public function __construct() {
         if(!$this->delimiter)
            $this->setDelimiter(';');
         if(!$this->enclosure)
            $this->setEnclosure('"');
      }

      public function getEnclosure() {
         return $this->enclosure;
      }

      public function getDelimiter() {
         return $this->delimiter;
      }

      public function setDelimiter($delimiter) {
         $this->delimiter = $delimiter;
      }

      public function setEnclosure($enclosure) {
         $this->enclosure = $enclosure;
      }

      public function __destruct() {
         $this->close();
      }

      public function read() {
         return fgetcsv($this->file, 0, $this->getDelimiter(), $this->getEnclosure());
      }

      public function close() {
         if($this->file)
            fclose($this->file);
      }
   }
}
