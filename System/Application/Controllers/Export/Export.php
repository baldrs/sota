<?php
/**
 * Created by JetBrains PhpStorm.
 * User: baldrs
 * Date: 11/8/12
 * Time: 12:54 PM
 * To change this template use File | Settings | File Templates.
 */
namespace System\Application\Controllers\Export {
   use System\Core\Engine;

   class Export extends \System\Core\ShellController {
      protected static $db;
      protected $file;

      public function __construct() {
         setlocale(LC_ALL, 'ru_RU.UTF-8');
      }

      protected static function map($properties, $typeProperties, $config) {
         $result = array();
         for($key = 0, $length = count($typeProperties); $key < $length; $key++) {
            $tp = $typeProperties[$key];
            foreach($properties as $ID => $p) {
               if($tp->ID == $ID) {
                  if(!is_array($p) && $p) {
                     $result[$key]    = $p;
                     $properties[$ID] = null;
                     break;
                  } elseif($p && is_array($p)) {
                     $result[$key]    = implode(' , ', $p);
                     $properties[$ID] = null;
                     unset($tmp);
                     break;
                  }
               }
            }
            if(!$result[$key]) $result[$key] = '';
         }

         $properties = array_filter($properties);

         if(!$config)
            return $result;

         if($config['additional'] && !empty($properties)) {
            $tmp = array();

            foreach($config['aProperties'] as $propertyID) {
               foreach($properties as $ID => $value) {
                  if($ID == $propertyID) {
                     if($config['aValues'][$ID] !== 'raw')
                        $tmp[] = $config['aValues'][$ID];
                     else {
                        if(is_array($value))
                           $value = implode(' , ', $value);

                        $tmp[] = $value;
                     }
                  }
               }
            }
            $result[] = implode(' , ', $tmp);
         } elseif(empty($properties)) {
            $result[] = '';
         }

         return $result;
      }

      protected static function bind($data, $config) {
         $result = array();
         for($index = 0, $length = count($config['fields']); $index < $length; $index++) {
            $rules = explode('|', $config['fields'][$index]);
            $field = array_shift($rules);

            if(sizeof($rules)) { //modifiers
               $_result = $data->{$field} ? : '';
               for($i = 0, $l = count($rules); $i < $l; $i++) { //$rules as $rule) {
                  $rule   = $rules[$i];
                  $raw    = explode(":", $rule);
                  $method = array_shift($raw);
                  $args   = array_values($raw);

                  $_result = call_user_func_array(
                     '\\System\\Application\\Controllers\\Export\\Helpers::' . $method, array_merge(array($_result), $args));
               }
               $result[] = $_result;
            } else {
               if($field == 'default')
                  $result[] = $config['default'][$index];
               else
                  $result[] = $data->{$field};
            }
         }

         return $result;
      }

      public function exportToOnec() {
         $csv = new BaseCsv();
         $csv->open(__DIR__ . '/../../../Temp/Export/1C/products.csv');

         $p = Engine::$dbi->query('select * from sota_products order by typeID asc, sku asc  ')->getStatement();

         while($row = $p->fetchObject())
            $csv->write(array(
                             $row->sku,
                             '',
                             'K' . $row->categoryID,
                             $row->name ? : '',
                             $row->price ? : 0,
                             $row->vendor ? : '',
                        ));
         $csv->close();
         $csv->open(__DIR__ . '/../../../Temp/Export/1C/categories.csv');

         $p = Engine::$dbi->query('select * from sota_categories')->getStatement();

         while($r = $p->fetchObject())
            $csv->write(array(
                             'K' . $r->ID,
                             'K' . $r->parent,
                             $r->name,
                        ));
         unset($csv);
         if(file_exists(__DIR__ . '/../../../Temp/Export/1C/1C.zip'))
            unlink(__DIR__ . '/../../../Temp/Export/1C/1C.zip');

         exec('cd ' . __DIR__ . '/../../../Temp/Export/1C/ &&  zip 1C.zip -9 *');

         header('Content-Type: application/zip');

         header('Content-Disposition: attachment; filename="1C' . date('_d_m_Y') . '.zip"');

         header('Content-Transfer-Encoding: binary');

         readfile(__DIR__ . '/../../../Temp/Export/1C/1C.zip');
      }

      public function exportType($type = 0, $limit = 0) {
         $gconfig = \System\Lib\Yaml\Yaml::parse(file_get_contents(__DIR__ . '/Export.yml'));

         if(in_array($type, $gconfig['blacklisted']))
            return null;

         $std = $gconfig['std'];

         $config = $gconfig[$type];
         $tp     = Engine::$dbi
            ->prepare('select magento, ID from sota_properties where typeID = ?')->execute(array($type))->result();
         $ids    = array();
         foreach($tp as $to)
            $ids[] = $to->ID;
         for($i = 0, $l = count($tp); $i < $l; $i++)
            if(in_array($tp[$i]->ID, $config['aProperties'])) $tp[$i] = null;

         $typeProperties = array_values(array_filter($tp));

         $result = array();
         foreach($typeProperties as $property) {
            if(!in_array($property->ID, $config['aProperties']))
               $result[] = $property->magento;
         }

         $head = array_merge($std['fields'], $result);

         if($config['additional'])
            $head[] = $config['additional'];

         $result = Engine::$dbi
            ->prepare('select * from sota_products where typeID = ? order by path asc')
            ->execute(array($type))->getStatement();

         $properties = array();
         if(sizeof($ids)) {
            $sql = 'select propertyID, productID, value from sota_propertiesValues where propertyID in (' . implode(', ', $ids) . ')';

            $properties2 = Engine::$dbi->query($sql)->getStatement();

            while($c = $properties2->fetch(\PDO::FETCH_OBJ)) {
               if(!isset($properties[$c->productID]))
                  $properties[$c->productID] = array();

               if(!isset($properties[$c->productID][$c->propertyID]))
                  $properties[$c->productID][$c->propertyID] = array();

               $properties[$c->productID][$c->propertyID][] = $c->value;
            }

            unset($properties2);
            array_walk($properties, function (&$e, $k) {
               array_walk($e, function (&$e, $k) {
                  if(is_array($e) && count($e) == 1)
                     $e = $e[0];
               });
            });

            echo ' ', round(memory_get_usage() / pow(1024, 2), 2), 'MB<br/>';
         }

         if(file_exists('../System/Temp/Export/products' . $type . '.csv'))
            unlink('../System/Temp/Export/products' . $type . '.csv');

         $csv = fopen('../System/Temp/Export/products' . $type . '.csv', 'w');

         fputcsv($csv, $head, ';', '"');
         $c = count($head);

         $counter = 0;

         while($row = $result->fetch(\PDO::FETCH_OBJ)) {
            $_row = array_merge(self::bind($row, $config), self::map($properties[$row->ID] ? : array(), $typeProperties, $config));
            if(count($_row) < $c)
               $_row = array_pad($_row, $c, '');
            elseif(count($_row) > $c)
               $_row = array_slice($_row, 0, $c);
            if(isset($properties[$row->ID]))
               unset($properties[$row->ID]);

            if($limit) {
               $counter++;
               if($counter == $limit) break;
            }

            fputcsv($csv, $_row, ';', '"');
         }
         unset($result);
         fclose($csv);
         echo 'Peak memory consumption ', round(memory_get_peak_usage() / pow(1024, 2), 2), 'MB<br/>';
      }

      public function openFile($file) {
         if($this->file)
            fclose($this->file);
         $this->file = fopen($file, 'w');

         return $this;
      }

      public function closeFile() {
         fclose($this->file);

         return $this;
      }

      public function __destruct() {
         if($this->file)
            fclose($this->file);
      }
   }
}

