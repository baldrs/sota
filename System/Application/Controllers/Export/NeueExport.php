<?php

namespace System\Application\Controllers\Export {
   use System\Core\Engine;

   class NeueExport extends \System\Core\ShellController {
      private $writer;

      public function __construct() {
         error_reporting(E_ALL & ~E_NOTICE);
         $this->writer = new CsvIO(';', '"');
      }

      public function basicExport() {
         $this->writer->open(__DIR__ . '/../../../Temp/Export/products_all.csv', 'w');

         /*         $types = '('.implode(',',array_map(function($arg) { return Engine::$dbi->quote($arg);},
                     Engine::$dbi->query('select magento_name from sota_neue_products_attributes_groups')
                     ->getStatement()->fetchAll(\PDO::FETCH_COLUMN))).')';*/

         $defaults =
            array('status' => 'Enabled', 'visibility' => 'Catalog, search', 'tax_class_id' => 'None', 'use_config_min_sale_qty' => 1, 'is_in_stock' => 0, 'min_sale_qty' => 1);

         $id_list = '(' . implode(',', Engine::$dbi->query('select ID from sota_neue_products where to_magento = 1')->getStatement()->fetchAll(\PDO::FETCH_COLUMN)) . ')';

         $head = Engine::$dbi->query('select ID, magento_name from sota_neue_products where to_magento = 1')->getStatement()->fetchAll(\PDO::FETCH_NUM);

         $no_empty_types = '(' . implode(',', Engine::$dbi
            ->query('select baseID from sota_neue_products_values where propertyID = 70 and value != ""')
            ->getStatement()->fetchAll(\PDO::FETCH_COLUMN)) . ')';

         $products = Engine::$dbi->query(
            'select baseID, propertyID, value from sota_neue_products_values where propertyID in ' . $id_list . ' and baseID in ' . $no_empty_types)->getStatement();

         $data = array();
         while($current = $products->fetch(\PDO::FETCH_NUM)) {
            if(!isset($data[$current[0]])) {
               $data[$current[0]] = array($current[1] => $current[2]);
               continue;
            }

            $data[$current[0]][$current[1]] = $current[2];
         }

         unset($products);

         $tmp = array();
         for($j = 0, $m = sizeof($head); $j < $m; $j++) $tmp[] = $head[$j][1];
         $tmp = array_merge($tmp, array_keys($defaults));
         $this->writer->write($tmp);
         unset($tmp);

         $keys = array_keys($data);
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $result  = array();
            $current = $data[$keys[$i]];

            for($j = 0, $m = sizeof($head); $j < $m; $j++) {
               $result[] = $head[$j][0] == 13 ? str_replace(' / ', '/', $current[$head[$j][0]]) : $current[$head[$j][0]];

               switch($head[$j][0]) {
                  case 70:
                     if(in_array($current[$head[$j][0]], array('k_atyre_auto', 'k_awheel_auto', 'k_atyre_gruz'))) {
                        if($current[31] >= 4 || !($current[31] % 4)) {
                           $defaults['min_sale_qty']            = 4;
                           $defaults['use_config_min_sale_qty'] = 0;
                        } else {
                           $defaults['min_sale_qty']            = 1;
                           $defaults['use_config_min_sale_qty'] = 1;
                        }
                     } else {
                        $defaults['min_sale_qty'] = 1;
                     }
                     break;
                  case 31:
                     $defaults['is_in_stock'] = $current[31] > 0 ? 1 : 0;
                     break;
               }
            }
            $result = array_merge($result, $defaults);
            $this->writer->write($result);
         }
      }

      public function propertiesExport($type_name) {
         $this->writer->open(__DIR__ . '/../../../Temp/Export/' . $type_name . '.csv', 'w');

         $additional = array(
            'k_atyre_auto' => array('k_atyre_car_additional', 6, 8, 9, 18),
            'k_atyre_gruz' => array('k_atyre_gruz_additional', 24, 26, 27, 29)
         );

         $additional_defaults = array(
            'k_atyre_auto' => array(6 => 'XL', 8 => 'Шипованая', 9 => 'RunFlat'),
            'k_atyre_gruz' => array(24 => 'XL', 26 => 'Шипованая', 27 => 'RunFlat'),
         );

         $typeID = Engine::$dbi
            ->prepare('select ID from sota_neue_products_attributes_groups where magento_name = ?')
            ->execute(array($type_name))->row()->ID;

         $attributes = Engine::$dbi
            ->prepare('select ID, magento_name from sota_neue_products_attributes where typeID = :id')
            ->bindValue(':id', $typeID, \PDO::PARAM_INT)
            ->execute()->result(\PDO::FETCH_NUM);

         $product_ids = '(' . implode(',', Engine::$dbi
               ->prepare('select distinct baseID from sota_neue_products_values where propertyID = 70 and value = ?')
               ->execute(array($type_name))->column()
         ) . ')';

         Engine::$dbi->query('select baseID, propertyID, value from sota_neue_products_attributes_values where baseID in ' . $product_ids);

         $properties = array();

         while($current = Engine::$dbi->row()) {
            if(!isset($properties[$current->baseID]))
               $properties[$current->baseID] = array();

            if(!isset($properties[$current->baseID][$current->propertyID]))
               $properties[$current->baseID][$current->propertyID] = array();

            $properties[$current->baseID][$current->propertyID][] = $current->value;
         }

         array_walk($properties, function (&$e, $k) {
            array_walk($e, function (&$e, $k) {
               if(is_array($e) && count($e) == 1)
                  $e = $e[0];
            });
         });

         $head  = array('sku');
         $match = array(0);

         for($i = 0, $l = sizeof($attributes); $i < $l; $i++) {
            if(isset($additional[$type_name])) {
               if(in_array($attributes[$i][0], $additional[$type_name])) continue;
            }
            $head[]  = $attributes[$i][1];
            $match[] = $attributes[$i][0];
         }

         if(isset($additional[$type_name]))
            $head[] = array_shift($additional[$type_name]);

         $this->writer->write($head);

         $keys = array_keys($properties);
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            $current = $properties[$keys[$i]];
            $result  = array();
            for($j = 0, $m = sizeof($match); $j < $m; $j++) {
               if(!$j) { // sku
                  $result[] = $keys[$i];
                  continue;
               }

               if(is_array($current[$match[$j]])) {
                  $result[] = implode(' , ', $current[$match[$j]]);
               } else {
                  $result[] = $current[$match[$j]];
               }
            }

            $tmp = array();
            if($additional[$type_name]) {
               for($j = 0, $m = sizeof($additional[$type_name]); $j < $m; $j++) {
                  $property = $current[$additional[$type_name][$j]];
                  if($property) {
                     if($property == 'true') {
                        $tmp[] = $additional_defaults[$type_name][$additional[$type_name][$j]];
                     } elseif(is_array($property)) {
                        $tmp[] = implode(' , ', $property);
                     } else {
                        $tmp[] = $property;
                     }
                  }
               }
            }

            if(sizeof($tmp)) {
               $result[] = implode(' , ', $tmp);
            }

            if(sizeof($head) > sizeof($result)) {
               $result = array_merge($result, array_fill(sizeof($result), sizeof($head) - sizeof($result), ''));
            }

            $this->writer->write($result);
         }

      }

      public function populateOldTables() {
         $old_tables = array('products', 'suppliers',); //'propertiesValues');

         $equality_products = array(
            'products'  => array(
               'ID', 'categoryID', 'supplierID', 'vendorID', 'sku', 'vendor', 'supplier',
               'supplierCity', 'name', 'path', 'numericPath', 'availability', 'inprice', 'price',
               'amount', 'update', 'typeID',
            ),
            'suppliers' => array(
               'productID', 'supplierID', 'categoryID', 'vendorID', 'name', 'vendor', 'supplier', 'supplierCity', 'availability',
               'additionalInfo', 'currency', 'rawPrice', 'inprice', 'price', 'amount', 'update', 'deliverers', 'cashless'
            ),
         );

         $properties_id = array(
            'products'  => array(0, 55, 62, 63, 0, 69, 41, 66, 4, 13, 57, 32, 7, 6, 31, 44, 70),
            'suppliers' => array(0, 30, 32, 34, 5, 3, 27, 36, 16, 37, 14, 10, 9, 8, 15, 29, 38, 35),
         );

         $newtypes = array(
            'k_atyre_auto' => 1, 'k_awheel_auto' => 2, 'k_bsan_wash' => 3, 'k_bsan_mixer' => 3, 'Default' => 0, 'k_atyre_gruz' => 4,
         );
         foreach($old_tables as $table) {
            $p = $properties_id[$table];

            $field = $table == 'suppliers' ? 'supplierID, ' : ' ';

            Engine::$dbi->query("select baseID, propertyID,{$field}value from sota_neue_{$table}_values where propertyID in (" . implode(',', $p) . ')');

            $products = array();

            if($table == 'products') {
               while($current = Engine::$dbi->row(\PDO::FETCH_NUM)) {
                  if(!isset($products[$current[0]])) {
                     $products[$current[0]] = array($current[1] => $current[2]);
                     continue;
                  }

                  $products[$current[0]][$current[1]] = $current[2];
               }
            } else {
               while($current = Engine::$dbi->row(\PDO::FETCH_NUM)) {
                  if(!isset($products[$current[2]])) {
                     $products[$current[2]] = array($current[0] => array($current[1] => $current[3]));
                     continue;
                  }

                  $products[$current[2]][$current[0]][$current[1]] = $current[3];
               }
            }

            $into = "insert into sota_{$table}(" . implode(',', array_map(function ($e) { return "`$e`"; }, $equality_products[$table])) . ') values ';

            if($table == 'products') {
               $keys = array_keys($products);

               for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
                  $current = $products[$keys[$i]];
                  $result  = array();

                  for($j = 0, $m = sizeof($p); $j < $m; $j++) {
                     if($p[$j] == 0) {
                        $result[] = $keys[$i];

                        continue;
                     } elseif($p[$j] == 44 || $p[$j] == 29) {
                        $result[] = date('Y-m-d H:i', strtotime($current[$p[$j]]));
                        continue;
                     } elseif($p[$j] == 70 && $table == 'products') {
                        $result[] = $newtypes[$current[70]];
                        continue;
                     }

                     $result[] = $current[$p[$j]];

                  }

                  $into .= '(' . implode(',', array_map(function ($e) { return Engine::$dbi->quote($e); }, $result)) . '), ';
               }

            } elseif($table == 'suppliers') {
               $keys = array_keys($products);

               for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
                  $current = $products[$keys[$i]];
//                  $supplierID = $keys[$i];
                  $product_keys = array_keys($current);

                  for($j = 0, $m = sizeof($product_keys); $j < $m; $j++) {
                     $current_product = $current[$product_keys[$j]];

                     $result = array();

                     for($k = 0, $n = sizeof($p); $k < $n; $k++) {
                        if($p[$k] == 0) {
                           $result[] = $product_keys[$j];

                           continue;
                        } elseif($p[$k] == 44 || $p[$k] == 29) {
                           $result[] = date('Y-m-d H:i', strtotime($current_product[$p[$k]]));
                           continue;
                        } elseif($p[$k] == 70 && $table == 'products') {
                           $result[] = $newtypes[$current_product[70]];
                           continue;
                        }

                        $result[] = $current_product[$p[$k]];

                     }

                     $into .= '(' . implode(',', array_map(function ($e) { return Engine::$dbi->quote($e); }, $result)) . '), ';
                  }

               }
            }

            Engine::$dbi->query("truncate table sota_{$table}");
            Engine::$dbi->query(substr($into, 0, strlen($into) - 2));

         }

         // обновление свойств
         Engine::$dbi->query('truncate table sota_propertiesValues');

         Engine::$dbi->query(
            'insert into sota_propertiesValues(propertyID, productID, value)
          SELECT if(propertyID > 17, propertyID + 1, propertyID) as propertyID, baseID, value
          FROM `sota_neue_products_attributes_values`');

         $this->populateCategories();
      }

      private function populateCategories() {
         Engine::$dbi->query('select baseID, propertyID, value from sota_neue_categories_values where propertyID in ( 1, 2, 3 ) ');

         echo "<pre>";
         $categories = array();

         while($current = Engine::$dbi->row(\PDO::FETCH_NUM)) {
            if(!isset($categories[$current[0]])) {
               $categories[$current[0]] = array($current[1] => $current[2]);

               continue;
            }

            $categories[$current[0]][$current[1]] = $current[2];
         }

         $keys = array_keys($categories);

         $result = 'insert into sota_categories(`ID`, `parent`, `name`) values ';
         for($i = 0, $l = sizeof($keys); $i < $l; $i++) {
            if(sizeof($categories[$keys[$i]]) > 3) continue;

            if(sizeof($categories[$keys[$i]]) < 3) {
               $categories[$keys[$i]] = array_merge($categories[$keys[$i]], array_fill(sizeof($categories[$keys[$i]]), 3 - sizeof($categories[$keys[$i]]), ''));
            }

            $result .= '(' . implode(',', array_map(array(Engine::$dbi, 'quote'), $categories[$keys[$i]])) . '), ';
         }

         Engine::$dbi->query('truncate table sota_categories');

         Engine::$dbi->query(substr($result, 0, strlen($result) - 2));
      }

   }

}
