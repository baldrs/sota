<?php

namespace System\Application\Controllers {
   use System\Core\Engine;

   class Shell extends \System\Core\Controller {

      public function __construct() { }

      public function index() { }

      public function migrate() {
         $time = microtime(true);

         if(!$_SERVER['HTTP_HOST'])
            $_SERVER['HTTP_HOST'] = end(explode('/', $this->config['host']));

         $domain_folder = 'kolesiko.ua';
         /*
                  Это уже проделала автоматизация
                  Engine::out("Importing files");
                  Engine::out("Url: " . 'http://' . $_SERVER['HTTP_HOST'] . '/import/');
                  $import = $this->_get('http://' . $_SERVER['HTTP_HOST'] . '/import/');

                  Engine::out($import);*/
         Engine::out("Exporting files");
         //   Engine::out("Url: " . 'http://' . $_SERVER['HTTP_HOST'] . '/export/');
//         $export = $this->_get('http://' . $_SERVER['HTTP_HOST'] . '/export/');
         $export = new \System\Application\Controllers\Export\NeueExport();
         $export->basicExport();
         foreach(array('k_atyre_auto', 'k_awheel_auto', 'k_atyre_gruz') as $type)
            $export->propertiesExport($type);

         $export->populateOldTables();

         $profiles = array('products0.csv', 'k_atyre_auto', 'k_awheel_auto', 'k_atyre_gruz');
         foreach($profiles as $profile)
            system("php ../$domain_folder/magmi/cli/magmi.cli.php -profile=" . $profile . ' -mode=create');

         /*         Engine::out($export);
                  $path  = '../System/Temp/Export';
                  $dir   = scandir($path);
                  $magmi = array();

                  foreach($dir as $file) {
                     if(is_dir($path . '/' . $file)) continue;

                     if(strpos($file, 'product') === 0) {
                        Engine::out("Starting magento mass importer for $file");
                        Engine::out('php ../kolesiko.ua/magmi/cli/magmi.cli.php -profile=' . $file . ' -mode=create ');
                        $magmi[] = system('php ../kolesiko.ua/magmi/cli/magmi.cli.php -profile=' . $file . ' -mode=create');
                     }
                  }*/

         Engine::out("Starting magento indexer");
         system("php ../$domain_folder/shell/indexer.php reindexall");
         Engine::out('Генерю xml');
         $xml = array('yandex', 'nadavi', 'technoportal', 'vcene', 'hotline', 'freemarket');

         foreach($xml as $file)
            Engine::fork('php index.php xml ' . $file);

         $adWords = new \System\Application\Controllers\Export\Adwords();
         $adWords->generate();
         $adWords->generate('winter');

         Engine::out("Execution time: " . (microtime(true) - $time));
      }

      public function loadFile($args) {
         list($table,) = explode('_', pathinfo($args[2], PATHINFO_BASENAME));
         echo $table . "\n";
         $loader = new \System\Application\Controllers\Import\NeueImport();
         $loader->putValuesDataIntoTable($args[2], $table);
      }

      public function xml($args) {

         $out = new \System\Application\Controllers\Export\XMLOut();
         $out->{$args[2]}();
      }

      private function _get($url) {
         if(!extension_loaded('curl'))
            exit("curl required");

         $hs = curl_init($url);
         curl_setopt($hs, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($hs);

         curl_close($hs);

         return $result;
      }
   }
}
