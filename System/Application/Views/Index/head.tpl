<base href="http://{$smarty.server.HTTP_HOST}"/>
<meta charset="utf-8">
<title>SOTA</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Baldrs">
<link rel="shortcut icon" href="/images/favicon.png">

<!-- Le styles -->
<link href="/scripts/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/scripts/plugins/fineuploader/fineuploader.css" rel="stylesheet">
<link href="/styles/user.css" rel="stylesheet">

<link href="/scripts/plugins/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<link href="/scripts/plugins/datepicker/css/datepicker.css" rel="stylesheet">

<script type="text/javascript" src="/scripts/plugins/jquery/jquery-1.8.3.js"></script>
<script type="text/javascript" src="/scripts/plugins/jquery/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="/scripts/plugins/tableFilter/tableFilter.js"></script>
<script type="text/javascript" src="/scripts/plugins/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/scripts/plugins/datepicker/js/locales/bootstrap-datepicker.ru.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script type="text/javascript" src="/scripts/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/scripts/plugins/bootstrap/js/bootstrap-typeahead.js"></script>
<script type="text/javascript" src="/scripts/storage.js"></script>
<script type="text/javascript" src="/scripts/ticket.js"></script>
<script type="text/javascript" src="/scripts/ready.js"></script>
<script type="text/javascript" src="/scripts/plugins/fineuploader/jquery.fineuploader-3.0.js"></script>
