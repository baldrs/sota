<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
   <h3 id="myModalLabel">{$title}, дата: {$date}, ответственный: {$name}</h3>
</div>
<div class="modal-body" id="save">
{if $view}
      {include file="Index/Tickets/view.tpl" ticket=$ticket user=$user edit=true}
      {elseif $edit}
      {include file="Index/Tickets/edit.tpl" ticket=$ticket user=$user}

      {/if}
</div>
<div class="modal-footer">
{if $edit && !$view}
   <input class="input-small article" placeholder="Артикул" type="text">
   <button class="btn add-product" data-ticket="{$ticket->ID}">Добавить</button>
{/if}
   <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
{if !$view}
   <button class="btn btn-primary" {if $edit}data-additional='{$ticket->ID}'{/if}>Сохранить</button>

{/if}
</div>
