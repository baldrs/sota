{extends 'Index/layout.tpl'}
{block name="content"}
   {if $done == 'update'}
   <div class="alert alert-success">Обновление успешно завершено</div>
      {elseif $done == 'export'}
   <div class="alert alert-success">Экспорт успешно завершен</div>
   {/if}
<ul class="nav nav-tabs">
   <li class="active"><a data-toggle="tab" href="#users">Пользователи</a></li>
   <li class="active"><a data-toggle="tab" href="#csv">Обновление</a></li>
</ul>
<div class="tab-content">
   <div class="tab-pane active" id="csv">
   {*<form class="form-inline" action="">
      <button class="btn btn-primary" id="update">Импортировать</button>*}{*
         <button class="btn btn-primary" id="exportall">Экспортировать</button>
         <div id="upload">
            <noscript>
               <p>Включите JavaScript</p>
            </noscript>
         </div>
      </form>
      <script type="text/javascript">
            {literal}
            (function ($) {
               $('#upload').fineUploader({
                  request:{
                     endpoint:'/admin/uploadCsv/'
                  },
                  template:'<div class="qq-uploader">' +
                           '<div class="qq-upload-drop-area"><span>{dragZoneText}</span></div>' +
                           '<div class="btn btn-primary qqq-upload"><div>{uploadButtonText}</div></div>' +
                           '<ul class="qq-upload-list"></ul>' +
                           '</div>',
                  fileTemplate:'<li>' +
                               '<div class="qq-progress-bar"></div>' +
                               '<span class="qq-upload-spinner"></span>' +
                               '<span class="qq-upload-finished"></span>' +
                               '<span class="qq-upload-file"></span>' +
                               '<span class="qq-upload-size"></span>' +
                               '<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>' +
                               '<a class="qq-upload-retry" href="#">{retryButtonText}</a>' +
                               '<span class="qq-upload-status-text">{statusText}</span>' +
                               '</li>',

                  classes:   {
                     button: 'qqq-upload',
                     fail:   'alert alert-error',
                     success:'alert alert-success'
                  },
                  text:      {
                     uploadButton:      'Загрузить CSV',
                     cancelButton:      'Отмена',
                     retryButton:       'Попробовать еще раз',
                     failUpload:        'Загузка не удалась',
                     dragZone:          'Перетащите сюда чтобы загрузить',
                     formatProgress:    "{percent}% из {total_size}",
                     waitingForResponse:"Обрабатывается..."
                  },
                  onComplete:function () {
                     $('#update').show();
                  }
               });
            })($);
            {/literal}
      </script>

   </div>*}
      <div class="tab-pane active" id="users">
         <form class="form-inline" action="admin/addUser/" method="post">
            <fieldset>
               <input class="input-medium" name="login" type="text" placeholder="Логин">
               <input class="input-medium" name="passwd" type="text" placeholder="Пароль">
               <input class="input-medium" name="name" type="text" placeholder="Имя пользователя">
               <input class="input-medium" name="dept" type="text" placeholder="Отдел">
               <select name="groupID" class="input-medium">
                  <option value="0">-</option>
                  {foreach $groups as $group}
                     <option value="{$group->ID}">{$group->name}</option>
                  {/foreach}
               </select>
               <button type="submit" class="btn btn-primary">Создать пользователя</button>
            </fieldset>
         </form>
         <table class="table table-striped users">
            <thead>
            <tr>
               <th>№</th>
               <th>Логин</th>
               <th>Имя</th>
               <th>Группа</th>
               <th>Включен</th>
               <th></th>
            </tr>
            </thead>
            <tbody>
               {foreach $users as $_user}
               <tr>
                  <td>{$_user->ID}</td>
                  <td>{$_user->login}</td>
                  <td>{$_user->name}</td>
                  <td>{$_user->group}</td>
                  <td>{if $_user->enabled}+{else}-{/if}</td>
                  <td></td>
               </tr>
               {/foreach}
            </tbody>
         </table>

      </div>
      <div class="tab-pane" id="properties"></div>
   </div>
</div>
{/block}