<div class="row ticket">
<div class="span12">
<div class="row">
   <div class="span3">
      <input disabled name="name" placeholder="Имя клиента" type="text" class="input-xlarge"
             value="{$ticket->credentials->name}">
      <input disabled name="phone" placeholder="Телефон" type="text" class="input-xlarge"
             value="{$ticket->credentials->phone}">
   {if $ticket->comments}
      <table class="table table-striped">
         <thead>
         <tr>
            <th>Кто</th>
            <th>Когда</th>
            <th>Сообщение</th>
         </tr>
         </thead>
         <tbody>
            {foreach $ticket->comments as $comment}
            <tr>
               <td>{$comment->name}</td>
               <td>{$comment->created}</td>
               <td>{$comment->comment}</td>
            </tr>
            {/foreach}
         </tbody>
      </table>
   {/if}
      <textarea disabled class="input-xlarge" name="comment" style="height: 60px; resize: none;"
                placeholder="Комментарий...">{if $ticket->comment}{$ticket->comment}{/if}</textarea>
   </div>
   <div class="span1">
      <label class="checkbox">
         <input disabled class="delivery" name="delivery[1]"
                value="1" type="checkbox" {if $ticket->delivery.1}checked{/if}>Самовывоз</label>
      <label class="checkbox spacer">
         <input disabled class="delivery" name="delivery[2]"
                value="2" type="checkbox" {if $ticket->delivery.2}checked{/if}>Киев</label>
      <label class="checkbox spacer">
         <input disabled class="delivery" name="delivery[3]"
                value="3" type="checkbox" {if $ticket->delivery.3}checked{/if}>Украина</label>
   </div>
   <div class="span6">
      <div class="control-row">
         <input disabled type="text" name="date[1]" class="span1 date datepicker"
                value="{$ticket->delivery.2->deliverTime|date_format:"%Y-%m-%d"}" placeholder="2012-12-21"></div>
      <div class="control-row">
         <input disabled type="text" name="date[2]" class="span1 date datepicker"
                value="{$ticket->delivery.2->deliverTime|date_format:"%Y-%m-%d"}" placeholder="2012-12.21">
         <input disabled type="text" name="address" class="span5"
                value="{$ticket->delivery.2->address}" placeholder="пр-т. Отрадный, 52"></div>
      <div class="controls control-row">
         <select disabled class="span1" name="delivererID">
            <option value="0">-</option>
         {foreach $deliverers as $deliverer}
            <option value="{$deliverer->ID}"
                    {if $deliverer->ID == $ticket->delivery.3->delivererID}selected{/if}>{$deliverer->name}</option>
         {/foreach}
         </select>
         <input disabled type="text" class="span2" name="city" placeholder="Львов" value="{$ticket->delivery.3->city}">
         <input disabled style="width: 282px;" type="text" name="warehouse" class="span3"
                value="{$ticket->delivery.3->warehouse}" placeholder="Склад №8">
      </div>
      <div class="control-row">
         <input disabled type="text" class="input-xxlarge" name="receiver"
                placeholder="Дребеденко Степан Анатолиевич, 099 999 99 99"
                value="{$ticket->delivery.3->receiver}">
      </div>
   </div>
   <div class="span2">
   {if $edit}
   {*            <label class="radio"><input disabled {if $ticket->status == 1}checked{/if} name="status" value="1" type="radio">В
                  обработке</label>
               <label class="radio"><input disabled {if $ticket->status == 2}checked{/if} name="status" value="2" type="radio">Обработан</label>
               <label class="radio"><input disabled {if $ticket->status == 3}checked{/if} name="status" value="3" type="radio">Заявка
                  на заказ</label>
               <label class="radio"><input disabled {if $ticket->status == 4}checked{/if} name="status" value="4" type="radio">Отказ</label>
               {if in_array($user->group, array('administrators', 'logistics'))}
                  <label class="radio"><input disabled {if $ticket->status == 5}checked{/if} name="status" value="5" type="radio">Заказ</label>
               *}{*<label class="radio"><input disabled {if $ticket->status == 6}checked{/if} name="status" value="6" type="radio">Архив</label>*}
      {foreach $ticket->statuses as $status}
         <label class="radio">
            <input disabled {if $ticket->status == $status->ID}checked{/if}
                   name="status" value="{$status->ID}" type="radio">
            {$status->name}</label>
      {/foreach}
   {*{/if}*}
   {/if}
   </div>
</div>
<div class="row">
   <div class="span12" style="width: 1220px;">
      <table class="table table-hover">
         <thead>
         <tr>
            <th>Артикул</th>
            <th>Наименование</th>
            <th></th>
            <th>К-во</th>
            <th>Цена</th>
         {if $user->group != 'operators'}
            <th>Розн.</th>
            <th>DLR</th>
         {/if}
            <th>Доставка</th>
            <th>Б/Н</th>
            <th>Город</th>
            <th></th>
            <th>Оператор</th>
            <th>Логист</th>
            <th></th>
         </tr>
         </thead>
         <tbody>
         {foreach $ticket->products as $product}
         <tr class='info product' data-product="{$product->productID}">
            <td>{$product->sku}</td>
            <td>{$product->name}</td>
            <td></td>
            <td><input disabled class="input-tiny product-amount" data-product="{$product->productID}" type='text'
                       value="4"></td>
            <td><input disabled class="input-tiny product-price" data-product="{$product->productID}"
                       value="{$product->price}" type="text"></td>
            {if $user->group != 'operators'}
               <td>-</td>
               <td>-</td>
            {/if}
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{$product->soComment}</td>
            <td>
               {if !$edit}
                  <button disabled class='btn btn-danger delete' data-product='{$product->ID}' title='Удалить'><i
                     class='icon-remove icon-white'></i></button>
               {/if}
            </td>
         </tr>
            {foreach $product->suppliers as $supplier}
            <tr class='supplier {*if $supplier->purchase} success{/if*}'>
               <td>
               {*<input disabled class="purchase" name="supplier"
                         data-supplier="{$supplier->supplierID}" data-ticket='{$ticket->ID}'
                         data-product="{$product->productID}" {if $supplier->purchase}checked{/if} value="1"
                         type="checkbox">*}
                     {if $user->group != 'operators'}
                  {$supplier->rawPrice} {$supplier->currency}
               {/if}
               </td>
               <td>{$supplier->name}</td>
               <td>{$supplier->additionalInfo}</td>
               <td class="amount">{$supplier->amount}</td>
               <td class="price" style="background-color: {if $product->price - $supplier->inprice > 25}#DFF0D8{else}#f2dede{/if};">
                  {if $user->group != "operators"}
                           {$supplier->price}
                        {else}
                           {if $product->price - $supplier->inprice >= 25}
                     {$product->price}
                     {else}
                     {$supplier->price}
                  {/if}
                        {/if}
               </td>
               {if $user->group != 'operators'}
                  <td>{$supplier->inprice}</td>
                  <td>{$supplier->supplier}</td>
               {/if}
               <td>{$supplier->deliverers}</td>
               <td>{$supplier->cashless}</td>
               <td>{$supplier->supplierCity}</td>
               {if date('Ymd') == date('Ymd', strtotime($supplier->update))}
                  <td class="date">
                     {date('H:i', strtotime($supplier->update))}
                  </td>
                  {else}
                  <td class="date date-old">
                     {date('d.m', strtotime($supplier->update))}
                  </td>
               {/if}
               <td>
                  <div class='input-prepend  '>
                     <div class='btn-group'>
                        <button disabled class='btn dropdown-toggle' data-toggle='dropdown'>
                           <span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu pull-left'>
                           <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                  data-replace="Наличие">Уточнить
                              наличие</a></li>
                           <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                  data-replace="Резерв">Поставить в
                              резерв</a></li>
                           <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                  data-replace="Дата/страна">Дата/страна</a></li>
                        {*      <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                     data-replace="Заказать">Заказать</a></li> *}
                           <li class='divider'></li>
                           <li><a class='textify' tabindex='-1' data-clean='true' href='#'>Очистить</a></li>
                        </ul>
                     </div>
                     <textarea disabled
                               class='input-medium comment-operator'
                               data-supplier="{$supplier->supplierID}"
                               data-product="{$product->productID}"
                        >{$supplier->comment}</textarea>
                  </div>
               </td>
               <td colspan="2">{$supplier->soComment}</td>
            </tr>
            {/foreach}
            {foreachelse}
         <tr class="warning">
            <td colspan="{if $user->group != "operators"}11{else}8{/if}">В тикет еще не добавлены товары</td>
         </tr>
         {/foreach}
         </tbody>
      </table>
   </div>


</div>
</div>
</div>