{foreach $tickets as $ticket}
{include file="Index/Tickets/ticketLine.tpl" ticket=$ticket showstatus=true user=$user iteration=$ticket->status}
   {foreachelse}
<tr class="notfound">
   <td colspan="11">
      <h3 class="no-tickets text-info">Ничего не найдено по фразе &laquo;{$phrase}&raquo;. Уточните ваш запрос и
         попробуйте еще раз</h3>
   </td>
</tr>
{/foreach}