<div class="row ticket">
   <div class="span12">
      <div class="row">
         <div class="span2">
            <h3>Тикет №{$ticket->ID}</h3>
         </div>
         <div class="span10" class='status-box'>
            <div class="control-group" style="margin-top: 18px">
               <span><b>Статус:</b></span>
               <label class="radio inline">
                  <input name="status[{$ticket->ID}]" data-ticket="{$ticket->ID}" value="1" type="radio"
                         {if $ticket->status == 1}checked{/if} class="status statusNew">
                  В обработке
               </label>
               <label class="radio inline">
                  <input name="status[{$ticket->ID}]" data-ticket="{$ticket->ID}" value="2" type="radio"
                         {if $ticket->status == 2}checked{/if} class="status statusOrder">
                  Заказ
               </label>
               <label class="radio inline">
                  <input name="status[{$ticket->ID}]" data-ticket="{$ticket->ID}" value="3" type="radio"
                         {if $ticket->status == 3}checked{/if} class="status statusCancel">
                  Отказ
               </label>
            {if $user->group != 'operators'}
               <label class="radio inline">
                  <input name="status[{$ticket->ID}]" data-ticket="{$ticket->ID}" value="4" type="radio"
                         {if $ticket->status == 4}checked{/if} class="status statusClosed">
                  Закрыт
               </label>
            {/if}
            </div>
         </div>
      </div>
      <div class="row">
         <div class="span9">
            <table class="table table-hover">
               <thead>
               <tr>
                  <th>Артикул</th>
                  <th>Наименование</th>
                  <th></th>
                  <th>Город</th>
                  <th>К-во</th>
                  <th>Цена</th>
                  <th></th>
                  <th></th>
               </tr>
               </thead>
               <tbody>
               {foreach $ticket->products as $product}
               <tr class='info'>
                  <td>{$product->productID}</td>
                  <td>{$product->name}</td>
                  <td></td>
                  <td></td>
                  <td>{$product->amount} шт</td>
                  <td>{$product->price} грн</td>
                  <td>{$product->comment}</td>
                  <td>{$product->soComment}</td>
               </tr>
                  {foreach $product->suppliers as $supplier}
                  <tr class='supplier' data-product='{$product->productID}'>
                     <td>
                        <input class="purchase" name="supplier[{$product->productID}]"
                               data-supplier="{$supplier->supplierID}" data-ticket='{$ticket->ID}'
                               data-product="{$product->productID}" type="radio">
                     </td>
                     <td>{$supplier->name}</td>
                     <td>{$supplier->additionalInfo}</td>
                     <td>{$supplier->supplierCity}</td>
                     <td>{$supplier->amount} шт</td>
                     <td>{$supplier->price} грн</td>
                     <td>{$supplier->comment}</td>
                     <td>{$supplier->soComment}</td>
                  </tr>
                  {/foreach}
               {/foreach}
               </tbody>
            </table>
         </div>
         <div class="span3">
            <table class="table table-striped">
               <thead>
               <tr>
                  <th colspan="2">Клиент</th>
               </tr>
               </thead>
               <tr>
                  <td>ФИО</td>
                  <td>{$ticket->credentials->name}</td>
               </tr>
               <tr>
                  <td>№ Телефона</td>
                  <td>{$ticket->credentials->phone}</td>
               </tr>
            {if $ticket->credentials->deliveryMethodID == 0}
               <tr>
                  <td colspan="2">Метод доставки не выбран</td>
               </tr>
               {elseif $ticket->credentials->deliveryMethodID == 1}
               <tr>
                  <td colspan="2">Самовывоз</td>
               </tr>
               {elseif $ticket->credentials->deliveryMethodID == 2}
               <tr>
                  <td>Доставка по Киеву:</td>
                  <td>{$ticket->credentials->address}</td>
               </tr>
               {elseif $ticket->credentials->deliveryMethodID == 3}
               <tr>
                  <td colspan="2">Доставка по Украине:</td>
               </tr>
               <tr>
                  <td>Перевозчик</td>
                  <td>{$ticket->credentials->deliverer}</td>
               </tr>
               <tr>
                  <td>Склад</td>
                  <td>{$ticket->credentials->warehouse}</td>
               </tr>
               <tr>
                  <td>Получатель</td>
                  <td>{$ticket->credentials->receiver}</td>
               </tr>
            {/if}
            {if $ticket->comment}
               <tr>
                  <td colspan="2">Комментарий:</td>
               </tr>
               <tr>
                  <td colspan="2">{$ticket->comment}</td>
               </tr>
            {/if}
            </table>
         </div>

      </div>
   </div>
</div>
