<div class="tab-pane ticket-holder {if $first}active{/if}" id="{$name}">
   <table class="table table-striped table-sortable table-filterable">
      <thead>
      <tr>
         <th>№ <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
            class="icon-resize-vertical"></i></th>
         <th>Имя <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
            class="icon-resize-vertical"></i></th>
         <th>Телефон <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
            class="icon-resize-vertical"></i></th>
         <th style="width: 200px;">Товар <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
            class="icon-resize-vertical"></i></th>
      {literal}
         <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                   title="Самовывоз">С</a>
            <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
         <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                   title="Доставка по Киеву">К</a>
            <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
         <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                   title="Доставка по Украине">У</a>
            <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
      {/literal}
         <th data-filter-type="dropdown">Ответственный <i class="icon-chevron-up"></i><i
            class="icon-chevron-down"></i><i
            class="icon-resize-vertical"></i></th>
         <th data-filter-type="date-range" style="width: 170px">Дата <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
            class="icon-resize-vertical"></i></th>
         <th data-filter='none'><!--i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i--></th>
      </tr>
      </thead>
   {foreach $page->tickets as $ticket}
   {include file="Index/Tickets/ticketLine.tpl" ticket=$ticket user=$user iteration=$page@iteration}
      {foreachelse}
      <tr>
         <td colspan="11">
            <h3 class="no-tickets">Тут пока нет тикетов</h3>
         </td>
      </tr>
   {/foreach}
   </table>
</div>
