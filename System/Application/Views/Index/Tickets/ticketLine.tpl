<tr>
   <td>{$ticket->ID} {if $showstatus}{$ticket->statuswText}{/if}</td>
   <td>{$ticket->credentials->name}</td>
   <td>{$ticket->credentials->phone}</td>
   <td>
   {$ticket->products.0->name}
   </td>
   <td>{if $ticket->delivery.1}+{else}-{/if}</td>
   <td>{if $ticket->delivery.2}+{else}-{/if}</td>
   <td>{if $ticket->delivery.3}+{else}-{/if}</td>
   <td>{$ticket->creator}</td>
   <td>{$ticket->created}</td>
   <td>
   {if (in_array($user->group, array('logistics','administrators')) && $iteration > 3) || $iteration < 4}
      <button class="btn btn-mini edit-ticket" data-ticket='{$ticket->ID}'
         {literal}
              onmouseenter="$(this).tooltip({placement:'bottom'})"
         {/literal} title="Редактировать"
         ><i class="icon-pencil"></i></button>
      {else}
      <button class="btn btn-mini view-ticket" data-ticket='{$ticket->ID}'
         {literal}
              onmouseenter="$(this).tooltip({placement:'bottom'})"
         {/literal} title="Просмотреть"
         ><i class="icon-eye-open"></i></button>
   {/if}
   </td>
</tr>