<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
{include file='Index/head.tpl'}
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
   <div class="navbar-inner">
      <div class="container">
         <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </a>

         <form class="navbar-search form-search pull-left" id="search" method="post" action="/searchBySku/">
            <div class="input-append">
               <input type="text" class="span2 search-query"
                      placeholder="{if $smarty.session.lastsearchused == 'sku' || !isset($smarty.session.lastsearchused)}По артикулу{else}По наименованию{/if}"
                      name="value"
                      {if $skuSearch}value="{$skuSearch}" {elseif $nameSearch }value="{$nameSearch}"{/if}
                      autocomplete="off">

               <div class="btn-group">
                  <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown" title="Выберите тип поиска"
                     href="#" style="margin-top: -5px; border-radius: 0;">
                     <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" id="searchType">
                     <!-- dropdown menu links -->
                     <li><a href="#" data-type='sku'>По артикулу</a></li>
                     <li><a href="#" data-type='name'>По наименованию</a></li>
                  </ul>
               </div>
               <button class="btn btn-inverse submit" type="submit"
                       data-type='{if $smarty.session.lastserachused}{$smarty.session.lastsearchused}{else}sku{/if}'>
                  <i class="icon-search icon-white"></i></button>
            </div>
         </form>
         <div class="nav-collapse collapse">
            <ul class="nav">
               <li><a href="filters/">Подборы</a></li>
               <li{if $smarty.server.REQUEST_URI == '/'} class='active'{/if}><a href="/">Тикеты</a></li>
               <li><a href="#" id="currentTicket">Текущий тикет</a></li>
            {if !in_array($user->group, array('operators', 'superoperators'))}
               <li{if $smarty.server.REQUEST_URI == '/logistics/'} class='active'{/if}><a
                  href="/logistics/">Логистика</a></li>
            {/if}
            {if $user->group == 'administrators'}
               <li{if $smarty.server.REQUEST_URI == '/admin/'} class="active"{/if}><a
                  href="/admin/">Админ</a></li>
            {/if}
            </ul>
         </div>
         <ul class="nav pull-right">
            <li><a href="logout/">Выйти</a></li>
         </ul>
         <p class="navbar-text pull-right">Привет, {$user->name}. </p>
      </div>
   </div>
</div>

<div class="container">
{block name='content'} {/block}
   <div id="ticket" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
   {include file='Index/modal.tpl' date=date("d.m.Y") name=$user->name title="Новый тикет" user=$user}
   </div>
</div>
<!-- /container -->

</body>
</html>
