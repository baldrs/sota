{extends "Index/layout.tpl"}
{block name="content"}
<ul class="nav nav-tabs" id="suppliers">
   {foreach $suppliers as $supplier}
      <li{if $supplier@first} class="active" {/if} ><a data-toggle='tab'
                                                       href="#{$supplier->supplier}">{$supplier->supplier}
         ({$supplier->items|sizeof})</a></li>
   {/foreach}
</ul>
<div class="tab-content" id="logistics">

   {foreach $suppliers as $_supplier}
      <div class="tab-pane{if $_supplier@first} active{/if}" id="{$_supplier->supplier}">
         <table class="table table-sortable table-striped">
            <thead>
            <tr>
            {*<th>Купить</th>*}
               <th>Вход</th>
               <th>Наименование</th>
               <th></th>
               <th>К-во</th>
               <th>Вход</th>
               <th title="Рекомендованая розница">Розн</th>
               <th>Город</th>
               <th></th>
               <th></th>
               <th></th>
               <th style="min-width: 120px">Оператор</th>
               <th>Логист</th>
               <th></th>
            </tr>
            </thead>
            {foreach $_supplier->items as $supplier} {* лень было переименовывать, дальше кусок кода из другого места *}
               <tr class='supplier{*if $supplier->purchase} success{/if*}' data-product='{$product->productID}'>
               {*
             <td>
               <input class="purchase" name="supplier"
                       data-supplier="{$supplier->supplierID}" data-ticket='{$ticket->ID}'
                       data-product="{$product->productID}" {if $supplier->purchase}checked{/if} value="1"
                       type="checkbox">
                  </td>
                       *}
                  <td>{$supplier->rawPrice} {$supplier->currency}</td>
                  <td>{$supplier->name}</td>
                  <td>{$supplier->additionalInfo}</td>
                  <td class="amount"><input class="input-tiny supplier-amount" type="text" value="{$supplier->amount}"></td>
                  <td class="inprice"><input class="input-tiny supplier-inprice" value="{$supplier->inprice}" type="text"></td>
                  <td class="price"><input class="input-tiny supplier-price" value="{$supplier->price}" type="text"></td>
                  <td>{$supplier->supplierCity}</td>
                  <td>{$supplier->deliverers}</td>
                  <td>{$supplier->cashless}</td>
                  {if date('Ymd') == date('Ymd', strtotime($supplier->update))}
                     <td class="date">
                        {date('H:i', strtotime($supplier->update))}
                     </td>
                     {else}
                     <td class="date date-old">
                        {date('d.m', strtotime($supplier->update))}
                     </td>
                  {/if}
                  <td>{$supplier->comment}</td>
                  <td>
                     <div class='input-prepend  '>
                        <div class='btn-group'>
                           <button class='btn dropdown-toggle' data-toggle='dropdown'>
                              <span class='caret'></span>
                           </button>
                           <ul class='dropdown-menu pull-left'>
                              <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                     data-replace="В наличии">Есть в наличии</a></li>
                              <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                     data-replace="Нет в наличии">Нет в наличии</a></li>
                              <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                     data-replace="Резерв">Резерв</a></li>
                              <li><a tabindex='-1' data-product='{$product->productID}' class='textify' href='#'
                                     data-replace="Под заказ">Под заказ</a></li>
                              <li class='divider'></li>
                              <li><a class='textify' tabindex='-1' data-clean='true' href='#'>Очистить</a></li>
                           </ul>
                        </div>
                        <textarea
                           class='input-medium comment-operator'
                           data-supplier="{$supplier->supplierID}"
                           data-product="{$product->productID}"
                           >{$supplier->soComment}</textarea>
                     </div>
                  </td>
                  <td>
                     <div class="contol-row" style="width: 90px;">

                        <button class="btn update-supplier" data-supplier='{$supplier->supplierID}'
                                data-product='{$supplier->productID}' data-ticket='{$supplier->ticketID}'
                                onmouseenter="$(this).tooltip({literal}{placement: 'bottom'}{/literal})"
                                title="Сохранить">
                           <i class="icon-ok"></i>
                        </button>
                        <button class="btn edit-ticket" data-ticket='{$supplier->ticketID}'
                           {literal}
                                onmouseenter="$(this).tooltip({placement:'bottom'})"
                           {/literal} title="Редактировать"
                           ><i class="icon-info-sign"></i></button>
                     </div>
                  </td>
               </tr>
            {/foreach}
         </table>
      </div>
   {/foreach}
</div>
{/block}