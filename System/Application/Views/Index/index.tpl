{extends 'Index/layout.tpl'}
{block name="content"}
<ul class="nav nav-tabs" id="statuses">
   {foreach $pages as $name => $page}
      <li{if $page@first} class="active"{/if}>
         <a data-toggle="tab" href="#{$name}">
            {$page->name} (<span class="page{$page@index}">{$page->count}</span>)
         </a>
      </li>
   {/foreach}
   <li><a data-toggle='tab' href="#search-tickets">Поиск</a></li>
</ul>
<div class="tab-content">
   {foreach $pages as $name => $page}
   {include file='Index/Tickets/ticketPage.tpl' page=$page first=$page@first  name=$name}
   {*      <div class="tab-pane ticket-holder {if $page@first}active{/if}" id="{$name}">
            <table class="table table-striped table-sortable table-filterable">
               <thead>
               <tr>
                  <th>№ <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
                  <th>Имя <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
                  <th>Телефон <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
                  <th style="width: 200px;">Товар <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
                  {literal}
                     <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                               title="Самовывоз">С</a>
                        <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                           class="icon-resize-vertical"></i></th>
                     <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                               title="Доставка по Киеву">К</a>
                        <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                           class="icon-resize-vertical"></i></th>
                     <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                               title="Доставка по Украине">У</a>
                        <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                           class="icon-resize-vertical"></i></th>
                  {/literal}
                  <th data-filter-type="dropdown">Ответственный <i class="icon-chevron-up"></i><i
                     class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
                  <th data-filter-type="date-range" style="width: 170px">Дата <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
                  <th data-filter='none'><!--i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                        class="icon-resize-vertical"></i--></th>
               </tr>
               </thead>
               {foreach $page->tickets as $ticket}
               {include file="Index/Tickets/ticketLine.tpl" ticket=$ticket user=$user iteration=$page@iteration}
                  {foreachelse}
                  <tr>
                     <td colspan="11">
                        <h3 class="no-tickets">Тут пока нет тикетов</h3>
                     </td>
                  </tr>
               {/foreach}
            </table>
         </div>*}
   {/foreach}
   <div class="tab-pane ticket-holder" id="search-tickets">
      <div class="control-row">
         <input type="text" style="margin: 0;" class="input-xxlarge search-tickets-text">
         <button class="btn btn-primary search-tickets">Искать</button>
      </div>
      <table class="table table-striped table-sortable">
         <thead>
         <tr>
            <th>№ <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
            <th>Имя <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
            <th>Телефон <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
            <th style="width: 200px;">Товар <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
            {literal}
               <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                         title="Самовывоз">С</a>
                  <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
               <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                         title="Доставка по Киеву">К</a>
                  <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
               <th data-filter="none"><a rel="tooltip" href="#" onmouseenter="$(this).tooltip({placement: 'bottom'})"
                                         title="Доставка по Украине">У</a>
                  <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i></th>
            {/literal}
            <th data-filter-type="dropdown">Ответственный <i class="icon-chevron-up"></i><i
               class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
            <th data-filter-type="date-range">Дата <i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
               class="icon-resize-vertical"></i></th>
            <th data-filter='none'><!--i class="icon-chevron-up"></i><i class="icon-chevron-down"></i><i
                     class="icon-resize-vertical"></i--></th>
         </tr>
         </thead>
         {foreach $search as $ticket}
         {include file="Index/Tickets/ticketLine.tpl" ticket=$ticket user=$user iteration=$ticket->status}
            {foreachelse}
            <tr class="notfound">
               <td colspan="11">
                  <h3 class="no-tickets text-info">Чтобы начать поиск, введите искомую фразу в текстовое поле и нажмите
                     кнопку искать.</h3>
               </td>
            </tr>
         {/foreach}
      </table>
   </div>
</div>
{/block}