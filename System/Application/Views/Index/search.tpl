{extends 'Index/layout.tpl'}
{block name="content"}
   {if $nameSearch}
   <h2>Поиск &laquo;{$nameSearch}&raquo;</h2>
      {elseif $IDSearch}
   <h2>Поиск по артикулу &laquo;{$IDSearch}&raquo;</h2>
   {/if}
{include file='Index/Filters/filterTable.tpl' products=$products user=$user}
{*if $products}
<table class="table table-striped table-hover">
   <thead>
   <tr>
      <th>Артикул</th>
      <th>Наименование</th>
      <th>Количество</th>
      <th>Цена</th>
      <th></th>
   </tr>
   </thead>
   <tbody>
      {foreach $products as $product}
      <tr>
         <td>{$product->ID}</td>
         <td>{$product->name}</td>
         <td>{$product->amount}</td>
         <td>{$product->price}</td>
         <td>
            <button class="btn btn-primary pull-right addToTicket" type="button" data-id="{$product->ID}">
               <i class="icon-plus icon-white"></i> Добавить в тикет
            </button>
         </td>
      </tr>
      {/foreach}
   </tbody>
</table>
   {else}
<p class="lead text-info">
   По вашему запросу ничего не найдено
</p>
{/if*}
{/block}

