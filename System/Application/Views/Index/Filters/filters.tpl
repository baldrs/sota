{extends 'Index/layout.tpl'}
{block name="content"}
<div class="control-row">
   <select name="type" id="type">
      <option value="0">-</option>
      {foreach $types as $type}
         <option value="{$type->ID}">{$type->name}</option>
      {/foreach}
   </select>
</div>
<div class="control-row filter-container form-inline"></div>
<div class="filter-output"></div>
{/block}