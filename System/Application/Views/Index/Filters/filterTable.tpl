<table class="table table-hover table-sortable">
   <thead>
   <tr>
      <th>Артикул</th>
      <th>Наименование</th>
      <th></th>
      <th>Кол-во</th>
   {if $user->group != 'operators'}
      <th>Вход</th>
   {/if}
      <th>Цена</th>
   {if $user->group != 'operators'}
      <th>Поставщик</th>
   {/if}
      <th>Город</th>
      <th>Перевозчик</th>
      <th>Б/Н</th>
      <th></th>
   </tr>
   </thead>
   <tbody>
   {foreach $products as $product}
   <tr class="info">
      <td>{$product->sku}</td>
      <td>{$product->name}</td>
      <td>{$product->additional}</td>
      <td>{$product->amount}</td>
      {if $user->group != 'operators'}
         <td>{$product->inprice}</td>
      {/if}
      <td>{$product->price}</td>
      {if $user->group != 'operators'}
         <td>{$product->supplier}</td>
      {/if}
      <td></td>
      <td></td>
      <td></td>
      <td>
         <button class="btn btn-primary pull-right addToTicket btn-mini"
            {literal}
                 onmouseenter="$(this).tooltip({placement:'bottom'})"
            {/literal}
                 type="button" data-id="{$product->ID}" title="Добавить в тикет">
            <i class="icon-plus icon-white" style="margin-top: -1px;"></i>
         </button>
      </td>
   </tr>
      {foreach $product->suppliers as $supplier}
      <tr>
         <td>
            {if $user->group != 'operators'}
               {$supplier->rawPrice} {$supplier->currency}
            {/if}
         </td>
         <td>
            {$supplier->name}
         </td>
         <td>{$supplier->additionalInfo}</td>
         <td>{$supplier->amount}</td>
         {if $user->group != 'operators'}
            <td>{$supplier->inprice}</td>
         {/if}
         <td style="background-color: {if $product->price - $supplier->inprice > 25}#DFF0D8{else}#f2dede{/if};">
            {if $user->group != "operators"}
               {$supplier->price}
            {else}
               {if $product->price - $supplier->inprice >= 25}
               {$product->price}
               {else}
               {$supplier->price}
            {/if}
            {/if}
         </td>
         {if $user->group != 'operators'}
            <td>{$supplier->supplier}</td>
         {/if}
         <td>{$supplier->supplierCity}</td>
         <td>{$supplier->deliverers}</td>
         <td>{$supplier->cashless} </td>
         {if date('Ymd') == date('Ymd', strtotime($supplier->update))}
            <td class="date">
               {date('H:i', strtotime($supplier->update))}
            </td>
            {else}
            <td class="date date-old">
               {date('d.m', strtotime($supplier->update))}
            </td>
         {/if}
      </tr>
      {/foreach}
   {/foreach}
   </tbody>
</table>