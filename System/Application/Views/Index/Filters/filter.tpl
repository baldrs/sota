{*foreach $properties as $property}
{/foreach*}

<table class="table" style="width: auto">
   <thead>
   <tr>
   {foreach $properties as $property}
      <th>{$property->name}</th>
   {/foreach}
   {if $properties}
      <th>Нал</th>
      <th></th>
   {/if}
   </tr>
   </thead>
   <tbody>
   <tr>
   {foreach $properties as $property}
      {if $property->gentype == 1}
         <td>
            <input type="checkbox" name="{$property->ID}">
         </td>
         {elseif $property->gentype == 2}
         <td>
            <select name="{$property->ID}_min" class="input-mini">
               <option value="-">-</option>
               {foreach $property->values as $value}
                  <option value="{$value->value}">{$value->value}</option>
               {/foreach}
            </select>

            <select name="{$property->ID}_max" class="input-mini">
               <option value="-">-</option>
               {foreach $property->values as $value}
                  <option value="{$value->value}">{$value->value}</option>
               {/foreach}
            </select>
         </td>
         {else}
         <td>
            <select name="{$property->ID}" class="input-small">
               <option value="-">-</option>
               {foreach $property->values as $value}
                  <option value="{$value->value}"
                          {if $property->gendefault && $property->gendefault == $value->value}selected{/if}>{$value->value}</option>
               {/foreach}
            </select>
         </td>
      {/if}
   {/foreach}
   {if $properties}
      <td>
         <input type="checkbox" name="available" checked>
      </td>
      <td>
         <button class="btn btn-primary lookup">Искать</button>
      </td>
   {/if}
   </tr>
   </tbody>
</table>
